import React from 'react';
import './App.css';
import RewrdComSum from './Components/RewrdComSum';
import { MDBCard, MDBCardBody, MDBCardHeader, MDBBtn, MDBIcon, MDBRow, MDBCol } from 'mdbreact';

function App() {
  return (
    <div>
      <MDBRow>
        <MDBCol xl="6" style={{marginbottom: '1.5rem!important'}}>
          <MDBCard style={{ height: '450px' }}>
            <MDBCardHeader className="white-text">Reward of Companies Summary</MDBCardHeader>
            <MDBCardBody>
              <RewrdComSum/>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </div>
  );
}

export default App;

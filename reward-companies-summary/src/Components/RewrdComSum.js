import React, {useEffect, useState} from 'react';
import { Doughnut, Chart } from 'react-chartjs-2';
import testData from './testData';

//set value to middle Dougnut Chart
var originalDoughnutDraw = Chart.controllers.doughnut.prototype.draw;
Chart.helpers.extend(Chart.controllers.doughnut.prototype, {
  draw: function() {
    originalDoughnutDraw.apply(this, arguments);
    var chart = this.chart;
    var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx;
    var fontSize = (height / 140).toFixed(2);
    ctx.font = fontSize + "em sans-serif";
    ctx.textBaseline = "middle";
    var value = chart.config.data.datasets[0].data[0];
    var text = value + '%',
        textX = Math.round((width - ctx.measureText(text).width) / 2),
        textY = height / 1.9;
    ctx.fillText(text, textX, textY);
  }
});

function RewrdComSum(props) {

    const [csrCodeState, setCsrCodeState] = useState();
    const [csrCodeData, setCsrCodeData] = useState([]);
    const [csrDescription, setCsrDescription] = useState('');
    const [csrName, setCsrName] = useState('');

//setData
    const dataDoughnut1 = {
        datasets: [{
          data: [60,40],
          backgroundColor: ['#46BFBD','#f5f5f5'],
        }],
      };
    const dataDoughnut2 = {
    datasets: [{
        data: [30,70],
        backgroundColor: ['#ec1e1e','#f5f5f5'],
    }],
    };
    const dataDoughnut3 = {
    datasets: [{
        data: [10,90],
        backgroundColor: ['#2e2929','#f5f5f5'],
    }],
    };
    const optionDoughnut ={
        
        responsive: true,
        maintainAspectRatio: false
    }
    const fetchData = () => {
        let codeData = [];
        testData.CSR.map(data => {
            codeData.push({codeId: data.code,name:data.name ,des: data.description});
        });
        console.log(codeData);
        setCsrCodeData(codeData);
    }
    useEffect(()=>{
        fetchData();
    },[])


/*dropdown area*/
    const ddcsrCode = () => {
    return(
    <div>
        <button 
        style={{width:'100%', height:'30px'}}
            //className='btn-dropdown'
            data-toggle="dropdown"
        >
            V
        </button>
        <ul className="dropdown-menu dropdown-menu-right"  id='area'>
          <option className='dropdownElement' onClick={(e)=>{setCsrCodeState(''); setCsrDescription(''); setCsrName('')}}>-------------</option>
          { 
            csrCodeData.map(csrCode => 
            <option key={csrCode.codeId} className='dropdownElement' onClick={(e)=>{setCsrCodeState(e.target.value); setCsrDescription(csrCode.des); setCsrName(csrCode.name)}}>
              {csrCode.codeId}
            </option>)
          }
        </ul>
    </div>
    );
  }

    return (
        <div className='list-group row'>
            <div className='col-6'>
                <div className='row'>
                    <div className='col-5'>
                        <b>CSR's code</b>
                        <div className='row'>
                            <div className="csrCode">
                                {csrCodeState}
                            </div>
                            <div>
                                {ddcsrCode()}
                            </div>
                        </div>
                    </div>
                    <div className='col-7'>
                        <b>Description</b>
                        <textarea style={{width:'100%'}} value={csrDescription}></textarea>
                    </div>
                </div>
                <div style={{fontSize:'12px'}}>
                    <p> 
                        <b>{csrName} </b>
                        {csrDescription}
                    </p>
                </div>
                <div className='row' style={{marginTop:'-10px'}}>
                    <div className='col-4'>
                        <Doughnut data={dataDoughnut1} options={optionDoughnut} />
                    </div>
                    <div className='col-4'>
                        <Doughnut data={dataDoughnut2} options={optionDoughnut} />
                    </div>
                    <div className='col-4'>
                        <Doughnut data={dataDoughnut3} options={optionDoughnut} />
                    </div>
                </div>
                <div className='row' style={{fontSize:'10.5px', textAlign:'center'}}>
                    <div className='col-4'>
                        <a href='#'style={{color:'blue'}}>Fully compliance 60/100 areas</a>
                    </div>
                    <div className='col-4'>
                        <a href='#'>Partially compliance 30/100 areas</a>
                    </div>
                    <div className='col-4'>
                        <a href='#' style={{color:'red'}}>None compliance 10/100 areas</a>
                    </div>
                </div>
            </div>
            <div className='col-6'></div>
        </div>
    );
}

export default RewrdComSum;
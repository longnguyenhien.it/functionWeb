const grapql = require('graphql');

const { 
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull
} = grapql;

const LocationType = new GraphQLObjectType({
    name: 'Location',
    fields: () => ({
        
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        
    }
});
module.exports = new GraphQLSchema({
    query: RootQuery
})
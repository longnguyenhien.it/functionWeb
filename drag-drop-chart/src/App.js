//import react Hooks
import React, { useEffect, useState } from "react";
//import Css
import './Chart.css';
import './App.css';
//import DnD
import { DragDropContext, Droppable } from "react-beautiful-dnd";
//import Toastify
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
//import Chart Components
import LeftChart from "./Components/ChartComponents/LeftChart";
import LeftElement from "./Components/ChartComponents/LeftElement";
import MiddleChart from "./Components/ChartComponents/MiddleChart";
import MiddleElement from "./Components/ChartComponents/MiddleElement";
import RightChart from "./Components/ChartComponents/RightChart";
import RightChart2 from "./Components/ChartComponents/RightChart2";
//import File resource
import chartImages from "./Components/File/chartImage";
import lineElements from "./Components/File/lineElements";
import pieElement from "./Components/File/pieElement";
import barElements from "./Components/File/barElement";

toast.configure()
function App() {
  const [chartList, setChartList] = useState(chartImages);
  const [activeChart, setActiveChart] = useState([]);
  const [elementList, setElementList] = useState();
  const [activeElement, setActiveElement] = useState();
  const [keyChart, setKeyChart] = useState();
  const [hasChart, setHasChart] = useState(false);
  const [buttonChart, setButtonChart] = useState(false);

  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    //All
    if (!destination) {
      return;
    }
    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    //Middle Chart Field
    if (
      source.droppableId === "dndLeftChart" &&
      destination.droppableId === "dndMiddleChart" &&
      activeChart.length < 1
    ) {
      const sourceItems = [...chartList];
      const desItems = [...activeChart];
      const [removed] = sourceItems.splice(source.index, 1);
      desItems.splice(destination.index, 0, removed);
      setChartList(sourceItems);
      setActiveChart(desItems);
      setKeyChart(draggableId);
      switch (draggableId) {
        case "1":
          setElementList(lineElements.defaultElement);
          setActiveElement(lineElements.activeElement);
          setHasChart(true);
          break;
        case "2":
          setElementList(pieElement.defaultElement);
          setActiveElement(pieElement.activeElement);
          setHasChart(true);
          break;
        case "3":
          setElementList(barElements.defaultElement);
          setActiveElement(barElements.activeElement);
          setHasChart(true);
          break;
        case "4":
          setElementList(lineElements.defaultElement);
          setActiveElement(lineElements.activeElement);
          setHasChart(true);
          break;
        case "5":
          setElementList(lineElements.defaultElement);
          setActiveElement(lineElements.activeElement);
          setHasChart(true);
          break;
        case "6":
          setElementList(pieElement.defaultElement);
          setActiveElement(pieElement.activeElement);
          setHasChart(true);
          break;
        default:
          break;
      }
    }
    //Left Chart Field
    if (
      source.droppableId === "dndMiddleChart" &&
      destination.droppableId === "dndLeftChart"
    ) {
      const sourceItems = [...activeChart];
      const destItems = [...chartList];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setActiveChart(sourceItems);
      setChartList(destItems);
      setHasChart(false);
      setButtonChart(false);
    }

    //Middle X Element Field
    if (
      source.droppableId === "dndXLeftElement" &&
      destination.droppableId === "dndXMiddleElement" &&
      activeElement.X.length < 1
    ) {
      const sourceItems = [...elementList.X];
      const destItems = [...activeElement.X];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setElementList({
        ...elementList,
        X: sourceItems,
      });
      setActiveElement({
        ...activeElement,
        X: destItems,
      });
    }
    //Middle Y Element Field
    if (
      source.droppableId === "dndYLeftElement" &&
      destination.droppableId === "dndYMiddleElement" &&
      activeElement.Y.length < 2
    ) {
      const sourceItems = [...elementList.Y];
      const destItems = [...activeElement.Y];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index + 1, 0, removed);
      setElementList({
        ...elementList,
        Y: sourceItems,
      });
      setActiveElement({
        ...activeElement,
        Y: destItems,
      });
      if (activeElement.Y.length > 0) {
        setButtonChart(false)
      }
    }
    //Left X Element Field
    if (
      source.droppableId === "dndXMiddleElement" &&
      destination.droppableId === "dndXLeftElement"
    ) {
      const sourceItems = [...activeElement.X];
      const destItems = [...elementList.X];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setActiveElement({
        ...activeElement,
        X: sourceItems,
      });
      setElementList({
        ...elementList,
        X: destItems,
      });
      setButtonChart(false);
    }
    //Left Y Element Field
    if (
      source.droppableId === "dndYMiddleElement" &&
      destination.droppableId === "dndYLeftElement"
    ) {
      const sourceItems = [...activeElement.Y];
      const destItems = [...elementList.Y];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setActiveElement({
        ...activeElement,
        Y: sourceItems,
      });
      setElementList({
        ...elementList,
        Y: destItems,
      });
      setButtonChart(false);
      
    }
  };
  const showRightChart = () => {
  if (activeElement.Y.length > 1) {
    return(
      <>
        <RightChart
          keyChart={keyChart}
          activeElement={activeElement}
        />
        <br/>
        <RightChart2
          keyChart={keyChart}
          activeElement={activeElement}
        />
      </>
    );
  }
  if (activeElement.Y.length == 1) {
    return(
      <>
        <RightChart
          keyChart={keyChart}
          activeElement={activeElement}
        />
      </>
    );
  }
  };
  const clickConFirm = () => {
    if (activeChart.length == 0) {
      toast.error('Please Choose Chart!')
      return;
    }
    if (
      activeElement.X.length == 0 &&
      activeElement.Y.length == 0
    ) {
      toast.error("Please Choose X and Y Element");
      return;
    }
    if (
      activeElement.X.length == 0
    ) {
      toast.error("Please Choose X Element");
      return;
    }
    if (
      activeElement.Y.length == 0
    ) {
      toast.error("Please Choose Y Element");
      return;
    }
    setButtonChart(true);
  };
  return (
    <div
      style={{
        backgroundColor: "#f6f8fa",
      }}
    >
      <DragDropContext onDragEnd={onDragEnd}>
        <div className="row">
          {/* left column */}
          <aside className="column left">
            <div className="left-group">
              <div className="left-item" style={{ marginTop: "30px" }}>
                {/* <input type="text" placeholder="Seach..."></input> */}
              </div>
              <strong style={{ marginTop: "30px", fontSize: "16px" }}>
                Chart:
              </strong>
              <br />
              <br />
              <LeftChart chartList={chartList} />
              {hasChart ? <LeftElement elementList={elementList} /> : null}
            </div>
          </aside>
          {/* middle column */}
          <section className="column midle">
            <div className="midle-group">
              <div className="middle-item" style={{ marginTop: "30px" }}>
                {/* <input type="text" placeholder="Seach..."></input> */}
              </div>
              <strong style={{ marginTop: "30px", fontSize: "16px" }}>
                Chart:
              </strong>
              <MiddleChart activeChart={activeChart} hasChart={hasChart} />
              <div style={{ textAlign: "center", marginTop: "20px" }}>
                {hasChart ? (
                  <MiddleElement
                    activeElement={activeElement}
                    hasChart={hasChart}
                  />
                ) : null}
              </div>
              <div style={{ textAlign: "center", marginTop: "100px" }}>
                <button
                  className="button-Confirm"
                  onClick={clickConFirm}
                >
                  Confirm
                </button>
              </div>
            </div>
          </section>
          {/* right column */}
          <section className="column right">
            <div className="right-group">
              <div className="right-item">
              <div style={{ marginTop: "20px", marginLeft:"40px", fontSize: "30px"}}>
                <strong>{buttonChart ? activeElement.X[0].name : null}</strong>
              </div>
                {buttonChart ? (
                  showRightChart()
                ) : null}
              </div>
            </div>
          </section>
        </div>
      </DragDropContext>
    </div>
  );
}

export default App;

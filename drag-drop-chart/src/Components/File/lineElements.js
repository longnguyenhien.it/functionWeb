const lineElements = {
  XName: {
    Day: ["Mon", "Tue", "Wed", "Thu", "Fri"],
    Week: ["1", "2", "3", "4"],
    Month: ["Jan", "Mar", "May", "Jul", "Sep", "Nov", "Dec"],
    Year: ["2018", "2019", "2020"],
  },
  YName: {
    HourlyTemp: {
      Day: [3, 2, 5, 6, 1],
      Week: [12, 23, 24, 13],
      Month: [35, 56, 67, 12, 34, 67, 43],
      Year: [233, 566, 677],
    },
    RainFall: {
      Day: [5, 2, 3, 1, 6],
      Week: [2, 27, 2, 13],
      Month: [31, 36, 27, 52, 14, 27, 33],
      Year: [133, 266, 377],
    },
    WindSpeed: {
      Day: [3, 2, 1, 6, 5],
      Week: [3, 2, 24, 13],
      Month: [31, 36, 27, 52, 14, 27, 33],
      Year: [133, 266, 377],
    },
    Money: {
      Day: [8, 1, 4, 3, 2],
      Week: [4, 27, 24, 1],
      Month: [31, 36, 27, 52, 14, 27, 33],
      Year: [133, 266, 377],
    },
    Rate: {
      Day: [8, 1, 4, 3, 2],
      Week: [5, 7, 24, 13],
      Month: [31, 36, 27, 52, 14, 27, 33],
      Year: [133, 566, 377],
    },
  },
  defaultElement: {
    X: [
      { id: "X1", name: "Day" },
      { id: "X2", name: "Week" },
      { id: "X3", name: "Month" },
      { id: "X4", name: "Year" },
    ],
    Y: [
      { id: "Y1", name: "Hourly Temp" },
      { id: "Y2", name: "RainFall" },
      { id: "Y3", name: "WindSpeed" },
      { id: "Y4", name: "Money" },
      { id: "Y5", name: "Rate" },
    ],
  },
  activeElement: {
    X: [],
    Y: [],
  },
};
export default lineElements;

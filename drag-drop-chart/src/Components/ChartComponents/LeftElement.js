import React, { useRef, useState } from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";

function LeftElement(props) {
  const { elementList } = props;

  return (
    <div>
      <div style={{ textAlign: "left", marginTop: "50px" }}>
        {/* X Element */}
        <strong style={{ textAlign: "left"}}>
          X Element:
          <br />
          <br />
        </strong>
        <Droppable droppableId="dndXLeftElement">
          {(provided, snapshot) => {
            return (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                style={{
                  background: snapshot.isDraggingOver ? "lightgrey" : "white",
                  /* justifycontent: 'center', */
                  ...provided.droppableProps.style,
                }}
              >
                {elementList.X.map((ele, index) => {
                  return (
                    <Draggable key={ele.id} draggableId={ele.id} index={index}>
                      {(provided) => {
                        return (
                          <>
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={{
                                display: "block",
                                padding: "5px 15px",
                                /* border: "1px solid Lightgrey", */
                                /* backgroundColor: "lightgreen", */
                                textAlign: "left",
                                /*  borderRadius: "6px", */
                                marginBottom: "2px",
                                fontSize: "15px",
                                ...provided.draggableProps.style,
                              }}
                              className="draggable-element"
                            >
                              <img src="/chartIcon/star.png" /> &nbsp; &nbsp;
                              &nbsp;
                              {ele.name}
                            </div>
                          </>
                        );
                      }}
                    </Draggable>
                  );
                })}
                {provided.placeholder}
              </div>
            );
          }}
        </Droppable>
        <br />
        {/* Y Element */}
        <strong>
          Y Element:
          <br />
          <br />
        </strong>
        <Droppable droppableId="dndYLeftElement">
          {(provided, snapshot) => {
            return (
              <span
                {...provided.droppableProps}
                ref={provided.innerRef}
                style={{
                  background: snapshot.isDraggingOver ? "lightgrey" : "white",
                  ...provided.droppableProps.style,
                }}
              >
                {elementList.Y.map((ele, index) => {
                  return (
                    <Draggable key={ele.id} draggableId={ele.id} index={index}>
                      {(provided) => {
                        return (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={{
                              padding: "5px 15px",
                              /* border: "1px solid Lightgrey", */
                              textAlign: "left",
                              fontSize: "15px",
                              /* borderRadius: "6px", */
                              marginBottom: "3px",
                              /* backgroundColor: "lightgreen", */
                              ...provided.draggableProps.style,
                            }}
                          >
                            <img src="/chartIcon/star.png" /> &nbsp; &nbsp;
                            &nbsp;
                            {ele.name}
                          </div>
                        );
                      }}
                    </Draggable>
                  );
                })}
                {provided.placeholder}
              </span>
            );
          }}
        </Droppable>
      </div>
    </div>
  );
}

export default LeftElement;

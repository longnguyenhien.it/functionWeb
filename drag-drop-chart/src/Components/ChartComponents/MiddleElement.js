import React, { useEffect, useState } from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";

function MiddleElement(props) {
  const { activeElement } = props;

  const activeXField = () => {
    if (activeElement.X.length == 0) {
      return <p style={{ opacity: 0.6, marginTop: "15px" }}>Drag X Element</p>;
    }
    return activeElement.X.map((acEle, index) => {
      return (
        <Draggable key={acEle.id} draggableId={acEle.id} index={index}>
          {(provided) => {
            return (
              <>
                <p
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                  style={{
                    padding: "10px",
                    /*  border: "1px solid Lightgrey", */
                    textAlign: "left",
                    /* borderRadius: "6px", */
                    marginBottom: "2px",
                    /* backgroundColor: "lightgreen", */
                    fontSize: "15px",
                    ...provided.draggableProps.style,
                  }}
                >
                  <img src="/chartIcon/star.png" /> &nbsp; &nbsp; &nbsp;
                  {acEle.name}
                </p>
              </>
            );
          }}
        </Draggable>
      );
    });
  };
  const activeYField = () => {
    if (activeElement.Y.length == 0) {
      return <p style={{ opacity: 0.6, marginTop: "15px" }}>Drag Y Element</p>;
    }
    return activeElement.Y.map((acEle, index) => {
      return (
        <Draggable key={acEle.id} draggableId={acEle.id} index={index}>
          {(provided) => {
            return (
              <>
                <p
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                  style={{
                    padding: "10px",
                    /*  border: "1px solid Lightgrey", */
                    textAlign: "left",
                    /* borderRadius: "6px", */
                    marginBottom: "2px",
                    /* backgroundColor: "lightgreen", */
                    fontSize: "15px",
                    ...provided.draggableProps.style,
                  }}
                >
                  <img src="/chartIcon/star.png" /> &nbsp; &nbsp; &nbsp;
                  {acEle.name}
                </p>
              </>
            );
          }}
        </Draggable>
      );
    });
  };

  return (
    <>
      <h4 style={{ marginTop: "40px", textAlign: "left" }}>
        Column/Label/X-Axis:
      </h4>
      <Droppable droppableId="dndXMiddleElement">
        {(provided, snapshot) => {
          return (
            <div
              className="midle-item midle-column midle-row-false"
              {...provided.droppableProps}
              ref={provided.innerRef}
              style={{
                background: snapshot.isDraggingOver ? "lightgrey" : "white",
              }}
            >
              <p style={{ marginTop: "10px" }}>{activeXField()}</p>
              {provided.placeholder}
            </div>
          );
        }}
      </Droppable>

      <h4 style={{ marginTop: "60px", textAlign: "left" }}>Row/Y-Axis:</h4>
      <Droppable droppableId="dndYMiddleElement">
        {(provided, snapshot) => {
          return (
            <div
              className="midle-item midle-column midle-row-false"
              {...provided.droppableProps}
              ref={provided.innerRef}
              style={{
                background: snapshot.isDraggingOver ? "lightgrey" : "white",
              }}
            >
              <p style={{ marginTop: "10px" }}>{activeYField()}</p>
              {provided.placeholder}
            </div>
          );
        }}
      </Droppable>
    </>
  );
}

export default MiddleElement;

import React from 'react';
import './App.css';
import MainPage from './components/Main/MainPage';
import SideBar from './components/SideBar/SideBar';


function App() {
  return (
    <div className="row">
      <div className='col-2 col-sm-2 col-md-1 col-lg-1'><SideBar/></div>
      <div className='col-10 col-sm-10 col-md-11 col-lg-11'><MainPage /></div>
    </div>
  );
}

export default App;

import React, {useEffect, useState} from 'react';
import { Line, Pie } from "react-chartjs-2";
import { defaults } from 'react-chartjs-2';


defaults.global.defaultFontColor = '#5D54A4';


function Interaction(props) {
    const {dataAllFiltered, startDate, endDate} = props
    const [chartTypeState, setChartTypeState] = useState('Vùng');
    const [labelData, setLabelData] = useState();
    const [chartData, setChartData] = useState();


    {/*dropdown area*/}
    const ddchartType = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
            <option className='w3-animate-right dropdownElement' onClick={()=>{setChartTypeState('')}}>-------------------</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Vùng</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Đường</option>
        </ul>
    </div>
    );}
    //set Data và Label
    useEffect(()=>{
        const getDaysArray = (start, end) => {
            let arr=[];
            for(let dt=new Date(start); dt<=end; dt.setDate(dt.getDate()+1)){
                arr.push(new Date(dt));
            }
            return arr;
        }; 
        //set labelData
        let daylist = getDaysArray(new Date(startDate),new Date(endDate));
        let weekday = ["Chủ Nhật","Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"]
        daylist = daylist.map((v)=> v = weekday[v.getDay()])//.slice(0,10)).join("")
        setLabelData([...daylist])
        //set chartData
        let dayList1 = getDaysArray(new Date(startDate),new Date(endDate));
        dayList1 = dayList1.map((v)=>v.toISOString());
        let dataChartList = [];
        for (let i = 0; i < dayList1.length; i++) {
            let count = 0;
            let dateD = new Date(dayList1[i]).getDate();
            let dateM = new Date(dayList1[i]).getMonth();
            let dateY = new Date(dayList1[i]).getFullYear();
            for (let j = 0; j < dataAllFiltered.length; j++) {
                let answerDateD = new Date(dataAllFiltered[j].createdDate.$date).getDate();
                let answerDateM = new Date(dataAllFiltered[j].createdDate.$date).getMonth();
                let answerDateY = new Date(dataAllFiltered[j].createdDate.$date).getFullYear();
                if (dateD === answerDateD && dateM === answerDateM && dateY === answerDateY) {
                    count++;
                }
            }
            dataChartList.push(count);
        }
        setChartData([...dataChartList]);
    }, [dataAllFiltered])

    //LineFill
    const dataLineFill = {
        labels: labelData,
        datasets: [
          {
            fill: true,
            data: chartData,
            borderColor: ["rgba(153, 102, 255, 1)"],
            backgroundColor: ["rgba(153, 102, 255, 1)"],
            pointbackgroundColor: "rgba(153, 102, 255, 1)",
            pointborderColor: "rgba(153, 102, 255, 1)",
          },
        ],
      };
    //Line  
    const dataLine = {
        labels: labelData,
        datasets: [
          {
            fill: false,
            data: chartData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
        ],
    };
    const options = {
        scale: {
            ticks: { beginAtZero: true },
          },
        title: {
          display: true,
          text: "SỐ LƯỢNG TƯƠNG TÁC",
          responsive: true,
          maintainAspectRatio: false
        }
    };


    const chartControl = () => {
        switch (chartTypeState) {
            case 'Vùng':
                return <Line data={dataLineFill} options={options} />
            case 'Đường':
                return <Line data={dataLine} options={options} />
            default:
                return null
        }
    }
    return (
        <div>
            {/*chart type*/}
            <div className="chartType">
                <div className="row">
                <div
                    className='chartTypeState'
                >
                    {chartTypeState}
                </div>
                <div
                    className='chartTypeDD'
                >
                    {ddchartType()}
                </div>
                </div>
            </div>
            {/*chart*/}
            <div className='chart'>{chartControl()}</div>
        </div>
    );
}

export default Interaction;
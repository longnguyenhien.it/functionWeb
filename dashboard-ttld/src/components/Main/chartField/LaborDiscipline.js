import React, {useEffect, useState} from 'react';
import { Line, Bar } from "react-chartjs-2";
import { defaults } from 'react-chartjs-2';


defaults.global.defaultFontColor = '#5D54A4';


function LaborDiscipline(props) {
    const {dataAllFiltered} = props
    const [chartTypeState, setChartTypeState] = useState('Vùng');
    const [labelData, setLabelData] = useState([
        "Doanh nghiệp  có thực  hiện đúng  quy định  về các  khoản khấu  trừ tiền  lương của  người lao  động không ? (v.d.  tiền bảo  hiểm, tiền  phạt, v.v….)", 
        "Doanh nghiệp  có tuân  thủ quy  định về  xây dựng  và đăng  ký NQLĐ  với cơ  quan quản  lý nhà  nước không?",
        "NQLĐ có  được cập  nhật với  thực trạng  Doanh nghiệp  và còn  hiệu lực  tại thời  điểm này  không?",
        "Doanh nghiệp  có tuân  thủ quy  định và  trình tự  xử lý  kỷ luật  lao động  không?"
    ]);
    const [veryAgreeData, setVeryAgreeData] = useState();
    const [agreeData, setAgreeData] = useState();
    const [middleData, setMiddleData] = useState([0,0,0,0,0,0,0,0]);
    const [disAgreeData, setDisAgreeData] = useState();
    const [veryDisAgreeData, setVeryDisAgreeData] = useState([0,0,0,0,0,0,0,0]);
    const [dontKnowData, setDontKnowData] = useState();
    {/*dropdown area*/}
    const ddchartType = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
            <option className='w3-animate-right dropdownElement' onClick={()=>{setChartTypeState('')}}>-------------------</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Vùng</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Đường</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Cột</option>
        </ul>
    </div>
    );}
    //set Data và Label
    useEffect(()=>{
      //set Data for this component
        let componentData = dataAllFiltered.filter(data => {
            return data.groupId.$oid === "5ec33833329e33023ffd6f85"
        })
      //set Data for veryAgreeData 1
        let veryAgree = []; 
        let cVALSI513 = 0, cVALSI61 = 0, cVALSI611 = 0, cVALSI62 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5ec338e5329e33023ffd73ee" && answer.questionCode === "LSI5.13") {
              cVALSI513++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd7494" && answer.questionCode === "LSI6.1") {
              cVALSI61++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd73fa" && answer.questionCode === "LSI6.1.1") {
              cVALSI611++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd7373" && answer.questionCode === "LSI6.2") {
              cVALSI62++;
            }
          })
        })
        veryAgree.push(cVALSI513, cVALSI61, cVALSI611, cVALSI62);
        console.log(veryAgree);
        setVeryAgreeData([...veryAgree]);
      //set Data for agreeData 3
        let agree = []; 
        let cALSI513 = 0, cALSI61 = 0, cALSI611 = 0, cALSI62 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5ec338e5329e33023ffd73bf" && answer.questionCode === "LSI5.13") {
              cALSI513++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd740a" && answer.questionCode === "LSI6.1") {
              cALSI61++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd73cb" && answer.questionCode === "LSI6.1.1") {
              cALSI611++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd73af" && answer.questionCode === "LSI6.2") {
              cALSI62++;
            }
          })
        })
        agree.push(cALSI513, cALSI61, cALSI611, cALSI62);
        console.log(agree);
        setAgreeData([...agree]);
      //set Data for disAgreeData 2
        let disAgree = []; 
        let cDALSI513 = 0, cDALSI61 = 0, cDALSI611 = 0, cDALSI62 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5ec338e5329e33023ffd745c" && answer.questionCode === "LSI5.13") {
              cDALSI513++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd744c" && answer.questionCode === "LSI6.1") {
              cDALSI61++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd7402" && answer.questionCode === "LSI6.1.1") {
              cDALSI611++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd74a8" && answer.questionCode === "LSI6.2") {
              cDALSI62++;
            }
          })
        })
        disAgree.push(cDALSI513, cDALSI61, cDALSI611, cDALSI62);
        console.log(disAgree);
        setDisAgreeData([...disAgree]);
      //set Data for dontKnowData 4
        let dontKnow = []; 
        let cDKLSI513 = 0, cDKLSI61 = 0, cDKLSI611 = 0, cDKLSI62 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5ec338e5329e33023ffd73e6" && answer.questionCode === "LSI5.13") {
              cDKLSI513++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd7490" && answer.questionCode === "LSI6.1") {
              cDKLSI61++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd73b7" && answer.questionCode === "LSI6.1.1") {
              cDKLSI611++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd747c" && answer.questionCode === "LSI6.2") {
              cDKLSI62++;
            }
          })
        })
        dontKnow.push(cDKLSI513, cDKLSI61, cDKLSI611, cDKLSI62);
        console.log(dontKnow);
        setDontKnowData([...dontKnow]);
    }, [dataAllFiltered])

    //LineFill
    const dataLineFill = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: true,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: true,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: true,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: true,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: true,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: true,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
      };
    //Line  
    const dataLine = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: false,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: false,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: false,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: false,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: false,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: false,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
    };
    //Bar
    const dataBar = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            data: veryAgreeData,
            borderColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              ],
            backgroundColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
            ],
          },
          {
            label: "Đồng Ý",
            data: agreeData,
            borderColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
          },
          {
            label: "Trung Lập",
            data: middleData,
            borderColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
          },
          {
            label: "Không Đồng Ý",
            data: disAgreeData,
            borderColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
            backgroundColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
          },
          {
            label: "Rất Không Đồng Ý",
            data: veryDisAgreeData,
            borderColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
            backgroundColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
          },
          {
            label: "Không Rõ",
            data: dontKnowData,
            borderColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
            backgroundColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
          },
        ],
    }
    const options = {
        title: {
          display: true,
          text: "KỶ LUẬT LAO ĐỘNG",
          responsive: true,
          maintainAspectRatio: false,
        },
        pointLabelFontSize : 10,
        scales: {         
          ticks: { beginAtZero: true },
            xAxes: [
              {
                ticks: {
                  callback: function(label) {
                    if (/\s/.test(label)) {
                      return label.split("  ");
                    }else{
                      return label;
                    }              
                  }
                },
              },
            ],
          }
    };
    const optionsBar = {
      title: {
        display: true,
        text: "KỶ LUẬT LAO ĐỘNG",
        responsive: true,
        maintainAspectRatio: false,
      },
      pointLabelFontSize : 10,
      scales: {         
        ticks: { beginAtZero: true },
          xAxes: [
            {
              ticks: {
                callback: function(label) {
                  if (/\s/.test(label)) {
                    return label.split("  ");
                  }else{
                    return label;
                  }              
                }
              },
              stacked: true,
            },
          ],
          yAxes: [
            {
              stacked: true,
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        }
  };
    
    const chartControl = () => {
        switch (chartTypeState) {
            case 'Vùng':
                return <Line data={dataLineFill} options={options} height={210}/>
            case 'Đường':
                return <Line data={dataLine} options={options} height={210}/>
            case 'Cột':
                return<Bar data={dataBar} options={optionsBar} height={210}/>
            default:
                return null
        }
    }
    return (
        <div>
            {/*chart type*/}
            <div className="chartType">
                <div className="row">
                <div
                    className='chartTypeState'
                >
                    {chartTypeState}
                </div>
                <div
                    className='chartTypeDD'
                >
                    {ddchartType()}
                </div>
                </div>
            </div>
            {/*chart*/}
            <div className='chart'>{chartControl()}</div>
        </div>
    );
}

export default LaborDiscipline;
import React, {useEffect, useState} from 'react';
import { Line, Bar } from "react-chartjs-2";
import { defaults } from 'react-chartjs-2';


defaults.global.defaultFontColor = '#5D54A4';


function FemaleLaborWelfare(props) {
    const {dataAllFiltered} = props
    const [chartTypeState, setChartTypeState] = useState('Vùng');
    const [labelData, setLabelData] = useState([
        "Bố trí  nghỉ 01  giờ trong  ngày làm  việc đối  với lao  động nữ  mang thai  từ tháng  thứ 7 trở  lên, nuôi  con nhỏ  dưới 12  tháng tuổi", 
        "Bố trí  nghỉ 30  phút trong  ngày làm  việc đối  với lao  động nữ  trong thời  gian hành  kinh: ",
        "Trả lương  cho lao  động nữ  trong giờ  nghỉ làm  vệ sinh  kinh nguyệt  (đối với  lao động  hưởng lương  theo sản  phẩm)",
        "Bảo đảm  việc làm  cho lao  động nữ  sau khi  hết thời  gian nghỉ  thai sản",
        "Việc xử  lý kỷ  luật lao  động đối  với lao  động nữ  trong thời  gian mang  thai, nghỉ  hưởng chế  độ thai  sản và  nuôi con  nhỏ dưới  12 tháng  tuổi"
    ]);
    const [veryAgreeData, setVeryAgreeData] = useState();
    const [agreeData, setAgreeData] = useState();
    const [middleData, setMiddleData] = useState([0,0,0,0,0,0,0,0]);
    const [disAgreeData, setDisAgreeData] = useState();
    const [veryDisAgreeData, setVeryDisAgreeData] = useState([0,0,0,0,0,0,0,0]);
    const [dontKnowData, setDontKnowData] = useState();
    {/*dropdown area*/}
    const ddchartType = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
            <option className='w3-animate-right dropdownElement' onClick={()=>{setChartTypeState('')}}>-------------------</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Vùng</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Đường</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Cột</option>
        </ul>
    </div>
    );}
    //set Data và Label
    useEffect(()=>{
      //set Data for this component
        let componentData = dataAllFiltered.filter(data => {
            return data.groupId.$oid === "5ecf5d54329e33023ffd9af6"
        })
      //set Data for veryAgreeData
        let veryAgree = []; 
        let cVALSI912 = 0, cVALSI913 = 0, cVALSI914 = 0, cVALSI915 = 0, cVALSI916 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86de" && answer.questionCode === "LSI9.1.2") {
              cVALSI912++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd869a" && answer.questionCode === "LSI9.1.3") {
              cVALSI913++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd870a" && answer.questionCode === "LSI9.1.4") {
              cVALSI914++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd866a" && answer.questionCode === "LSI9.1.5") {
              cVALSI915++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd869e" && answer.questionCode === "LSI9.1.6") {
              cVALSI916++;
            }
          })
        })
        veryAgree.push(cVALSI912, cVALSI913, cVALSI914, cVALSI915, cVALSI916);
        console.log(veryAgree);
        setVeryAgreeData([...veryAgree]);
      //set Data for agreeData
        let agree = []; 
        let cALSI912 = 0, cALSI913 = 0, cALSI914 = 0, cALSI915 = 0, cALSI916 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86a2" && answer.questionCode === "LSI9.1.2") {
              cALSI912++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8692" && answer.questionCode === "LSI9.1.3") {
              cALSI913++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86fe" && answer.questionCode === "LSI9.1.4") {
              cALSI914++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8712" && answer.questionCode === "LSI9.1.5") {
              cALSI915++;
            }
            if (answer.answerId.$oid === "5eccc1f5329e33023ffd8732" && answer.questionCode === "LSI9.1.6") {
              cALSI916++;
            }
          })
        })
        agree.push(cALSI912, cALSI913, cALSI914, cALSI915, cALSI916);
        console.log(agree);
        setAgreeData([...agree]);
      //set Data for disAgreeData
        let disAgree = []; 
        let cDALSI912 = 0, cDALSI913 = 0, cDALSI914 = 0, cDALSI915 = 0, cDALSI916 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8726" && answer.questionCode === "LSI9.1.2") {
              cDALSI912++;
            }
            if (answer.answerId.$oid === "5eccc1f5329e33023ffd8742" && answer.questionCode === "LSI9.1.3") {
              cDALSI913++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86ae" && answer.questionCode === "LSI9.1.4") {
              cDALSI914++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86ce" && answer.questionCode === "LSI9.1.5") {
              cDALSI915++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8696" && answer.questionCode === "LSI9.1.6") {
              cDALSI916++;
            }
          })
        })
        disAgree.push(cDALSI912, cDALSI913, cDALSI914, cDALSI915, cDALSI916);
        console.log(disAgree);
        setDisAgreeData([...disAgree]);
      //set Data for dontKnowData
        let dontKnow = []; 
        let cDKLSI912 = 0, cDKLSI913 = 0, cDKLSI914 = 0, cDKLSI915 = 0, cDKLSI916 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd867a" && answer.questionCode === "LSI9.1.2") {
              cDKLSI912++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd868a" && answer.questionCode === "LSI9.1.3") {
              cDKLSI913++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8672" && answer.questionCode === "LSI9.1.4") {
              cDKLSI914++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86ba" && answer.questionCode === "LSI9.1.5") {
              cDKLSI915++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86d6" && answer.questionCode === "LSI9.1.6") {
              cDKLSI916++;
            }
          })
        })
        dontKnow.push(cDKLSI912, cDKLSI913, cDKLSI914, cDKLSI915, cDKLSI916);
        console.log(dontKnow);
        setDontKnowData([...dontKnow]);
    }, [dataAllFiltered])

    //LineFill
    const dataLineFill = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: true,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: true,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: true,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: true,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: true,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: true,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
      };
    //Line  
    const dataLine = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: false,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: false,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: false,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: false,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: false,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: false,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
    };
    //Bar
    const dataBar = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            data: veryAgreeData,
            borderColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              ],
            backgroundColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
            ],
          },
          {
            label: "Đồng Ý",
            data: agreeData,
            borderColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
          },
          {
            label: "Trung Lập",
            data: middleData,
            borderColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
          },
          {
            label: "Không Đồng Ý",
            data: disAgreeData,
            borderColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
            backgroundColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
          },
          {
            label: "Rất Không Đồng Ý",
            data: veryDisAgreeData,
            borderColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
            backgroundColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
          },
          {
            label: "Không Rõ",
            data: dontKnowData,
            borderColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
            backgroundColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
          },
        ],
    }
    const options = {
        ticks: { beginAtZero: true },
        title: {
          display: true,
          text: "NGHỈ, TRỢ CẤP THAI SẢN VÀ BẢO ĐẢM CÔNG VIỆC",
          responsive: true,
          maintainAspectRatio: false,
        },
        pointLabelFontSize : 10,
        scales: {         
            xAxes: [
              {
                ticks: {
                  callback: function(label) {
                    if (/\s/.test(label)) {
                      return label.split("  ");
                    }else{
                      return label;
                    }              
                  }
                },
              },
            ],
          }
    };
    const optionsBar = {
      title: {
        display: true,
        text: "NGHỈ, TRỢ CẤP THAI SẢN VÀ BẢO ĐẢM CÔNG VIỆC",
        responsive: true,
        maintainAspectRatio: false,
      },
      pointLabelFontSize : 10,
      scales: {     
          ticks: { beginAtZero: true },    
          xAxes: [
            {
              ticks: {
                callback: function(label) {
                  if (/\s/.test(label)) {
                    return label.split("  ");
                  }else{
                    return label;
                  }              
                }
              },
              stacked: true,
            },
          ],
          yAxes: [
            {
              stacked: true,
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        }
  };
    
    const chartControl = () => {
        switch (chartTypeState) {
            case 'Vùng':
                return <Line data={dataLineFill} options={options} height={210}/>
            case 'Đường':
                return <Line data={dataLine} options={options} height={210}/>
            case 'Cột':
                return<Bar data={dataBar} options={optionsBar} height={210}/>
            default:
                return null
        }
    }
    return (
        <div>
            {/*chart type*/}
            <div className="chartType">
                <div className="row">
                <div
                    className='chartTypeState'
                >
                    {chartTypeState}
                </div>
                <div
                    className='chartTypeDD'
                >
                    {ddchartType()}
                </div>
                </div>
            </div>
            {/*chart*/}
            <div className='chart'>{chartControl()}</div>
        </div>
    );
}

export default FemaleLaborWelfare;
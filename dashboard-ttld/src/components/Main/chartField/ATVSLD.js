import React, {useEffect, useState} from 'react';
import { Line, Bar } from "react-chartjs-2";
import { defaults } from 'react-chartjs-2';


defaults.global.defaultFontColor = '#5D54A4';


function ATVSLD(props) {
    const {dataAllFiltered} = props
    const [chartTypeState, setChartTypeState] = useState('Vùng');
    const [labelData, setLabelData] = useState([
        "Doanh nghiệp  có tuân  thủ quy  định trong  việc tổ  chức tự  kiểm tra  và báo  cáo về  an toàn,  vệ sinh  lao động  định kỳ  không? ", 
        "Doanh nghiệp  có tuân  thủ quy  định trong  việc Tổ  chức quan  trắc môi  trường lao  động ít  nhất 01  lần trong  một năm  không?",
        "Doanh nghiệp  có tuân  thủ quy  định về  quản lý,  điều tra,  khai báo  và phòng  ngừa TNLĐ  không?"
    ]);
    const [veryAgreeData, setVeryAgreeData] = useState();
    const [agreeData, setAgreeData] = useState();
    const [middleData, setMiddleData] = useState([0,0,0,0,0,0,0,0]);
    const [disAgreeData, setDisAgreeData] = useState();
    const [veryDisAgreeData, setVeryDisAgreeData] = useState([0,0,0,0,0,0,0,0]);
    const [dontKnowData, setDontKnowData] = useState();
    {/*dropdown area*/}
    const ddchartType = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
            <option className='w3-animate-right dropdownElement' onClick={()=>{setChartTypeState('')}}>-------------------</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Vùng</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Đường</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Cột</option>
        </ul>
    </div>
    );}
    //set Data và Label
    useEffect(()=>{
      //set Data for this component
        let componentData = dataAllFiltered.filter(data => {
            return data.groupId.$oid === "5ecf63e7329e33023ffd9fa4"
        })
      //set Data for veryAgreeData
        let veryAgree = []; 
        let cVALSI77 = 0, cVALSI712 = 0, cVALSI7142 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc833329e33023ffd8943" && answer.questionCode === "LSI7.7") {
              cVALSI77++;
            }
            if (answer.answerId.$oid === "5eccc833329e33023ffd890b" && answer.questionCode === "LSI7.12") {
              cVALSI712++;
            }
            if (answer.answerId.$oid === "5eccc833329e33023ffd88eb" && answer.questionCode === "LSI7.14.2") {
              cVALSI7142++;
            }
          })
        })
        veryAgree.push(cVALSI77, cVALSI712, cVALSI7142);
        console.log(veryAgree);
        setVeryAgreeData([...veryAgree]);
      //set Data for agreeData
        let agree = []; 
        let cALSI77 = 0, cALSI712 = 0, cALSI7142 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc833329e33023ffd8923" && answer.questionCode === "LSI7.7") {
              cALSI77++;
            }
            if (answer.answerId.$oid === "5eccc833329e33023ffd88fb" && answer.questionCode === "LSI7.12") {
              cALSI712++;
            }
            if (answer.answerId.$oid === "5eccc833329e33023ffd896f" && answer.questionCode === "LSI7.14.2") {
              cALSI7142++;
            }
          })
        })
        agree.push(cALSI77, cALSI712, cALSI7142);
        console.log(agree);
        setAgreeData([...agree]);
      //set Data for disAgreeData
        let disAgree = []; 
        let cDALSI77 = 0, cDALSI712 = 0, cDALSI7142 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc833329e33023ffd89ab" && answer.questionCode === "LSI7.7") {
              cDALSI77++;
            }
            if (answer.answerId.$oid === "5eccc833329e33023ffd898f" && answer.questionCode === "LSI7.12") {
              cDALSI712++;
            }
            if (answer.answerId.$oid === "5eccc833329e33023ffd88db" && answer.questionCode === "LSI7.14.2") {
              cDALSI7142++;
            }
          })
        })
        disAgree.push(cDALSI77, cDALSI712, cDALSI7142);
        console.log(disAgree);
        setDisAgreeData([...disAgree]);
      //set Data for dontKnowData
        let dontKnow = []; 
        let cDKLSI77 = 0, cDKLSI712 = 0, cDKLSI7142 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc833329e33023ffd88e7" && answer.questionCode === "LSI7.7") {
              cDKLSI77++;
            }
            if (answer.answerId.$oid === "5eccc833329e33023ffd89a3" && answer.questionCode === "LSI7.12") {
              cDKLSI712++;
            }
            if (answer.answerId.$oid === "5eccc833329e33023ffd89cb" && answer.questionCode === "LSI7.14.2") {
              cDKLSI7142++;
            }
          })
        })
        dontKnow.push(cDKLSI77, cDKLSI712, cDKLSI7142);
        console.log(dontKnow);
        setDontKnowData([...dontKnow]);
    }, [dataAllFiltered])

    //LineFill
    const dataLineFill = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: true,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: true,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: true,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: true,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: true,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: true,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
      };
    //Line  
    const dataLine = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: false,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: false,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: false,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: false,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: false,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: false,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
    };
    //Bar
    const dataBar = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            data: veryAgreeData,
            borderColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              ],
            backgroundColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
            ],
          },
          {
            label: "Đồng Ý",
            data: agreeData,
            borderColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
          },
          {
            label: "Trung Lập",
            data: middleData,
            borderColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
          },
          {
            label: "Không Đồng Ý",
            data: disAgreeData,
            borderColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
            backgroundColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
          },
          {
            label: "Rất Không Đồng Ý",
            data: veryDisAgreeData,
            borderColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
            backgroundColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
          },
          {
            label: "Không Rõ",
            data: dontKnowData,
            borderColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
            backgroundColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
          },
        ],
    }
    const options = {
        title: {
          display: true,
          text: "AN TOÀN VỆ SINH LAO ĐỘNG",
          responsive: true,
          maintainAspectRatio: false,
        },
        pointLabelFontSize : 10,
        scales: {         
            ticks: { beginAtZero: true },
            xAxes: [
              {
                ticks: {
                  callback: function(label) {
                    if (/\s/.test(label)) {
                      return label.split("  ");
                    }else{
                      return label;
                    }              
                  }
                },
              },
            ],
          }
    };
    const optionsBar = {
      title: {
        display: true,
        text: "AN TOÀN VỆ SINH LAO ĐỘNG",
        responsive: true,
        maintainAspectRatio: false,
      },
      pointLabelFontSize : 10,
      scales: {   
          ticks: { beginAtZero: true },      
          xAxes: [
            {
              ticks: {
                callback: function(label) {
                  if (/\s/.test(label)) {
                    return label.split("  ");
                  }else{
                    return label;
                  }              
                }
              },
              stacked: true,
            },
          ],
          yAxes: [
            {
              stacked: true,
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        }
  };
    
    const chartControl = () => {
        switch (chartTypeState) {
            case 'Vùng':
                return <Line data={dataLineFill} options={options} height={210}/>
            case 'Đường':
                return <Line data={dataLine} options={options} height={210}/>
            case 'Cột':
                return<Bar data={dataBar} options={optionsBar} height={210}/>
            default:
                return null
        }
    }
    return (
        <div>
            {/*chart type*/}
            <div className="chartType">
                <div className="row">
                <div
                    className='chartTypeState'
                >
                    {chartTypeState}
                </div>
                <div
                    className='chartTypeDD'
                >
                    {ddchartType()}
                </div>
                </div>
            </div>
            {/*chart*/}
            <div className='chart'>{chartControl()}</div>
        </div>
    );
}

export default ATVSLD;
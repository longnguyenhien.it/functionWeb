import React, {useEffect, useState} from 'react';
import { Line, Bar } from "react-chartjs-2";
import { defaults } from 'react-chartjs-2';


defaults.global.defaultFontColor = '#5D54A4';


function Contract(props) {
    const {dataAllFiltered} = props
    const [chartTypeState, setChartTypeState] = useState('Vùng');
    const [labelData, setLabelData] = useState([
        "Báo cáo  định kỳ  về tình  hình tuyển  dụng và  nhu cầu  sử dụng  lao động", 
        "Quản lý  An toàn  vệ sinh  lao động  và báo  cáo định  kỳ về  tình hình  tai nạn  lao động",
        "Hợp đồng  thử việc",
        "Thời hạn  báo trước  khi đơn  phương chấm  dứt HĐLĐ  với NLĐ",
        "Thời điểm  chấm dứt  HĐLĐ đối  với Cán  bộ Công  đoàn chuyên  trách",
        "Quản lý  giấy tờ  tùy thân  hồ sơ  của NLĐ",
        "Đảm bảo  không cưỡng  ép lao  động",
        "Đào tạo  bồi dưỡng  nâng cao  trình độ  kỹ năng  nghề cho  NLĐ"
    ]);
    const [veryAgreeData, setVeryAgreeData] = useState();
    const [agreeData, setAgreeData] = useState();
    const [middleData, setMiddleData] = useState([0,0,0,0,0,0,0,0]);
    const [disAgreeData, setDisAgreeData] = useState();
    const [veryDisAgreeData, setVeryDisAgreeData] = useState([0,0,0,0,0,0,0,0]);
    const [dontKnowData, setDontKnowData] = useState();



    {/*dropdown area*/}
    const ddchartType = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
            <option className='w3-animate-right dropdownElement' onClick={()=>{setChartTypeState('')}}>-------------------</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Vùng</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Đường</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Cột</option>
        </ul>
    </div>
    );}
    //set Data và Label
    useEffect(()=>{
      //set Data for this component
        let componentData = dataAllFiltered.filter(data => {
            return data.groupId.$oid === "5ec335aa329e33023ffd6f77"
        })
      //set Data for veryAgreeData
        let veryAgree = []; 
        let cVALSI11 = 0, cVALSI12 = 0, cVALSI221 = 0, cVALSI223 = 0, cVALSI224 = 0, cVALSI225 = 0, cVALSI226 = 0, cVALSI227 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5ec338e5329e33023ffd737f" && answer.questionCode === "LSI1.1") {
              cVALSI11++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd7363" && answer.questionCode === "LSI1.2") {
              cVALSI12++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd72e7" && answer.questionCode === "LSI2.2.1") {
              cVALSI221++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74d8" && answer.questionCode === "LSI2.2.3") {
              cVALSI223++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74f4" && answer.questionCode === "LSI2.2.4") {
              cVALSI224++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74d4" && answer.questionCode === "LSI2.2.5") {
              cVALSI225++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd7500" && answer.questionCode === "LSI2.2.6") {
              cVALSI226++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74cc" && answer.questionCode === "LSI2.2.7") {
              cVALSI227++;
            }
          })
        })
        veryAgree.push(cVALSI11, cVALSI12, cVALSI221, cVALSI223, cVALSI224, cVALSI225, cVALSI226, cVALSI227);
        console.log(veryAgree);
        setVeryAgreeData([...veryAgree]);
      //set Data for agreeData
        let agree = []; 
        let cALSI11 = 0, cALSI12 = 0, cALSI221 = 0, cALSI223 = 0, cALSI224 = 0, cALSI225 = 0, cALSI226 = 0, cALSI227 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5ec338e5329e33023ffd738b" && answer.questionCode === "LSI1.1") {
              cALSI11++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd739f" && answer.questionCode === "LSI1.2") {
              cALSI12++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd732b" && answer.questionCode === "LSI2.2.1") {
              cALSI221++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74dc" && answer.questionCode === "LSI2.2.3") {
              cALSI223++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74b4" && answer.questionCode === "LSI2.2.4") {
              cALSI224++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74fc" && answer.questionCode === "LSI2.2.5") {
              cALSI225++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74b8" && answer.questionCode === "LSI2.2.6") {
              cALSI226++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74c0" && answer.questionCode === "LSI2.2.7") {
              cALSI227++;
            }
          })
        })
        agree.push(cALSI11, cALSI12, cALSI221, cALSI223, cALSI224, cALSI225, cALSI226, cALSI227);
        console.log(agree);
        setAgreeData([...agree]);
      //set Data for disAgreeData
        let disAgree = []; 
        let cDALSI11 = 0, cDALSI12 = 0, cDALSI221 = 0, cDALSI223 = 0, cDALSI224 = 0, cDALSI225 = 0, cDALSI226 = 0, cDALSI227 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5ec338e5329e33023ffd731f" && answer.questionCode === "LSI1.1") {
              cDALSI11++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd7377" && answer.questionCode === "LSI1.2") {
              cDALSI12++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd72be" && answer.questionCode === "LSI2.2.1") {
              cDALSI221++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74e0" && answer.questionCode === "LSI2.2.3") {
              cDALSI223++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74ac" && answer.questionCode === "LSI2.2.4") {
              cDALSI224++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74e4" && answer.questionCode === "LSI2.2.5") {
              cDALSI225++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74bc" && answer.questionCode === "LSI2.2.6") {
              cDALSI226++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74c8" && answer.questionCode === "LSI2.2.7") {
              cDALSI227++;
            }
          })
        })
        disAgree.push(cDALSI11, cDALSI12, cDALSI221, cDALSI223, cDALSI224, cDALSI225, cDALSI226, cDALSI227);
        console.log(disAgree);
        setDisAgreeData([...disAgree]);
      //set Data for dontKnowData
        let dontKnow = []; 
        let cDKLSI11 = 0, cDKLSI12 = 0, cDKLSI221 = 0, cDKLSI223 = 0, cDKLSI224 = 0, cDKLSI225 = 0, cDKLSI226 = 0, cDKLSI227 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5ec338e5329e33023ffd7347" && answer.questionCode === "LSI1.1") {
              cDKLSI11++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd733b" && answer.questionCode === "LSI1.2") {
              cDKLSI12++;
            }
            if (answer.answerId.$oid === "5ec338e5329e33023ffd734f" && answer.questionCode === "LSI2.2.1") {
              cDKLSI221++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74f8" && answer.questionCode === "LSI2.2.3") {
              cDKLSI223++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74b0" && answer.questionCode === "LSI2.2.4") {
              cDKLSI224++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd7504" && answer.questionCode === "LSI2.2.5") {
              cDKLSI225++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd74c4" && answer.questionCode === "LSI2.2.6") {
              cDKLSI226++;
            }
            if (answer.answerId.$oid === "5ec33af4329e33023ffd7508" && answer.questionCode === "LSI2.2.7") {
              cDKLSI227++;
            }
          })
        })
        dontKnow.push(cDKLSI11, cDKLSI12, cDKLSI221, cDKLSI223, cDKLSI224, cDKLSI225, cDKLSI226, cDKLSI227);
        console.log(dontKnow);
        setDontKnowData([...dontKnow]);
    }, [dataAllFiltered])

    //LineFill
    const dataLineFill = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: true,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: true,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: true,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: true,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: true,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: true,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
      };
    //Line  
    const dataLine = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: false,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: false,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: false,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: false,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: false,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: false,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
    };
    //Bar
    const dataBar = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            data: veryAgreeData,
            borderColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              ],
            backgroundColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
            ],
          },
          {
            label: "Đồng Ý",
            data: agreeData,
            borderColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
          },
          {
            label: "Trung Lập",
            data: middleData,
            borderColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
          },
          {
            label: "Không Đồng Ý",
            data: disAgreeData,
            borderColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
            backgroundColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
          },
          {
            label: "Rất Không Đồng Ý",
            data: veryDisAgreeData,
            borderColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
            backgroundColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
          },
          {
            label: "Không Rõ",
            data: dontKnowData,
            borderColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
            backgroundColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
          },
        ],
    }
    const options = {
        ticks: { beginAtZero: true },
        title: {
          display: true,
          text: "HỢP ĐỒNG LAO ĐỘNG",
          responsive: true,
          maintainAspectRatio: false,
        },
        pointLabelFontSize : 10,
        scales: {         
            xAxes: [
              {
                ticks: {
                  callback: function(label) {
                    if (/\s/.test(label)) {
                      return label.split("  ");
                    }else{
                      return label;
                    }              
                  }
                },
              },
            ],
          }
    };
    const optionsBar = {
      title: {
        display: true,
        text: "HỢP ĐỒNG LAO ĐỘNG",
        responsive: true,
        maintainAspectRatio: false,
      },
      pointLabelFontSize : 10,
      scales: {    
          ticks: { beginAtZero: true },     
          xAxes: [
            {
              ticks: {
                callback: function(label) {
                  if (/\s/.test(label)) {
                    return label.split("  ");
                  }else{
                    return label;
                  }              
                }
              },
              stacked: true,
            },
          ],
          yAxes: [
            {
              stacked: true,
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        }
  };
    
    const chartControl = () => {
        switch (chartTypeState) {
            case 'Vùng':
                return <Line data={dataLineFill} options={options} height={210}/>
            case 'Đường':
                return <Line data={dataLine} options={options} height={210}/>
            case 'Cột':
                return<Bar data={dataBar} options={optionsBar} height={210}/>
            default:
                return null
        }
    }
    return (
        <div>
            {/*chart type*/}
            <div className="chartType">
                <div className="row">
                <div
                    className='chartTypeState'
                >
                    {chartTypeState}
                </div>
                <div
                    className='chartTypeDD'
                >
                    {ddchartType()}
                </div>
                </div>
            </div>
            {/*chart*/}
            <div className='chart'>{chartControl()}</div>
        </div>
    );
}

export default Contract;
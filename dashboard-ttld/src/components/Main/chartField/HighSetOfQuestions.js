import React, {useEffect, useState} from 'react';
import { Radar } from "react-chartjs-2";
import { defaults } from 'react-chartjs-2';
import DataGroupId from '../Datas/BTWDataGroupId';
import _ from 'lodash';
import reportTitle from '../Datas/ReportTitle';


defaults.global.defaultFontColor = '#5D54A4';


function HighSetOfQuestion(props) {
    const {dataAllFiltered} = props
    const [chartTypeState, setChartTypeState] = useState('Radar');
    const [labelData, setLabelData] = useState();
    const [chartData, setChartData] = useState();


    {/*dropdown area*/}
    const ddchartType = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
            <option className='w3-animate-right dropdownElement' onClick={()=>{setChartTypeState('')}}>-------------------</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Radar</option>
        </ul>
    </div>
    );}
    //set Data và Label
    useEffect(()=>{
        //set Data for this Component
        let dataComponent = []
        dataAllFiltered.map((data)=>{
            reportTitle.map((report)=>{
                if (report.groupId === data.groupId.$oid) {
                    dataComponent.push(report);
                }
            })
        })
        console.log(dataComponent);
        //count duplicate groupId
        let result = [...dataComponent.reduce((mp, o) => {
            if (!mp.has(o.groupId)) mp.set(o.groupId, Object.assign({ count: 0 }, o)); // has: false, set: new array, assign: copy to {count: 0} o value
            mp.get(o.groupId).count++;
            return mp;
            }, new Map).values()];
            result = _.orderBy(result, 'count', 'desc');
            console.log(result);
            result = result.slice(0,5);
            console.log(result);
        //set Data for Label
        let label = [] ;
            result.map(r=>{
                label.push(r.abb);
            })
            setLabelData([...label]);
        console.log(label);
        //set Data for Chart data
        let chart = [];
            result.map(r => {
                chart.push(r.count)
            })
            setChartData([...chart])
        console.log(chart);
    }, [dataAllFiltered])

    //Radar  
    const dataRadar = {
        labels: labelData,
        datasets: [
          {
            label: "Bộ Câu Hỏi Được Trả Lời Nhiều Nhất",
            data: chartData,
            borderColor: "rgba(153, 102, 255, 0.5)",
            backgroundColor: "rgba(0.153, 102, 255, 0.5 )",
            borderWidth: 1,
          },
        ],
    };
    const options = {
        title: {
          display: true,
          text: "BỘ CÂU HỎI ĐƯỢC TRẢ LỜI NHIỀU NHẤT",
        },
        scale: {
            ticks: { beginAtZero: true },
          },
    };


    const chartControl = () => {
        switch (chartTypeState) {
            case 'Radar':
                return <Radar data={dataRadar} options={options}/>
            case 'Đường':
                return null
            default:
                return null
        }
    }
    return (
        <div>
            {/*chart type*/}
            <div className="chartType">
                <div className="row">
                <div
                    className='chartTypeState'
                >
                    {chartTypeState}
                </div>
                <div
                    className='chartTypeDD'
                >
                    {ddchartType()}
                </div>
                </div>
            </div>
            {/*chart*/}
            <div className='chart'>{chartControl()}</div>
        </div>
    );
}

export default HighSetOfQuestion;
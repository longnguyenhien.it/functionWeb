import React, {useEffect, useState} from 'react';
import { Line, Bar } from "react-chartjs-2";
import { defaults } from 'react-chartjs-2';


defaults.global.defaultFontColor = '#5D54A4';


function PCCC(props) {
    const {dataAllFiltered} = props
    const [chartTypeState, setChartTypeState] = useState('Vùng');
    const [labelData, setLabelData] = useState([
        "Doanh nghiệp  có tuân  thủ quy  định về  quản lý,  xây dựng  nội quy  về an  toàn phòng  cháy chữa  cháy không?", 
        "Doanh nghiệp  có tuân  thủ quy  định về  xây dựng  các biện  pháp về  phòng cháy,  cũng như  diễn tập  định kỳ  theo quy  định không?",
        "Doanh nghiệp  có thành  lập đội  phòng cháy  và chữa  cháy cơ  sở theo  đúng quy  định không?",
        "Doanh nghiệp  có Lập  và phê  duyệt  các phương  án chữa  cháy không?",
        "Doanh nghiệp  có thực  hiện kiểm  tra an  toàn về  phòng cháy  và chữa  cháy với  sự tham  gia của  cơ quan  chức năng  theo quy  định không?",
        "Doanh nghiệp  có tuân  thủ quy  định về  việc theo  dõi công  tác tuyên  truyền, bồi  dưỡng, huấn  luyện nghiệp  vụ phòng  cháy và  chữa cháy  cũng như  hoạt động  của đội  phòng cháy  chữa cháy  không?",
        "Doanh nghiệp  có tuân  thủ quy  định về  quản lý  và theo  dõi phương  tiện phòng  cháy và  chữa cháy  không?",
        "Doanh nghiệp  có tuân  thủ quy  định về  cửa, lối  thoát hiểm,  hành lang  an toàn  cháy không?"
    ]);
    const [veryAgreeData, setVeryAgreeData] = useState();
    const [agreeData, setAgreeData] = useState();
    const [middleData, setMiddleData] = useState([0,0,0,0,0,0,0,0]);
    const [disAgreeData, setDisAgreeData] = useState();
    const [veryDisAgreeData, setVeryDisAgreeData] = useState([0,0,0,0,0,0,0,0]);
    const [dontKnowData, setDontKnowData] = useState();
    {/*dropdown area*/}
    const ddchartType = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
            <option className='w3-animate-right dropdownElement' onClick={()=>{setChartTypeState('')}}>-------------------</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Vùng</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Đường</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Cột</option>
        </ul>
    </div>
    );}
    //set Data và Label
    useEffect(()=>{
      //set Data for this component
        let componentData = dataAllFiltered.filter(data => {
            return data.groupId.$oid === "5ecf5ca6329e33023ffd98e2"
        })
      //set Data for veryAgreeData 1
        let veryAgree = []; 
        let cVALSI81 = 0, cVALSI82 = 0, cVALSI83 = 0, cVALSI84 = 0, cVALSI85 = 0, cVALSI86 = 0, cVALSI87 = 0, cVALSI88 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84cf" && answer.questionCode === "LSI8.1") {
              cVALSI81++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84df" && answer.questionCode === "LSI8.2") {
              cVALSI82++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84e7" && answer.questionCode === "LSI8.3") {
              cVALSI83++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84ef" && answer.questionCode === "LSI8.4") {
              cVALSI84++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd852b" && answer.questionCode === "LSI8.5") {
              cVALSI85++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84f3" && answer.questionCode === "LSI8.6") {
              cVALSI86++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd851f" && answer.questionCode === "LSI8.7") {
              cVALSI87++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84ff" && answer.questionCode === "LSI8.8") {
              cVALSI88++;
            }
          })
        })
        veryAgree.push(cVALSI81, cVALSI82, cVALSI83, cVALSI84, cVALSI85, cVALSI86, cVALSI87, cVALSI88);
        console.log(veryAgree);
        setVeryAgreeData([...veryAgree]);
      //set Data for agreeData 3
        let agree = []; 
        let cALSI81 = 0, cALSI82 = 0, cALSI83 = 0, cALSI84 = 0, cALSI85 = 0, cALSI86 = 0, cALSI87 = 0, cALSI88 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84cb" && answer.questionCode === "LSI8.1") {
              cALSI81++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd8513" && answer.questionCode === "LSI8.2") {
              cALSI82++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84e3" && answer.questionCode === "LSI8.3") {
              cALSI83++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84bf" && answer.questionCode === "LSI8.4") {
              cALSI84++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84f7" && answer.questionCode === "LSI8.5") {
              cALSI85++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84db" && answer.questionCode === "LSI8.6") {
              cALSI86++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd850f" && answer.questionCode === "LSI8.7") {
              cALSI87++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84c7" && answer.questionCode === "LSI8.8") {
              cALSI88++;
            }
          })
        })
        agree.push(cALSI81, cALSI82, cALSI83, cALSI84, cALSI85, cALSI86, cALSI87, cALSI88);
        console.log(agree);
        setAgreeData([...agree]);
      //set Data for disAgreeData 2
        let disAgree = []; 
        let cDALSI81 = 0, cDALSI82 = 0, cDALSI83 = 0, cDALSI84 = 0, cDALSI85 = 0, cDALSI86 = 0, cDALSI87 = 0, cDALSI88 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84c3" && answer.questionCode === "LSI8.1") {
              cDALSI81++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84b3" && answer.questionCode === "LSI8.2") {
              cDALSI82++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84eb" && answer.questionCode === "LSI8.3") {
              cDALSI83++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84bb" && answer.questionCode === "LSI8.4") {
              cDALSI84++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84d3" && answer.questionCode === "LSI8.5") {
              cDALSI85++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd8517" && answer.questionCode === "LSI8.6") {
              cDALSI86++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84d7" && answer.questionCode === "LSI8.7") {
              cDALSI87++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd8503" && answer.questionCode === "LSI8.8") {
              cDALSI88++;
            }
          })
        })
        disAgree.push(cDALSI81, cDALSI82, cDALSI83, cDALSI84, cDALSI85, cDALSI86, cDALSI87, cDALSI88);
        console.log(disAgree);
        setDisAgreeData([...disAgree]);
      //set Data for dontKnowData 4
        let dontKnow = []; 
        let cDKLSI81 = 0, cDKLSI82 = 0, cDKLSI83 = 0, cDKLSI84 = 0, cDKLSI85 = 0, cDKLSI86 = 0, cDKLSI87 = 0, cDKLSI88 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84fb" && answer.questionCode === "LSI8.1") {
              cDKLSI81++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84af" && answer.questionCode === "LSI8.2") {
              cDKLSI82++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd8507" && answer.questionCode === "LSI8.3") {
              cDKLSI83++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd84b7" && answer.questionCode === "LSI8.4") {
              cDKLSI84++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd850b" && answer.questionCode === "LSI8.5") {
              cDKLSI85++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd851b" && answer.questionCode === "LSI8.6") {
              cDKLSI86++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd8527" && answer.questionCode === "LSI8.7") {
              cDKLSI87++;
            }
            if (answer.answerId.$oid === "5eccc18c329e33023ffd8523" && answer.questionCode === "LSI8.8") {
              cDKLSI88++;
            }
          })
        })
        dontKnow.push(cDKLSI81, cDKLSI82, cDKLSI83, cDKLSI84, cDKLSI85, cDKLSI86, cDKLSI87, cDKLSI88);
        console.log(dontKnow);
        setDontKnowData([...dontKnow]);
    }, [dataAllFiltered])

    //LineFill
    const dataLineFill = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: true,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: true,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: true,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: true,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: true,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: true,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
      };
    //Line  
    const dataLine = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: false,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: false,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: false,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: false,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: false,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: false,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
    };
    //Bar
    const dataBar = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            data: veryAgreeData,
            borderColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              ],
            backgroundColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
            ],
          },
          {
            label: "Đồng Ý",
            data: agreeData,
            borderColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
          },
          {
            label: "Trung Lập",
            data: middleData,
            borderColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
          },
          {
            label: "Không Đồng Ý",
            data: disAgreeData,
            borderColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
            backgroundColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
          },
          {
            label: "Rất Không Đồng Ý",
            data: veryDisAgreeData,
            borderColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
            backgroundColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
          },
          {
            label: "Không Rõ",
            data: dontKnowData,
            borderColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
            backgroundColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
          },
        ],
    }
    const options = {
        title: {
          display: true,
          text: "PHÒNG CHÁY CHỮA CHÁY",
          responsive: true,
          maintainAspectRatio: false,
        },
        pointLabelFontSize : 10,
        scales: {         
            xAxes: [
              {
                ticks: {
                  callback: function(label) {
                    if (/\s/.test(label)) {
                      return label.split("  ");
                    }else{
                      return label;
                    }              
                  }
                },
              },
            ],
          }
    };
    const optionsBar = {
      title: {
        display: true,
        text: "PHÒNG CHÁY CHỮA CHÁY",
        responsive: true,
        maintainAspectRatio: false,
      },
      pointLabelFontSize : 10,
      scales: {   
        ticks: { beginAtZero: true },      
          xAxes: [
            {
              ticks: {
                callback: function(label) {
                  if (/\s/.test(label)) {
                    return label.split("  ");
                  }else{
                    return label;
                  }              
                }
              },
              stacked: true,
            },
          ],
          yAxes: [
            {
              stacked: true,
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        }
  };
    
    const chartControl = () => {
        switch (chartTypeState) {
            case 'Vùng':
                return <Line data={dataLineFill} options={options} height={210}/>
            case 'Đường':
                return <Line data={dataLine} options={options} height={210}/>
            case 'Cột':
                return<Bar data={dataBar} options={optionsBar} height={210}/>
            default:
                return null
        }
    }
    return (
        <div>
            {/*chart type*/}
            <div className="chartType">
                <div className="row">
                <div
                    className='chartTypeState'
                >
                    {chartTypeState}
                </div>
                <div
                    className='chartTypeDD'
                >
                    {ddchartType()}
                </div>
                </div>
            </div>
            {/*chart*/}
            <div className='chart'>{chartControl()}</div>
        </div>
    );
}

export default PCCC;
import React, {useEffect, useState} from 'react';
import { Line, Bar } from "react-chartjs-2";
import { defaults } from 'react-chartjs-2';


defaults.global.defaultFontColor = '#5D54A4';


function BHXH(props) {
    const {dataAllFiltered} = props
    const [chartTypeState, setChartTypeState] = useState('Vùng');
    const [labelData, setLabelData] = useState([
        "Doanh nghiệp  có tuân  thủ quy  định về  tham gia  BHXH cho  NLĐ không?", 
        "Doanh nghiệp  có tuân  thủ quy  định về  tham gia  BHTN cho  NLĐ không?",
        "Doanh nghiệp  có tuân  thủ quy  định về  tham gia  BHYT cho  NLĐ không?",
        "Doanh nghiệp  có tuân  thủ quy  định về  thời gian  trích nộp  BH theo  quy định  không?"
    ]);
    const [veryAgreeData, setVeryAgreeData] = useState();
    const [agreeData, setAgreeData] = useState();
    const [middleData, setMiddleData] = useState([0,0,0,0,0,0,0,0]);
    const [disAgreeData, setDisAgreeData] = useState();
    const [veryDisAgreeData, setVeryDisAgreeData] = useState([0,0,0,0,0,0,0,0]);
    const [dontKnowData, setDontKnowData] = useState();
    {/*dropdown area*/}
    const ddchartType = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
            <option className='w3-animate-right dropdownElement' onClick={()=>{setChartTypeState('')}}>-------------------</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Vùng</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Đường</option>
            <option className='w3-animate-right dropdownElement' onClick={(e)=>{setChartTypeState(e.target.value)}}>Cột</option>
        </ul>
    </div>
    );}
    //set Data và Label
    useEffect(()=>{
      //set Data for this component
        let componentData = dataAllFiltered.filter(data => {
            return data.groupId.$oid === "5ecf5d54329e33023ffd9af6"
        })
      //set Data for veryAgreeData 1
        let veryAgree = []; 
        let cVALSI101 = 0, cVALSI102 = 0, cVALSI103 = 0, cVALSI104 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd870e" && answer.questionCode === "LSI10.1") {
              cVALSI101++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8676" && answer.questionCode === "LSI10.2") {
              cVALSI102++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86be" && answer.questionCode === "LSI10.3") {
              cVALSI103++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86da" && answer.questionCode === "LSI10.4") {
              cVALSI104++;
            }
          })
        })
        veryAgree.push(cVALSI101, cVALSI102, cVALSI103, cVALSI104);
        console.log(veryAgree);
        setVeryAgreeData([...veryAgree]);
      //set Data for agreeData 3
        let agree = []; 
        let cALSI101 = 0, cALSI102 = 0, cALSI103 = 0, cALSI104 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86b2" && answer.questionCode === "LSI10.1") {
              cALSI101++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86b6" && answer.questionCode === "LSI10.2") {
              cALSI102++;
            }
            if (answer.answerId.$oid === "5eccc1f5329e33023ffd8736" && answer.questionCode === "LSI10.3") {
              cALSI103++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86ca" && answer.questionCode === "LSI10.4") {
              cALSI104++;
            }
          })
        })
        agree.push(cALSI101, cALSI102, cALSI103, cALSI104);
        console.log(agree);
        setAgreeData([...agree]);
      //set Data for disAgreeData 2
        let disAgree = []; 
        let cDALSI101 = 0, cDALSI102 = 0, cDALSI103 = 0, cDALSI104 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86fa" && answer.questionCode === "LSI10.1") {
              cDALSI101++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86d2" && answer.questionCode === "LSI10.2") {
              cDALSI102++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8706" && answer.questionCode === "LSI10.3") {
              cDALSI103++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8722" && answer.questionCode === "LSI10.4") {
              cDALSI104++;
            }
          })
        })
        disAgree.push(cDALSI101, cDALSI102, cDALSI103, cDALSI104);
        console.log(disAgree);
        setDisAgreeData([...disAgree]);
      //set Data for dontKnowData 4
        let dontKnow = []; 
        let cDKLSI101 = 0, cDKLSI102 = 0, cDKLSI103 = 0, cDKLSI104 = 0;
        componentData.map(data => {
          data.feedback.map(answer => {
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8702" && answer.questionCode === "LSI10.1") {
              cDKLSI101++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd8716" && answer.questionCode === "LSI10.2") {
              cDKLSI102++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd871a" && answer.questionCode === "LSI10.3") {
              cDKLSI103++;
            }
            if (answer.answerId.$oid === "5eccc1f4329e33023ffd86f2" && answer.questionCode === "LSI10.4") {
              cDKLSI104++;
            }
          })
        })
        dontKnow.push(cDKLSI101, cDKLSI102, cDKLSI103, cDKLSI104);
        console.log(dontKnow);
        setDontKnowData([...dontKnow]);
    }, [dataAllFiltered])

    //LineFill
    const dataLineFill = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: true,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: true,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: true,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: true,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: true,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: true,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
      };
    //Line  
    const dataLine = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            fill: false,
            data: veryAgreeData,
            borderColor: ["rgba(153, 102, 255, 0.3)"],
            backgroundColor: ["rgba(153, 102, 255, 0.3)"],
            pointbackgroundColor: "rgba(153, 102, 255, 0.3)",
            pointborderColor: "rgba(153, 102, 255, 0.3)",
          },
          {
            label: "Đồng Ý",
            fill: false,
            data: agreeData,
            borderColor: ["rgba(255, 99, 132, 0.3)"],
            backgroundColor: ["rgba(255, 99, 132, 0.3)"],
            pointbackgroundColor: "rgba(255, 99, 132, 0.3)",
            pointborderColor: "rgba(255, 99, 132, 0.3)",
          },
          {
            label: "Trung Lập",
            fill: false,
            data: middleData,
            borderColor: ["rgba(255, 205, 86, 0.3)"],
            backgroundColor: ["rgba(255, 205, 86, 0.3)"],
            pointbackgroundColor: "rgba(255, 205, 86, 0.3)",
            pointborderColor: "rgba(255, 205, 86, 0.3)",
          },
          {
            label: "Không Đồng Ý",
            fill: false,
            data: disAgreeData,
            borderColor: ["rgba(54, 162, 235, 0.3)"],
            backgroundColor: ["rgba(54, 162, 235, 0.3)"],
            pointbackgroundColor: "rgba(54, 162, 235, 0.3)",
            pointborderColor: "rgba(54, 162, 235, 0.3)",
          },
          {
            label: "Rất Không Đồng Ý",
            fill: false,
            data: veryDisAgreeData,
            borderColor: ["rgba(94, 228, 127, 0.3)"],
            backgroundColor: ["rgba(94, 228, 127, 0.3)"],
            pointbackgroundColor: "rgba(94, 228, 127, 0.3)",
            pointborderColor: "rgba(94, 228, 127, 0.3)",
          },
          {
            label: "Không Rõ",
            fill: false,
            data: dontKnowData,
            borderColor: ["rgba(184, 228, 28, 0.3)"],
            backgroundColor: ["rgba(184, 228, 28, 0.3)"],
            pointbackgroundColor: "rgba(184, 228, 28, 0.3)",
            pointborderColor: "rgba(184, 228, 28, 0.3)",
          },
        ],
    };
    //Bar
    const dataBar = {
        labels: labelData,
        datasets: [
          {
            label: "Rất Đồng Ý",
            data: veryAgreeData,
            borderColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              ],
            backgroundColor: [
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
              "rgba(153, 102, 255, 0.3)",
            ],
          },
          {
            label: "Đồng Ý",
            data: agreeData,
            borderColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
              "rgba(255, 99, 132, 0.3)",
            ],
          },
          {
            label: "Trung Lập",
            data: middleData,
            borderColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
            backgroundColor: [
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
              "rgba(255, 205, 86, 0.3)",
            ],
          },
          {
            label: "Không Đồng Ý",
            data: disAgreeData,
            borderColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
            backgroundColor: [
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
              "rgba(54, 162, 235, 0.3)",
            ],
          },
          {
            label: "Rất Không Đồng Ý",
            data: veryDisAgreeData,
            borderColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
            backgroundColor: [
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
              "rgba(94, 228, 127, 0.3)",
          ],
          },
          {
            label: "Không Rõ",
            data: dontKnowData,
            borderColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
            backgroundColor: [
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
              "rgba(184, 228, 28, 0.3)",
            ],
          },
        ],
    }
    const options = {
        title: {
          display: true,
          text: "BẢO HIỂM XÃ HỘI, Y TẾ, TAI NẠN LAO ĐỘNG, BỆNH NGHỀ NGHIỆP",
          responsive: true,
          maintainAspectRatio: false,
        },
        pointLabelFontSize : 10,
        scales: {         
            ticks: { beginAtZero: true },
            xAxes: [
              {
                ticks: {
                  callback: function(label) {
                    if (/\s/.test(label)) {
                      return label.split("  ");
                    }else{
                      return label;
                    }              
                  }
                },
              },
            ],
          }
    };
    const optionsBar = {
      title: {
        display: true,
        text: "BẢO HIỂM XÃ HỘI, Y TẾ, TAI NẠN LAO ĐỘNG, BỆNH NGHỀ NGHIỆP",
        responsive: true,
        maintainAspectRatio: false,
      },
      pointLabelFontSize : 10,
      scales: {  
          ticks: { beginAtZero: true },       
          xAxes: [
            {
              ticks: {
                callback: function(label) {
                  if (/\s/.test(label)) {
                    return label.split("  ");
                  }else{
                    return label;
                  }              
                }
              },
              stacked: true,
            },
          ],
          yAxes: [
            {
              stacked: true,
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        }
  };
    
    const chartControl = () => {
        switch (chartTypeState) {
            case 'Vùng':
                return <Line data={dataLineFill} options={options} height={210}/>
            case 'Đường':
                return <Line data={dataLine} options={options} height={210}/>
            case 'Cột':
                return<Bar data={dataBar} options={optionsBar} height={210}/>
            default:
                return null
        }
    }
    return (
        <div>
            {/*chart type*/}
            <div className="chartType">
                <div className="row">
                <div
                    className='chartTypeState'
                >
                    {chartTypeState}
                </div>
                <div
                    className='chartTypeDD'
                >
                    {ddchartType()}
                </div>
                </div>
            </div>
            {/*chart*/}
            <div className='chart'>{chartControl()}</div>
        </div>
    );
}

export default BHXH;
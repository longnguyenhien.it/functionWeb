import React,{useState, useEffect} from 'react';
import axios from 'axios';
import companyTSIData from '../Datas/CompanyTypeSizeIndustry';

let industries = companyTSIData.filter(indus => {
  if (indus.type === 'COMPANY_INDUSTRY') {
    return true;
  }
  return false;
})

function IndustriesField(props) {
    const {setIndusValue} = props;

    const [searchTerm, setSearchTerm] = useState("");
    const [searchResults, setSearchResults] = useState([]);

//Search
useEffect(() => {
const results = industries.filter(industry => 
    industry.value.toLowerCase().includes(searchTerm.toLowerCase())  
);
setSearchResults(results);
}, [searchTerm]);
{/*fetching data*/}
// useEffect(()=>{
//     axios
//     .get('https://my-json-server.typicode.com/aegone-nguyen/testdb/industry')
//     .then((res)=>{
//         console.log(res);
//         setIndusData(res.data);
//     })
//     .catch((err)=>{
//         console.log(err);
//     })
//   },[])

{/*dropdown area*/}
const ddBtnIndus = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
        <option className='w3-animate-right dropdownElement' onClick={(e)=>{setSearchTerm('');setIndusValue('')}}>-------------</option>
        { 
            searchResults.map(indus => 
            <option key={indus._id.$oid} className='w3-animate-right dropdownElement' onClick={(e)=>{setSearchTerm(e.target.value); setIndusValue(indus._id.$oid)}}>
              {indus.value}
            </option>)
        }
        </ul>
    </div>
    );
}
const ddSearchIndus = () => {
    return(
      <div className="dropdown">
        <input  className='industryState'
                data-toggle="dropdown" 
                type="text" 
                placeholder="Search Industry..." 
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
        />
        <ul className="dropdown-menu dropdown-menu-left" id='area'>
          <option className='w3-animate-right dropdownElement' onClick={()=>{setSearchTerm('');setIndusValue('')}}>-------------</option>
          { 
            searchResults.map(indus => 
            <option key={indus._id.$oid} className='w3-animate-right dropdownElement' onClick={(e)=>{setSearchTerm(e.target.value);setIndusValue(indus._id.$oid)}}>
              {indus.value}
            </option>
            ) 
          }
        </ul>
      </div>
    );
  }
    return (
        <div>
            <h6>Ngành</h6>
            <div className="industry" >
            <div className="row">
                {ddSearchIndus()}
                <div className='industryDD'> 
                    {ddBtnIndus()}
                </div>
                </div>
            </div>   
        </div>
    );
}

export default IndustriesField;
import React, {useState, useEffect} from 'react';
import axios from 'axios';
import companyTSIData from '../Datas/CompanyTypeSizeIndustry';

let sizes = companyTSIData.filter(size => {
  if (size.type === 'COMPANY_SIZE') {
    return true;
  }
  return false;
})

function SizeField(props) {
  const {setSizeValue} = props;
    const [searchTerm, setSearchTerm] = useState("");
    const [searchResults, setSearchResults] = useState([]);

//Search
useEffect(() => {
const results = sizes.filter(size =>
    size.value.toLowerCase().includes(searchTerm.toLowerCase())  
);
setSearchResults(results);
}, [searchTerm]);
    {/*fetching data*/}
//     useEffect(()=>{
//     axios
//     .get('https://my-json-server.typicode.com/aegone-nguyen/testdb/headcount')
//     .then((res)=>{
//         console.log(res);
//         setSizeData(res.data);
//     })
//     .catch((err)=>{
//         console.log(err);
//     })
//   },[])

    {/*dropdown size*/}
    const ddBtnSize = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id='area'>
            <option className='w3-animate-right dropdownElement' onClick={()=>{setSearchTerm(''); setSizeValue('')}}>-------------</option>
            { 
                searchResults.map(size => 
                <option key={size._id.$oid} className='w3-animate-right dropdownElement' onClick={(e)=>{setSearchTerm(e.target.value); setSizeValue(size._id.$oid)}}>
                {size.value}
                </option>)
            }
        </ul>
    </div>
    );
    }
    const ddSearchSize = () => {
        return(
          <div className="dropdown">
            <input  className='sizeState'
                    data-toggle="dropdown" 
                    type="text" 
                    placeholder="Search Size..." 
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
            />
            <ul className="dropdown-menu dropdown-menu-left" id='area'>
              <option className='w3-animate-right dropdownElement' onClick={()=>{setSearchTerm('');setSizeValue('')}}>-------------</option>
              { 
                searchResults.map(size => 
                <option key={size._id.$oid} className='w3-animate-right dropdownElement' onClick={(e)=>{setSearchTerm(e.target.value);setSizeValue(size._id.$oid)}}>
                  {size.value}
                </option>
                ) 
              }
            </ul>
          </div>
        );
      }
    return (
        <div>
            <h6>Số lượng NS</h6>
            <div className="size" >
            <div className="row">
                {ddSearchSize()}
                <div className='sizeDD'> 
                {ddBtnSize()}
                </div>
                </div>
            </div>   
        </div>
    );
}

export default SizeField;
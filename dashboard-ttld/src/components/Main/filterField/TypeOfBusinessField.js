import React, {useState, useEffect} from 'react';
import axios from 'axios';
import companyTSIData from '../Datas/CompanyTypeSizeIndustry';

let typeOBFs = companyTSIData.filter(typeOBF => {
  if (typeOBF.type === 'COMPANY_TYPE') {
    return true;
  }
  return false;
})

function TypeOfBusinessField(props) {
  const {setTypeValue} = props;
    const [searchTerm, setSearchTerm] = useState("");
    const [searchResults, setSearchResults] = useState([]);

//Search
useEffect(() => {
    const results = typeOBFs.filter(typeOBF =>
      typeOBF.value.toLowerCase().includes(searchTerm.toLowerCase())  
    );
    setSearchResults(results);
}, [searchTerm]);

{/*fetching data*/}
// useEffect(()=>{
//     axios
//     .get('https://my-json-server.typicode.com/aegone-nguyen/testdb/company')
//     .then((res)=>{
//         console.log(res);
//         setTypeOBData(res.data);
//     })
//     .catch((err)=>{
//         console.log(err);
//     })
//   },[])

    {/*dropdown area*/}
    const ddBtnType = () => {
        return(
        <div className="dropdown">
            <button 
                className='btn-dropdown'
                data-toggle="dropdown">
                <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
            </button>
            <ul className="dropdown-menu dropdown-menu-right" id='area'>
                <option className='w3-animate-right dropdownElement' onClick={(e)=>{setSearchTerm(''); setTypeValue('')}}>-------------</option>
                { 
                searchResults.map(type => 
                <option key={type._id.$oid} className='w3-animate-right dropdownElement' onClick={(e)=>{setSearchTerm(e.target.value);setTypeValue(type._id.$oid)}}>
                {type.value}
                </option>)
                }
            </ul>
        </div>
        );
    }
    const ddSearchType = () => {
        return(
          <div className="dropdown">
            <input  className='typeOBState'
                    data-toggle="dropdown" 
                    type="text" 
                    placeholder="Search Area..." 
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
            />
            <ul className="dropdown-menu dropdown-menu-left" id='area'>
              <option className='w3-animate-right dropdownElement' onClick={()=>{setSearchTerm('');setTypeValue('')}}>-------------</option>
              { 
                searchResults.map(type => 
                <option key={type._id.$oid} className='w3-animate-right dropdownElement' onClick={(e)=>{setSearchTerm(e.target.value);setTypeValue(type._id.$oid)}}>
                  {type.value}
                </option>
                ) 
              }
            </ul>
          </div>
        );
      }
    return (
        <div >
            <h6>Loại hình doanh nghiệp</h6>
            <div className="typeOB" >
            <div className="row">
                {ddSearchType()}
                <div className='typeOBDD'> 
                {ddBtnType()}
                </div>
                </div>
            </div>   
        </div>
    );
}

export default TypeOfBusinessField;
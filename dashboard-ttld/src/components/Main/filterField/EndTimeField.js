import React, {useState, useEffect} from 'react';
import axios from 'axios';

function EndTimeField(props) {
const{timeName, setEndDate} = props;

const [searchTerm, setSearchTerm] = useState("");

const ddSearchTime = () => {
    return(
      <div className="dropdown">
        <input  className='timeState'
                type="date" 
                value={searchTerm}
                onChange={(e) => {setSearchTerm(e.target.value); setEndDate(e.target.value)}}
        />
      </div>
    );
}
    return (
        <div>
            <h6>{timeName}</h6>
            <div className="time" >
              <div className="row">
                {ddSearchTime()}
              </div>
            </div>   
        </div>
    );
}

export default EndTimeField;
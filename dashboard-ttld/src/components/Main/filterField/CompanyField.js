import React, { useState, useEffect } from "react";
import axios from "axios";
import DataCompanies from "../Datas/BTWDataComapanies";

function CompanyField(props) {
  const { areaValue, indusValue, sizeValue, typeValue, setDataComFiltered } = props;

  let dataFiltered = [];

  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [onClickReturn, setOnClickReturn] = useState(false);
  //const [dataFiltered, setDataFiltered] = useState([]);

const filterFunction = () => {
    //Filter theo Khu Vực
    let  areaCompanyData = DataCompanies.filter((data) => {
      if (areaValue === "") {
        return true;
      }
      if (
        areaValue !== "" &&
        ( 
          data.address.toLowerCase().includes(areaValue.nameVn.toLowerCase()) ||
          data.address.toLowerCase().includes(areaValue.nameEn.toLowerCase())
        ) &&
        ( 
          data.address.toLowerCase().includes(areaValue.cityVn.toLowerCase()) ||
          data.address.toLowerCase().includes(areaValue.cityEn.toLowerCase()) ||
          data.address.toLowerCase().includes(areaValue.cityAbb.toLowerCase())
        )
      ) {
        return true;
      }
      return false;
    });
    //Filter theo Ngành
    let industryCompanyData = areaCompanyData.filter((data) => {
      if (indusValue === "") {
        return true;
      }
      if (
        indusValue !== "" &&
        data.companyIndustry.$oid === indusValue
      ) {
        return true;
      }

      return false;
    });
    //Filter theo Số lượng NS
    let sizeCompanyData = industryCompanyData.filter((data) => {
      if (sizeValue === "") {
        return true;
      }
      if (
        sizeValue !== "" &&
        data.companySize.$oid === sizeValue
      ) {
        return true;
      }

      return false;
    });
    //Filter theo Loại hình doanh nghiệp
    let typeCompanyData = sizeCompanyData.filter((data) => {
      if (typeValue === "") {
        return true;
      }
      if (
        typeValue !== "" &&
        data.companyType.$oid === typeValue
      ) {
        return true;
      }

      return false;
    });
  dataFiltered = [...typeCompanyData]
  
}
  //Fillter
  useEffect(() => { 
    filterFunction();
    setSearchResults(dataFiltered);
    setDataComFiltered(dataFiltered)
  }, [areaValue,indusValue,sizeValue,typeValue])
  //Return
  useEffect(() => { 
    filterFunction();
    setDataComFiltered(dataFiltered)
  }, [onClickReturn])
  //Search
  useEffect(() => {
    filterFunction();
    const results = dataFiltered.filter((company) =>
      company.name.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setSearchResults(results);
  }, [searchTerm]);

  {
    /*fetching data*/
  }
  // useEffect(()=>{
  //     axios
  //     .get('https://my-json-server.typicode.com/aegone-nguyen/testdb/company')
  //     .then((res)=>{
  //         console.log(res);
  //         setCompanyData(res.data);
  //     })
  //     .catch((err)=>{
  //         console.log(err);
  //     })
  //   },[])

  {
    /*dropdown area*/
  }
  const ddBtnCompany = () => {
    return (
      <div className="dropdown">
        <button className="btn-dropdown" data-toggle="dropdown">
          <img
            src="./icon/down-arrow.png"
            alt="down-arrow"
            style={{ width: "26px", height: "26px" }}
          />
        </button>
        <ul className="dropdown-menu dropdown-menu-right" id="area">
          <option
            className="w3-animate-right dropdownElement"
            onClick={(e) => {
              setSearchTerm("");
              setOnClickReturn(!onClickReturn);
            }}
          >
            -------------
          </option>
          {searchResults.map((company) => (
            <option
              key={company._id.$oid}
              className="w3-animate-right dropdownElement"
              onClick={(e) => {
                setSearchTerm(e.target.value);
                setDataComFiltered([company]);
              }}
            >
              {company.name}
            </option>
          ))}
        </ul>
      </div>
    );
  };
  const ddSearchCompany = () => {
    return (
      <div className="dropdown">
        <input
          className="companyState"
          data-toggle="dropdown"
          type="text"
          placeholder="Search Company..."
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <ul className="dropdown-menu dropdown-menu-left" id="area">
          <option
            className="w3-animate-right dropdownElement"
            onClick={() => {
              setSearchTerm("");
              setOnClickReturn(!onClickReturn);
            }}
          >
            -------------
          </option>
          {searchResults.map((company) => (
            <option
              key={company._id.$oid}
              className="w3-animate-right dropdownElement"
              onClick={(e) => {
                setSearchTerm(e.target.value);
                setDataComFiltered([company]);
              }}
            >
              {company.name}
            </option>
          ))}
        </ul>
      </div>
    );
  };
  return (
    <div>
      <h6>Tên công ty</h6>
      <div className="company">
        <div className="row">
          {ddSearchCompany()}
          <div className="companyDD">{ddBtnCompany()}</div>
        </div>
      </div>
    </div>
  );
}

export default CompanyField;

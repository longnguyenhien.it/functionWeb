import React, { useState,useEffect } from "react";
import axios from 'axios';
import areaFillterData from '../Datas/areaFillter';


function AreaField(props) {
const {setAreaValue} = props;

const [searchTerm, setSearchTerm] = useState("");
const [searchResults, setSearchResults] = useState([]);

//Search
useEffect(() => {
  const results = areaFillterData.filter(area => {
    if (area.nameVn.toLowerCase().includes(searchTerm.toLowerCase())) {
      return true
    }
    if (area.cityAbb.toLowerCase().includes(searchTerm.toLowerCase())) {
      return true
    }
    return false
  });
  setSearchResults(results);
}, [searchTerm]);
  {/*fetching data*/}
// useEffect(()=>{
//   axios
//   .get('https://my-json-server.typicode.com/aegone-nguyen/testdb/area')
//   .then((res)=>{
//       console.log(res);
//       setAreaData(res.data);
//   })
//   .catch((err)=>{
//       console.log(err);
//   })

// },[])
  {/*dropdown area*/}
  const ddBtnArea = () => {
    return(
    <div className="dropdown">
        <button 
            className='btn-dropdown'
            data-toggle="dropdown">
            <img src='./icon/down-arrow.png' alt='down-arrow' style={{width: "26px",height: "26px"}}/>
        </button>
        <ul className="dropdown-menu dropdown-menu-right"  id='area'>
          <option className='w3-animate-right dropdownElement' onClick={()=>{setSearchTerm(''); setAreaValue('')}}>-------------</option>
          { 
            searchResults.map(area => 
            <option key={area.id} className='w3-animate-right dropdownElement' onClick={(e)=>{setSearchTerm(e.target.value); setAreaValue(area)}}>
              {area.nameVn}, {area.cityAbb}
            </option>
            )
          }
        </ul>
    </div>
    );
  }
  const ddSearchArea = () => {
    return(
      <div className="dropdown">
        <input  className='areaState'
                data-toggle="dropdown" 
                type="text" 
                placeholder="Search Area..." 
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
        />
        <ul className="dropdown-menu dropdown-menu-left" id='area'>
          <option className='w3-animate-right dropdownElement' onClick={()=>{setSearchTerm(''); setAreaValue('')}}>-------------</option>
          { 
            searchResults.map(area => 
            <option key={area.id} 
                    className='w3-animate-right dropdownElement' 
                    onClick={(e)=>{
                                    setSearchTerm(e.target.value);
                                    setAreaValue(area);
                                  }}
            >
              {area.nameVn}, {area.cityAbb}
            </option>
            ) 
          }
        </ul>
      </div>
    );
  }


  return (
    <div>
      <h6>Khu vực</h6>
      <div className="area">
        <div className="row">
          {ddSearchArea()}
          <div className='areaDD'>
            {ddBtnArea()}
          </div>
        </div>
      </div>
    </div>
  );
}

export default AreaField;

const areaFillterData = [
    {
        id: "HCM1",
        nameVn: "Quận 1",
        nameEn: "District 1",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM2",
        nameVn: "Quận 2",
        nameEn: "District 2",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM3",
        nameVn: "Quận 3",
        nameEn: "District 3",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM4",
        nameVn: "Quận 4",
        nameEn: "District 4",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM5",
        nameVn: "Quận 5",
        nameEn: "District 5",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM6",
        nameVn: "Quận 6",
        nameEn: "District 6",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM7",
        nameVn: "Quận 7",
        nameEn: "District 7",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM8",
        nameVn: "Quận 8",
        nameEn: "District 8",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM9",
        nameVn: "Quận 9",
        nameEn: "District 9",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM10",
        nameVn: "Quận 10",
        nameEn: "District 10",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM11",
        nameVn: "Quận 11",
        nameEn: "District 11",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM12",
        nameVn: "Quận 12",
        nameEn: "District 12",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM13",
        nameVn: "Gò Vấp",
        nameEn: "Go Vap",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM14",
        nameVn: "Thủ Đức",
        nameEn: "Thu Duc",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM15",
        nameVn: "Bình Thạnh",
        nameEn: "Binh Thanh",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM16",
        nameVn: "Tân Bình",
        nameEn: "Tan Binh",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM17",
        nameVn: "Tân Phú",
        nameEn: "Tan Phu",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM18",
        nameVn: "Phú Nhuận",
        nameEn: "Phu Nhuan",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM19",
        nameVn: "Bình Tân",
        nameEn: "Binh Tan",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM20",
        nameVn: "Củ Chi",
        nameEn: "Cu Chi",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM21",
        nameVn: "Hóc Môn",
        nameEn: "Hoc Mon",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM22",
        nameVn: "Bình Chánh",
        nameEn: "Binh Chanh",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM23",
        nameVn: "Nhà Bè",
        nameEn: "Nha Be",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "HCM24",
        nameVn: "Cần Giờ",
        nameEn: "Can Gio",
        cityVn: "Hồ Chí Minh",
        cityEn: "Ho Chi Minh",
        cityAbb: "HCM"
    },
    {
        id: "BD1",
        nameVn: "Thủ Dầu Một",
        nameEn: "Thu Dau Mot",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    },
    {
        id: "BD2",
        nameVn: "Bến Cát",
        nameEn: "Ben Cat",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    },
    {
        id: "BD3",
        nameVn: "Thủ Dầu Một",
        nameEn: "Thu Dau Mot",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    },
    {
        id: "BD4",
        nameVn: "Dĩ An",
        nameEn: "Di An",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    },
    {
        id: "BD5",
        nameVn: "Thuận An",
        nameEn: "Thuan An",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    },
    {
        id: "BD6",
        nameVn: "Tân Uyên",
        nameEn: "Tan Uyen",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    },
    {
        id: "BD7",
        nameVn: "Bắc Tân Uyên",
        nameEn: "Bac Tan Uyen",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    },
    {
        id: "BD8",
        nameVn: "Bàu Bàng",
        nameEn: "Bau Bang",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    },
    {
        id: "BD9",
        nameVn: "Dầu Tiếng",
        nameEn: "Dau Tieng",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    },
    {
        id: "BD10",
        nameVn: "Phú Giáo",
        nameEn: "Phu Giao",
        cityVn: "Bình Dương",
        cityEn: "Binh Duong",
        cityAbb: "Bình Dương"
    }
]
export default areaFillterData

    
const DataCompanies = [
  {
    "_id": {
      "$oid": "5e71a43453d9d402ddccb02d"
    },
    "avatar": "https://csrvietnam.org/assets/5d903546d3097f2167da9cff/2CBipf-F-blob",
    "nickname": "CSR TECH JSC",
    "phone": "02873022888",
    "gopy": 21,
    "status": true,
    "address": "3rd Floor An Phu Plaza, 117-119 Ly Chinh Thang District 3 HCMC – Vietnam",
    "bio": "",
    "password": "csrtech19",
    "accountType": "COMPANY",
    "name": "CSR TECH JSC",
    "email": "info@csrvietnam.org",
    "companySize": {
      "$oid": "5d60c4b8dbbfb83ee853560a"
    },
    "companyIndustry": {
      "$oid": "5d60c602dbbfb83ee853561f"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "companyId": {
      "$oid": "5e71a43453d9d402ddccb02c"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-03-18T04:31:48.603Z"
    },
    "updatedDate": {
      "$date": "2020-06-26T01:43:26.830Z"
    },
    "deletedDate": {
      "$date": "2020-03-18T04:31:48.603Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "taxCode": "",
    "active": true
  },
  {
    "_id": {
      "$oid": "5e71a4f053d9d402ddccb02f"
    },
    "avatar": "https://csrvietnam.org/assets/5d903546d3097f2167da9cff/X5qmwEMd4-blob",
    "nickname": "Esquel Garment Manufacturing (Vietnam)",
    "phone": "096 891 18 88",
    "gopy": 20,
    "status": true,
    "address": "Khu công nghiệp Vietnam-Singapore, 9 Đường Số 5 VSIP, Binh Hoà, Thuận An, Bình Dương,Vietnam",
    "bio": null,
    "password": "csrecosystem",
    "accountType": "COMPANY",
    "name": "Esquel Garment Manufacturing (Vietnam)",
    "email": "cty.goldgarment@gopy.io",
    "companySize": {
      "$oid": "5e71ce273381c60202ee783e"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "companyId": {
      "$oid": "5e71a4f053d9d402ddccb02e"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-03-18T04:34:56.323Z"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:09:00.522Z"
    },
    "deletedDate": {
      "$date": "2020-03-18T04:34:56.323Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "active": true,
    "taxCode": null
  },
  {
    "_id": {
      "$oid": "5e71a56653d9d402ddccb031"
    },
    "avatar": "https://csrvietnam.org/assets/5d903546d3097f2167da9cff/jaHbDuBRh-blob",
    "nickname": "Better Work Vietnam",
    "phone": "028 7305 0363",
    "gopy": 91,
    "status": true,
    "address": "106 Nguyễn Văn Trỗi, Phường 8, Phú Nhuận, Hồ Chí Minh,Vietnam",
    "bio": null,
    "password": "Abcd1359",
    "accountType": "COMPANY",
    "name": "Better Work Vietnam",
    "email": "cty.bwv@outlook.com",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c59edbbfb83ee8535618"
    },
    "companyType": {
      "$oid": "5d60c426dbbfb83ee8535607"
    },
    "companyId": {
      "$oid": "5e71a56653d9d402ddccb030"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-03-18T04:36:54.979Z"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:09:12.862Z"
    },
    "deletedDate": {
      "$date": "2020-03-18T04:36:54.979Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "active": true,
    "taxCode": null
  },
  {
    "_id": {
      "$oid": "5e71a5e353d9d402ddccb033"
    },
    "avatar": "https://csrvietnam.org/assets/5d903546d3097f2167da9cff/ceJV5MGp-blob",
    "nickname": "G'CONNECT",
    "phone": "0943777369",
    "gopy": 82,
    "status": true,
    "address": "6B Tú Xương, Phường 7, Quận 3, Tp. Hồ Chí Minh - Vietnam",
    "bio": null,
    "password": "Abcd1359",
    "accountType": "COMPANY",
    "name": "G'CONNECT",
    "email": "thuy.tran@gconnect.edu.vn",
    "companySize": {
      "$oid": "5d60c4b8dbbfb83ee853560a"
    },
    "companyIndustry": {
      "$oid": "5d60c602dbbfb83ee853561f"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "companyId": {
      "$oid": "5e71a5e353d9d402ddccb032"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-03-18T04:38:59.181Z"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:09:28.884Z"
    },
    "deletedDate": {
      "$date": "2020-03-18T04:38:59.181Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "taxCode": null,
    "active": true
  },
  {
    "_id": {
      "$oid": "5e71a6d753d9d402ddccb037"
    },
    "avatar": "https://csrvietnam.org/assets/5d903546d3097f2167da9cff/UsNoo1kl-blob",
    "nickname": "Liên Đoàn Lao Động Tp.HCM",
    "phone": "028 3829 7716",
    "gopy": 20,
    "status": true,
    "address": "14 Cách Mạng Tháng Tám, Phường Bến Thành, Quận 1, Hồ Chí Minh - Vietnam",
    "bio": null,
    "password": "0OBdfBw3YP",
    "accountType": "COMPANY",
    "name": "Liên Đoàn Lao Động Tp.HCM",
    "email": "ldld.hcm@outlook.com",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c5b7dbbfb83ee853561a"
    },
    "companyType": {
      "$oid": "5d60c3b5dbbfb83ee8535603"
    },
    "companyId": {
      "$oid": "5e71a6d753d9d402ddccb036"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-03-18T04:43:03.841Z"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:09:46.930Z"
    },
    "deletedDate": {
      "$date": "2020-03-18T04:43:03.841Z"
    },
    "__v": 0,
    "active": true,
    "taxCode": null,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    }
  },
  {
    "_id": {
      "$oid": "5e71a7c853d9d402ddccb03b"
    },
    "avatar": "https://csrvietnam.org/assets/5d903546d3097f2167da9cff/97bFh5n-p-blob",
    "nickname": "Le Tran Social Enterprise",
    "phone": "(+84) 0888636099",
    "gopy": 30,
    "status": true,
    "address": "6b Tu Xuong St Ward 7 District 3, Ho Chi Minh City - Vietnam",
    "bio": null,
    "password": "Abcd1359",
    "accountType": "COMPANY",
    "name": "Le Tran Social Enterprise",
    "email": "luan.le@letranse.net",
    "companySize": {
      "$oid": "5d60c4cadbbfb83ee853560b"
    },
    "companyIndustry": {
      "$oid": "5d60c59edbbfb83ee8535618"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "companyId": {
      "$oid": "5e71a7c853d9d402ddccb03a"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-03-18T04:47:04.706Z"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:10:10.068Z"
    },
    "deletedDate": {
      "$date": "2020-03-18T04:47:04.706Z"
    },
    "__v": 0,
    "active": true,
    "taxCode": null,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    }
  },
  {
    "_id": {
      "$oid": "5e731e7c5116f600bd7426a6"
    },
    "avatar": "https://csrvietnam.org/assets/5d903546d3097f2167da9cff/y2G9dl_bJ-blob",
    "gopy": 37,
    "status": true,
    "password": "gopy181070",
    "accountType": "COMPANY",
    "name": "Cty CP TM-DV Thang Long",
    "email": "giang181070@gmail.com",
    "companySize": {
      "$oid": "5d60c4cadbbfb83ee853560b"
    },
    "companyIndustry": {
      "$oid": "5d60c570dbbfb83ee8535614"
    },
    "companyType": {
      "$oid": "5d60c3b5dbbfb83ee8535603"
    },
    "companyId": {
      "$oid": "5e731e7c5116f600bd7426a5"
    },
    "createdDate": {
      "$date": "2020-03-19T07:25:48.074Z"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:10:22.563Z"
    },
    "__v": 0,
    "address": "42-44 Tạ Uyên, Phường 15, Quận 5, Hồ Chí Minh,Vietnam",
    "phone": "02838552608",
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "bio": null,
    "nickname": "Cty CP TM-DV Thang Long",
    "taxCode": null,
    "active": true
  },
  {
    "_id": {
      "$oid": "5ec623dda6cf59022178548e"
    },
    "avatar": null,
    "nickname": "M&J Garment-Printing-Embroidery",
    "phone": "+84 28 37191230",
    "gopy": 34,
    "status": true,
    "address": "27/9 Hamlet 5, Phan Van Hon Street, Tan Thoi Nhat Ward, District 12, Ho Chi Minh City, Vietnam",
    "bio": null,
    "password": "csrecosystem",
    "accountType": "COMPANY",
    "name": "M&J Garment-Printing-Embroidery",
    "email": "cty.mj@gopy.io",
    "companySize": {
      "$oid": "5e71ce273381c60202ee783e"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "companyId": {
      "$oid": "5ec623dda6cf59022178548d"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-05-21T06:46:53.302Z"
    },
    "__v": 0,
    "active": true,
    "taxCode": null,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:11:00.789Z"
    }
  },
  {
    "_id": {
      "$oid": "5ec62451a6cf590221785490"
    },
    "avatar": null,
    "nickname": "Unisoll Vina",
    "phone": "0275 3635 600",
    "gopy": 20,
    "status": true,
    "address": "Khu BI, BII, BIII, BIV KCN Giao Long giai đoạn II, Châu Thành, Bến Tre,Vietnam",
    "bio": null,
    "password": "csrecosystem",
    "accountType": "COMPANY",
    "name": "Unisoll Vina",
    "email": "cty.unisoll@gopy.io",
    "companySize": {
      "$oid": "5e71ce773381c60202ee783f"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "companyId": {
      "$oid": "5ec62451a6cf59022178548f"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-05-21T06:48:49.503Z"
    },
    "__v": 0,
    "active": true,
    "taxCode": null,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:11:14.824Z"
    }
  },
  {
    "_id": {
      "$oid": "5ecc8961a6cf590221785492"
    },
    "avatar": null,
    "nickname": "Namyang Song May Co., Ltd.",
    "phone": "0251 6279 331",
    "gopy": 20,
    "status": true,
    "address": "Lô B1 Đường 6 KCN, Sông Mây, Trảng Bom, Đồng Nai,Vietnam",
    "bio": null,
    "password": "csrecosystem",
    "accountType": "COMPANY",
    "name": "Namyang Song May Co., Ltd.",
    "email": "cty.namyang@gopy.io",
    "companySize": {
      "$oid": "5e71ce273381c60202ee783e"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "companyId": {
      "$oid": "5ecc8961a6cf590221785491"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-05-26T03:13:37.931Z"
    },
    "__v": 0,
    "active": true,
    "taxCode": null,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:11:31.751Z"
    }
  },
  {
    "_id": {
      "$oid": "5ecc8b65a6cf590221785494"
    },
    "avatar": null,
    "nickname": "Eclat textile Co., Ltd.",
    "phone": "0251 3560 746",
    "gopy": 20,
    "status": true,
    "address": "Khu Công Nghiệp Nhơn Trạch 2, Lot 1. Road 5A, Nhơn Trạch, Đồng Nai,Vietnam",
    "bio": null,
    "password": "csrecosystem",
    "accountType": "COMPANY",
    "name": "Eclat textile Co., Ltd.",
    "email": "cty.eclat@gopy.io",
    "companySize": {
      "$oid": "5e71ce273381c60202ee783e"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "companyId": {
      "$oid": "5ecc8b65a6cf590221785493"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-05-26T03:22:13.425Z"
    },
    "__v": 0,
    "active": true,
    "taxCode": null,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:11:47.845Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee2fcf8b2a6db0265fdab16"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "D10/89Q QL1A,  P.Tân Tạo, Quận Bình Chánh, Tp. HCM, Vietnam",
    "bio": "SX, gia công \ngiày",
    "password": "LBUPzWQ1dA",
    "accountType": "COMPANY",
    "name": "Cty TNHH Pouyuen Việt Nam",
    "nickname": "Cty TNHH Pouyuen Việt Nam",
    "email": "pouyuen.vn@gopy.io",
    "phone": "028 3876 2358",
    "companySize": {
      "$oid": "5e71ce773381c60202ee783f"
    },
    "companyIndustry": {
      "$oid": "5d60c570dbbfb83ee8535614"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee2fcf8b2a6db0265fdab15"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T03:56:40.696Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:12:17.135Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee2fe7ab2a6db0265fdab1c"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "D10/89Q QL1A,  P.Tân Tạo, Quận Bình Tân, Tp. HCM, Vietnam",
    "bio": "SX phụ liệu giày",
    "password": "ru6pFXNhMr",
    "accountType": "COMPANY",
    "name": "Cty TNHH Chang Yang",
    "nickname": "Cty TNHH Chang Yang",
    "email": "chang.yang@gopy.io",
    "phone": "02838 765 371",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c570dbbfb83ee8535614"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee2fe7ab2a6db0265fdab1b"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T04:03:06.266Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:12:54.034Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee30a55b2a6db0265fdab3c"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "QL1A, Tân Tạo, Bình Tân, Thành phố Hồ Chí Minh",
    "bio": "May mặc",
    "password": "dxahnaJ3H8",
    "accountType": "COMPANY",
    "name": "Cty TNHH Dinsen Việt Nam",
    "nickname": "Cty TNHH Dinsen Việt Nam",
    "email": "viet.quan@gopy.io",
    "phone": "02837542511",
    "companySize": {
      "$oid": "5e71ce773381c60202ee783f"
    },
    "companyIndustry": {
      "$oid": "5ee311407a820d0297097834"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee30a55b2a6db0265fdab3b"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T04:53:41.492Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-09-23T03:08:23.078Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee30e27b2a6db0265fdab46"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "103/7 Ao Đôi,  P.Bình Trị Đông A, Quận Bình Tân, Tp. HCM, Vietnam",
    "bio": "SX ống nhựa",
    "password": "kDHkGIpeFK",
    "accountType": "COMPANY",
    "name": "Cty CP Nhựa Minh Hùng",
    "nickname": "Cty CP Nhựa Minh Hùng",
    "email": "nhua.minhhung@gopy.io",
    "phone": "0338 797 978",
    "companySize": {
      "$oid": "5d60c4efdbbfb83ee853560e"
    },
    "companyIndustry": {
      "$oid": "5ee311407a820d0297097834"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee30e27b2a6db0265fdab45"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T05:09:59.799Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:17:54.224Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee31144b2a6db0265fdab50"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "91028 Tân Kỳ Tân Quý, Bình Hưng Hoà",
    "bio": "May mặc",
    "password": "A40YuDRmx7",
    "accountType": "COMPANY",
    "name": "Cty TNHH May Cần Mẫn",
    "nickname": "Cty TNHH May Cần Mẫn",
    "email": "shun.cheng@gopy.io",
    "phone": "02862691531",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ee31144b2a6db0265fdab4f"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T05:23:16.753Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-10-01T03:59:39.896Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee31bdab2a6db0265fdab64"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "27 Đường số 31, P.Bình Trị Đông B, Quận Bình Tân, Tp. HCM, Vietnam",
    "bio": "Sản xuất ngói xi măng",
    "password": "l0fL7xQSPz",
    "accountType": "COMPANY",
    "name": "Cty TNHH Hỷ Phát",
    "nickname": "Cty TNHH Hỷ Phát",
    "email": "hy.phat@gopy.io",
    "phone": "028 3768 2050",
    "companySize": {
      "$oid": "5d60c4cadbbfb83ee853560b"
    },
    "companyIndustry": {
      "$oid": "5ee311407a820d0297097834"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee31bdab2a6db0265fdab63"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T06:08:26.220Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:21:26.104Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee31d9ab2a6db0265fdab6a"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "4331/1, Nguyễn Cửu Phú, Tân Tạo A, Bình Tân, Tp. Hồ Chí Minh - Vietnam",
    "bio": "Sản xuất da, giày",
    "password": "6FhPsFNT8W",
    "accountType": "COMPANY",
    "name": "Cty TNHH Hyogo Shoes Việt Nam",
    "nickname": "Cty TNHH Hyogo Shoes Việt Nam",
    "email": "Hyogo.Shoes@gopy.io",
    "phone": "02837561115",
    "companySize": {
      "$oid": "5d60c4cadbbfb83ee853560b"
    },
    "companyIndustry": {
      "$oid": "5d60c570dbbfb83ee8535614"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ee31d9ab2a6db0265fdab69"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T06:15:54.075Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-09-10T04:06:57.192Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee31eb5b2a6db0265fdab6e"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "95 Lê Đình Cẩn, P.Tân Tạo, Quận Bình Tân, Tp. HCM, Vietnam",
    "bio": "Sản xuất giày",
    "password": "Mz3jHPGIeK",
    "accountType": "COMPANY",
    "name": "Cty May Quốc Tế Việt Nam Nhật Bản",
    "nickname": "Cty May Quốc Tế Việt Nam Nhật Bản",
    "email": "giay.giaimy@gopy.io",
    "phone": "0123456104",
    "companySize": {
      "$oid": "5d60c4cadbbfb83ee853560b"
    },
    "companyIndustry": {
      "$oid": "5d60c570dbbfb83ee8535614"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ee31eb5b2a6db0265fdab6d"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T06:20:37.942Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-10-08T03:16:05.053Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee31f95b2a6db0265fdab72"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "483 QL1A, P.Bình Hưng Hòa, Quận Bình Tân, Tp. HCM, Vietnam",
    "bio": "Cơ khí",
    "password": "C3HMNMYwDi",
    "accountType": "COMPANY",
    "name": "Cty TNHH Đức Tường",
    "nickname": "Cty TNHH Đức Tường",
    "email": "duc.tuong@gopy.io",
    "phone": "028 3750 3880",
    "companySize": {
      "$oid": "5d60c4cadbbfb83ee853560b"
    },
    "companyIndustry": {
      "$oid": "5ee311407a820d0297097834"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee31f95b2a6db0265fdab71"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T06:24:21.037Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-15T03:23:07.823Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee3208db2a6db0265fdab76"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "135/1 Lê Văn Quới, P.Bình Trị Đông, Quận Bình Tân, Tp. HCM, Vietnam",
    "bio": "Kinh doanh vận tải",
    "password": "tEkdIXR420",
    "accountType": "COMPANY",
    "name": "Cty TNHH SX-TM-BB Ngọc Triêm",
    "nickname": "Cty TNHH SX-TM-BB Ngọc Triêm",
    "email": "ngoc.triem@gopy.io",
    "phone": "0123456107",
    "companySize": {
      "$oid": "5d60c4efdbbfb83ee853560e"
    },
    "companyIndustry": {
      "$oid": "5ee305cc7a820d0297097833"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ee3208db2a6db0265fdab75"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-12T06:28:29.819Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-07-16T09:26:57.844Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee6fc15eef66e020502916d"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot K-3A-CN, Road No.8, My Phuoc Industrial Park, Ben Cat, Binh Dương, Vietnam",
    "bio": "T-shirts\nShorts",
    "password": "csrecosystem",
    "accountType": "COMPANY",
    "name": "New Wide Garment (Viet Nam) Co. Ltd",
    "nickname": "New Wide Garment (Viet Nam) Co. Ltd",
    "email": "new.wide@gopy.io",
    "phone": "+842743567262",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee6fc15eef66e020502916c"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-15T04:41:57.408Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-16T03:55:40.171Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee6ff06eef66e020502916f"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "823 Provincial Highway, Duc Lap Ha Village, Duc Hoa District, Long An, Vietnam",
    "bio": "Knitwear\nKnitwear",
    "password": "Ws99V1MN1H",
    "accountType": "COMPANY",
    "name": "Shinsung Vina Co., Ltd",
    "nickname": "Shinsung Vina ",
    "email": "shinsung.vina@gopy.io",
    "phone": "+842723811864",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ee6ff06eef66e020502916e"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-15T04:54:30.971Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-25T08:47:01.492Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee71901eef66e020502917c"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Street 6, Trang Bang Industrial Park, Trang Bang District, Tay Ninh, Vietnam",
    "bio": "Knit tops\nKnit  bottoms\n",
    "password": "g32tp59sGL",
    "accountType": "COMPANY",
    "name": "Lotus Textile & Garment Co. Ltd",
    "nickname": "Lotus Textile & Garment Co. Ltd",
    "email": "lotus.textile@gopy.io",
    "phone": "+842763896228",
    "companySize": {
      "$oid": "5d60c534dbbfb83ee8535612"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee71901eef66e020502917b"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-15T06:45:21.700Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-16T03:57:56.785Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee7253feef66e020502918a"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Road No. 6, Song Than 1 Industrial Park, Di An Town, Binh Duong, Vietnam",
    "bio": "Knitwear\nKnitwear\n",
    "password": "MzjBHRuKZw",
    "accountType": "COMPANY",
    "name": "Hansoll Vina Co., Ltd",
    "nickname": "Hansoll Vina Co., Ltd",
    "email": "hansoll.vina@gopy.io",
    "phone": "+842743732941",
    "companySize": {
      "$oid": "5e71ce273381c60202ee783e"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee7253feef66e0205029189"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-15T07:37:35.014Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-16T04:00:30.635Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee7377eeef66e02050291a4"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot 61, Road B, Linh Trung Export Processing Zone II, Binh Chieu Ward, Thu Duc, Ho Chi Minh, Vietnam",
    "bio": "Swimwear\t\nSportwear\n",
    "password": "3pMKIvenAw",
    "accountType": "COMPANY",
    "name": "King Hung Garments Industrial Co., Ltd",
    "nickname": "King Hung Garments Industrial Co., Ltd",
    "email": "king.hung@gopy.io",
    "phone": "+84287291829",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee7377eeef66e02050291a3"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-15T08:55:26.391Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-16T04:08:07.186Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee73f37eef66e02050291b2"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "7A Thong Nhat Road, Song Than Industrial Zone II, Di An, Binh Duong, Vietnam",
    "bio": "Knit tops\t\nKnit bottoms\n",
    "password": "BJSt425Kgm",
    "accountType": "COMPANY",
    "name": "Eins Vina Co., Ltd",
    "nickname": "Eins Vina Co., Ltd",
    "email": "eins.vina@gopy.io",
    "phone": "+842743737323",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee73f37eef66e02050291b1"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-15T09:28:23.571Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-16T04:10:11.782Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee8294d83101402549a5aee"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Road No. 7, Lot 28, Trang Bang Industrial Park, Trang Bang District, Tay Ninh, Vietnam",
    "bio": "Knit tops\t\nKnit pants\n",
    "password": "c2ZMXbYW84",
    "accountType": "COMPANY",
    "name": "Colltex Garment MFY Co., Ltd. VN",
    "nickname": "Colltex Garment MFY Co., Ltd. VN",
    "email": "colltex.vn@gopy.io",
    "phone": "+842763897340",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee8294d83101402549a5aed"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-16T02:07:09.154Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-16T04:15:43.697Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee83eed83101402549a5afe"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "184 Đường Trần Thanh Mại, Tân Tạo A, Bình Tân, Tp. Hồ Chí Minh, Vietnam",
    "bio": "Liên Đoàn Lao Động Quận Bình Tân",
    "password": "csrecosystem",
    "accountType": "COMPANY",
    "name": "Liên Đoàn Lao Động Quận Bình Tân",
    "nickname": "Liên Đoàn Lao Động Quận Bình Tân",
    "email": "idid.binhtan@gopy.io",
    "phone": "028 3875 0900",
    "companySize": {
      "$oid": "5d60c506dbbfb83ee853560f"
    },
    "companyIndustry": {
      "$oid": "5d60c5b7dbbfb83ee853561a"
    },
    "companyType": {
      "$oid": "5d60c3b5dbbfb83ee8535603"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee83eed83101402549a5afd"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-16T03:39:25.509Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-16T04:20:44.022Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee85b5e83101402549a5b18"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot A2, 3, 4 LOTECO EPZ, Bien Hoa, Dong Nai, Vietnam",
    "bio": "Caps\nCaps\n",
    "password": "ot6fc1h87Y",
    "accountType": "COMPANY",
    "name": "Yupoong Vietnam Co., Ltd",
    "nickname": "Yupoong Vietnam Co., Ltd",
    "email": "yupoong.vietnam@gopy.io",
    "phone": "+842513991981",
    "companySize": {
      "$oid": "5d60c534dbbfb83ee8535612"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ee85b5e83101402549a5b17"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-16T05:40:46.153Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ee864f683101402549a5b1e"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Street No. 1, Binh Duong Industrial Zone, Di An District, Binh Duong, Vietnam",
    "bio": "Screen printing\nScreen printing\n",
    "password": "gEOx3RcBKk",
    "accountType": "COMPANY",
    "name": "Dong- A Vina Co., Ltd",
    "nickname": "Dong- A Vina Co., Ltd",
    "email": "dong.a@gopy.io",
    "phone": "+842743782310",
    "companySize": {
      "$oid": "5d60c506dbbfb83ee853560f"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ee864f683101402549a5b1d"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-16T06:21:42.665Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ee8766d83101402549a5b3b"
    },
    "avatar": null,
    "gopy": 20,
    "status": true,
    "active": true,
    "address": " Binh Phuoc Quarter, Binh Chuan Ward, Thuan An, Binh Duong, Vietnam",
    "bio": "Shorts\t\nPants\n",
    "password": "ZBK0WgkMTT",
    "accountType": "COMPANY",
    "name": "K.J Vina Co., Ltd",
    "nickname": "K.J Vina Co., Ltd",
    "email": "k.j@gopy.io",
    "phone": "+842743625501",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": null,
    "companyId": {
      "$oid": "5ee8766d83101402549a5b3a"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-16T07:36:13.882Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-17T01:44:31.868Z"
    }
  },
  {
    "_id": {
      "$oid": "5ee9e781ce3f660479a57741"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot B1, B2, B5-B12 Giao Long Industrial Park, An Phuoc Commune, Chau Thanh, Ben Tre, Vietnam",
    "bio": "Sportswear\t\nPrinting\n",
    "password": "HXXYQCVGHx",
    "accountType": "COMPANY",
    "name": "Alliance One Apparel Co. Ltd",
    "nickname": "Alliance One Apparel Co. Ltd",
    "email": "alliance.one@gopy.io",
    "phone": "+842753612113",
    "companySize": {
      "$oid": "5e71ce773381c60202ee783f"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ee9e781ce3f660479a57740"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-17T09:50:57.091Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5eeadf42ce3f660479a5776b"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "18 Phan Huy Ich Street, Ward 14, Go Vap District, Ho Chi Minh, Vietnam",
    "bio": "T-Shirts\t\nKnitwear\n",
    "password": "EbYaBiiTHh",
    "accountType": "COMPANY",
    "name": "Phuong Nam Garment Trading Import Export JSC",
    "nickname": "Phuong Nam Garment Trading Import Export JSC",
    "email": "phuong.nam@gopy.io",
    "phone": "842839870191",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5eeadf42ce3f660479a5776a"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-18T03:28:02.451Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5eeae2f1ce3f660479a57771"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot P2, Street No 8, Loi Binh Nhon Industrial Zone, Tan An, Long An, Vietnam",
    "bio": "PP woven handbags\t\nShopping bags\n",
    "password": "rC8BHT3kLx",
    "accountType": "COMPANY",
    "name": "Branch of Continent Packaging Corporation",
    "nickname": "Branch of Continent Packaging Corporation",
    "email": "packaging.vn@gopy.io",
    "phone": "842723526203",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c602dbbfb83ee853561f"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5eeae2f1ce3f660479a57770"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-18T03:43:45.896Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5eeaeddece3f660479a5778b"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Thanh Phuoc Hamlet, Thanh Dien Village, Chau Thanh District, Tay Ninh, Vietnam",
    "bio": "Dresses\t\nBlouses\n",
    "password": "oouMJ4cVzc",
    "accountType": "COMPANY",
    "name": "Bando Vina Co., Ltd",
    "nickname": "Bando Vina Co., Ltd",
    "email": "bando.vina@gopy.io",
    "phone": "842763715090",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5eeaeddece3f660479a5778a"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-18T04:30:22.445Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5eeb38a9ce3f660479a5781a"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Suoi Cao Hamlet, Phuoc Dong, Go Dau, Tay Ninh, Vietnam",
    "bio": "Clothings\tSporting \nprotective gears\n",
    "password": "nAi1wjtNdb",
    "accountType": "COMPANY",
    "name": "Deyork Vietnam Co., Ltd",
    "nickname": "Deyork Vietnam Co., Ltd",
    "email": "deyork.vn@gopy.io",
    "phone": "+842763533970",
    "companySize": {
      "$oid": "5d60c506dbbfb83ee853560f"
    },
    "companyIndustry": {
      "$oid": "5d60c585dbbfb83ee8535616"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5eeb38a9ce3f660479a57819"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-18T09:49:29.206Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5eec29b1ce3f660479a57847"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "No. 48, Tang Nhon Phu Street, Tang Nhon Phu B Ward, District 9, Ho Chi Minh",
    "bio": "Denim bottoms\t\nWoven bottoms\n",
    "password": "20G6rLykMX",
    "accountType": "COMPANY",
    "name": "Phong Phu International JSC- Jean Export Garment Factory",
    "nickname": "Phong Phu International JSC- Jean Export Garment Factory",
    "email": "phongphu.vn@gopy.io",
    "phone": "028 6684 7979",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5eec29b1ce3f660479a57846"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-19T02:57:53.587Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-26T06:25:34.390Z"
    }
  },
  {
    "_id": {
      "$oid": "5eec6d65ce3f660479a578c3"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Hoa Lan Quarter, Thuan Giao Commune, Thuan An Town, Binh Duong, Vietnam",
    "bio": "Woven jackets\t\nWoven dresses\n",
    "password": "xKXi2lULyc",
    "accountType": "COMPANY",
    "name": "Poong In Vina 5 Co. Ltd",
    "nickname": "Poong In Vina 5 Co. Ltd",
    "email": "poongin.vina5@gopy.io",
    "phone": "+842743718976",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5eec6d65ce3f660479a578c2"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-19T07:46:45.227Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef012bcce3f660479a5790a"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot 64-66-68, Linh Trung Export Processing Zone 1, Hamlet 4, Linh Trung Commune, Thu Duc District, Ho Chi Minh, Vietnam",
    "bio": "Jackets\t\nDresses\n",
    "password": "veH1J7Du6r",
    "accountType": "COMPANY",
    "name": "Upgain (VN) Manufacturing Co., Ltd",
    "nickname": "Upgain (VN) Manufacturing Co., Ltd",
    "email": "upgain.vn@gopy.io",
    "phone": "+84288967155",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef012bcce3f660479a57909"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-22T02:09:00.515Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef019ebce3f660479a57916"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "CN14, Khai Quang Industrial Zone, Khai Quang Ward, Vinh Yen City, Vinh Phuc, Vietnam",
    "bio": "Knit tops\t\nKnit bottoms\n",
    "password": "Q2jWQfOIOW",
    "accountType": "COMPANY",
    "name": "Shinwon Ebenezer Vietnam Co., Ltd",
    "nickname": "Shinwon Ebenezer Vietnam Co., Ltd",
    "email": "shinwon.ebenezer@gopy.io",
    "phone": "+842113842830",
    "companySize": {
      "$oid": "5e71ce273381c60202ee783e"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef019ebce3f660479a57915"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-22T02:39:39.583Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef029c3ce3f660479a57939"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Hoa Lan Ward, Thuan Giao Town, Thuan An District, Binh Duong, Vietnam",
    "bio": "Dresses\t\nSkirts\n",
    "password": "8BjPt1H01A",
    "accountType": "COMPANY",
    "name": "JC INT'L Vina Co., Ltd",
    "nickname": "JC INT'L Vina Co., Ltd",
    "email": "jc.intl@gopy.io",
    "phone": "+842743716391",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef029c3ce3f660479a57938"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-22T03:47:15.450Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef038dece3f660479a5795f"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Binh Tien 2 Hamlet, Duc Hoa Ha Willage, Duc Hoa District, Long An, Vietnam",
    "bio": "Printing garment\n",
    "password": "cAVj6Hksxw",
    "accountType": "COMPANY",
    "name": "Jin Ju Plus Vina Co., Ltd",
    "nickname": "Jin Ju Plus Vina Co., Ltd",
    "email": "jin.ju.plus@gopy.io",
    "phone": "+84974091440",
    "companySize": {
      "$oid": "5d60c4efdbbfb83ee853560e"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef038dece3f660479a5795e"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-22T04:51:42.053Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef05bdbce3f660479a5798e"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot A1, Road No. 787, Thanh Thanh Cong Industrial Park, An Hoa Commune, Trang Bang District, Tay Ninh, Vietnam",
    "bio": "Sweaters\nSweaters\n",
    "password": "SUjq1iAoVG",
    "accountType": "COMPANY",
    "name": "First Team (Vietnam) Garment Ltd",
    "nickname": "First Team (Vietnam) Garment Ltd",
    "email": "first.team@gopy.io",
    "phone": "+842763883388",
    "companySize": {
      "$oid": "5e71ce773381c60202ee783f"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef05bdbce3f660479a5798d"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-22T07:20:59.988Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef06f87ce3f660479a579af"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "59/13Z Tien Lan 1 Hamlet, Ba Diem Commune, Hoc Mon District, Ho Chi Minh, Vietnam",
    "bio": "Embroidery\t\nEmbroidery\n",
    "password": "uMk7igmKoW",
    "accountType": "COMPANY",
    "name": "Eun-Sun Vina Embroidery Co., Ltd",
    "nickname": "Eun-Sun Vina Embroidery Co., Ltd",
    "email": "eun.sun@gopy.io",
    "phone": "+842854250026",
    "companySize": {
      "$oid": "5d60c506dbbfb83ee853560f"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef06f87ce3f660479a579ae"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-22T08:44:55.924Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef1bae97ac70e0246c0be13"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Plot P-1A, Road No.6, Long Hau Industrial Park Expansion Area, Long Hau Commune, Can Giuoc District, Long An, Vietnam",
    "bio": "Pants\t\nShirts\n",
    "password": "EDqk5rNnzl",
    "accountType": "COMPANY",
    "name": "Deutsche Bekleidungswerke Ltd",
    "nickname": "Deutsche Bekleidungswerke Ltd",
    "email": "deutsche.vn@gopy.io",
    "phone": "+8436201420",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef1bae97ac70e0246c0be12"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-23T08:18:49.425Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef1be827ac70e0246c0be21"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Road 12, Song Than 2 Industrial Park, Di An, Binh Duong, Vietnam",
    "bio": "Washing Products\t\nDyeing Products\n",
    "password": "VvMBYePwJ6",
    "accountType": "COMPANY",
    "name": "Sun Duck Vina Co.,Ltd",
    "nickname": "Sun Duck Vina Co.,Ltd",
    "email": "sun.duck@gopy.io",
    "phone": "+842743737603",
    "companySize": {
      "$oid": "5d60c4efdbbfb83ee853560e"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef1be827ac70e0246c0be20"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-23T08:34:10.266Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef1c7ab7ac70e0246c0be40"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Group 2, Dong Khoi Street, Tan Hiep Ward, Dong Nai, Vietnam",
    "bio": "Activewear\t\nUnderwear\n",
    "password": "bytuONXKAD",
    "accountType": "COMPANY",
    "name": "Son Ha Company Ltd",
    "nickname": "Son Ha Company Ltd",
    "email": "son.ha@gopy.io",
    "phone": "+842516291676",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef1c7ab7ac70e0246c0be3f"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-23T09:13:15.898Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef1cdf57ac70e0246c0be56"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Phu Hoa Hamlet, Phu Nhuan Commune, Cai Lay Town, My Tho, Vietnam",
    "bio": "Sweaters\nSweaters\n",
    "password": "NPp0kIGCXY",
    "accountType": "COMPANY",
    "name": "Eco Way Knitwear Co., Ltd",
    "nickname": "Eco Way Knitwear Co., Ltd",
    "email": "eco.way@gopy.io",
    "phone": "+842733799798",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef1cdf57ac70e0246c0be55"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-23T09:40:05.250Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef2b3187ac70e0246c0be6d"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "142 Quang Trung Street, Tran Hung Dao Ward, Thai Binh, Vietnam",
    "bio": "Jackets\t\nPants\n",
    "password": "HJ4g0oEUW5",
    "accountType": "COMPANY",
    "name": "Viet Thai Export Garment JSC",
    "nickname": "Viet Thai Export Garment JSC",
    "email": "vietthai.vn@gopy.io",
    "phone": "84363831686",
    "companySize": {
      "$oid": "5d60c534dbbfb83ee8535612"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef2b3187ac70e0246c0be6c"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-24T01:57:44.239Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef2b5b47ac70e0246c0be73"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Thanh Tan Industrial Complex, Thanh Tan Commune, Kien Xuong District, Thai Binh, Vietnam",
    "bio": "Jackets\t\nPants\n",
    "password": "SmqdgHBFXS",
    "accountType": "COMPANY",
    "name": "Thanh Tan Garment Factory",
    "nickname": "Thanh Tan Garment Factory",
    "email": "thanh.tan@gopy.io",
    "phone": "842273831686",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef2b5b47ac70e0246c0be72"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-24T02:08:52.807Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef2b7417ac70e0246c0be79"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Qui Trinh Hamlet, Nhi Quy Commune, Cai Lay Town, Tien giang, Vietnam",
    "bio": "Knitwear\nKnitwear\n",
    "password": "Jhy2KMUrQ6",
    "accountType": "COMPANY",
    "name": "Eco Tank Garment Co., Ltd",
    "nickname": "Eco Tank Garment Co., Ltd",
    "email": "eco.tank@gopy.io",
    "phone": "84733799668",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef2b7417ac70e0246c0be78"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-24T02:15:29.309Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef2d1957ac70e0246c0beb0"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "No.45 Yersin Street, Hiep Thanh Ward, Thu Dau Mot City, Binh Duong, VIetnam",
    "bio": "Dresses\t\nPants\n",
    "password": "B8VSVWYp3Z",
    "accountType": "COMPANY",
    "name": "Thomas Hill Co., Ltd",
    "nickname": "Thomas Hill Co., Ltd",
    "email": "thomas.hill@gopy.io",
    "phone": "+842743819398",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef2d1957ac70e0246c0beaf"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-24T04:07:49.042Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef2de137ac70e0246c0bed2"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot C, Road No. 3, Dong An Industrial Park, Binh Hoa Ward,Thuan An Town, Binh Duong, Vietnam",
    "bio": "Knitwear\t\nWoven\n",
    "password": "4TksaL57Rt",
    "accountType": "COMPANY",
    "name": "Leading Star Vietnam Garment Co., Ltd",
    "nickname": "Leading Star Vietnam Garment Co., Ltd",
    "email": "leading.star@gopy.io",
    "phone": "+84963316007",
    "companySize": {
      "$oid": "5d60c534dbbfb83ee8535612"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef2de137ac70e0246c0bed1"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-24T05:01:07.910Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef2f3577ac70e0246c0beee"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "189/8 Le Hong Phong Street, Tan Phuoc Village, Tan Binh Ward, Di An Town, Binh Duong, Vietnam",
    "bio": "Dresses\t\nT-shirts\n",
    "password": "4mNIk9wkgQ",
    "accountType": "COMPANY",
    "name": "Shinsung Tongsang Vina Co., Ltd",
    "nickname": "Shinsung Tongsang Vina Co., Ltd",
    "email": "shinsung.tongsang@gopy.io",
    "phone": "+842743739971",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef2f3577ac70e0246c0beed"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-24T06:31:51.504Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef3018b7ac70e0246c0bf00"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "No. P1, N5A Street, Hoa Xa Industrial Park, My Xa Ward, Nam Dinh, Vietnam",
    "bio": "Yoga pants\t\nJackets\n",
    "password": "Cipgw9Rk3s",
    "accountType": "COMPANY",
    "name": "Viet Thuan Apparel Company Limited",
    "nickname": "Viet Thuan Apparel Company Limited",
    "email": "viet.thuan@gopy.io",
    "phone": "+842283668998",
    "companySize": {
      "$oid": "5d60c534dbbfb83ee8535612"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef3018b7ac70e0246c0beff"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-24T07:32:27.209Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef308aa7ac70e0246c0bf0c"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "No.18, An Hoa Commune, Ward Hoa An, Bien Hoa, Dong Nai, Vietnam",
    "bio": "Jackets\t\nBase Layer\n",
    "password": "rWG8Dnj71T",
    "accountType": "COMPANY",
    "name": "Chinh Tuc Co., Ltd",
    "nickname": "Chinh Tuc Co., Ltd",
    "email": "chinhtuc.vn@gopy.io",
    "phone": "+8402513958117",
    "companySize": {
      "$oid": "5d60c506dbbfb83ee853560f"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef308aa7ac70e0246c0bf0b"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-24T08:02:50.515Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-24T08:03:47.731Z"
    }
  },
  {
    "_id": {
      "$oid": "5ef3102d7ac70e0246c0bf15"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Road No.2, Lien Hung Industrial Cluster, Binh Tien 2 Hamlet, Duc Hoa Ha Ward, Duc Hoa District, Long An, Vietnam",
    "bio": "Lady Shoes\nLady Shoes\n",
    "password": "9wuYOLrgwf",
    "accountType": "COMPANY",
    "name": "Rising Viet Nam Shoes Co., Ltd",
    "nickname": "Rising Viet Nam Shoes Co., Ltd",
    "email": "rising.vienam@gopy.io",
    "phone": "+842723769536",
    "companySize": {
      "$oid": "5d60c506dbbfb83ee853560f"
    },
    "companyIndustry": {
      "$oid": "5d60c570dbbfb83ee8535614"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef3102d7ac70e0246c0bf14"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-24T08:34:53.779Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-25T02:10:49.653Z"
    }
  },
  {
    "_id": {
      "$oid": "5ef413217ac70e0246c0bf28"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Workshop 1, Lot CN6, CN7, CN8 East Zone of Phu Thai Industrial Park, Kim Luong, Kinh Thanh District, Hai Duong, Vietnam",
    "bio": "Men's Underpants\t\nMen's T-shirts\n",
    "password": "qvMHaoPSob",
    "accountType": "COMPANY",
    "name": "Mayfair Garment Factory Ltd",
    "nickname": "Mayfair Garment Factory Ltd",
    "email": "mayfair.vn@gopy.io",
    "phone": "+842203952005",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef413217ac70e0246c0bf27"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-25T02:59:45.456Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef42619f0d8ec0046a42568"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "D1 Street, Suoi Tre Industrial Zone, Bao Vinh Ward, Long Khanh City, Dong Nai, Vietnam",
    "bio": "Handbags\t\nPurses\n",
    "password": "smpVdYgwHO",
    "accountType": "COMPANY",
    "name": "Branch of Leatherbank Co., Ltd",
    "nickname": "Branch of Leatherbank Co., Ltd",
    "email": "leatherbank.vn@gopy.io",
    "phone": "+84939669067",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c570dbbfb83ee8535614"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef42619f0d8ec0046a42567"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-25T04:20:41.307Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef426bdf0d8ec0046a4256a"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot 25, Road 1, Tan Duc Industrial Zone, Duc Hoa Ha Commune, Duc Hoa District, Long An, Vietnam",
    "bio": "Sport protective equipment\nSport protective equipment\n",
    "password": "pTAr7kpkLP",
    "accountType": "COMPANY",
    "name": "Praegear Vietnam Co., Ltd",
    "nickname": "Praegear Vietnam Co., Ltd",
    "email": "praegear.vn@gopy.io",
    "phone": "+842723774085",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef426bdf0d8ec0046a42569"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-25T04:23:25.666Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef444b1f0d8ec0046a42577"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Ly Thuong Kiet Street, Thai Binh City, Vietnam",
    "bio": "Shirts\nShirts\n",
    "password": "yKKeUVvDw1",
    "accountType": "COMPANY",
    "name": "Branch of Garment 10 Corp JSC - Thai Ha Garment Factory",
    "nickname": "Branch of Garment 10 Corp JSC - Thai Ha Garment Factory",
    "email": "thaiha.vn@gopy.io",
    "phone": "+842273736126",
    "companySize": {
      "$oid": "5d60c506dbbfb83ee853560f"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c40bdbbfb83ee8535605"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef444b1f0d8ec0046a42576"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-25T06:31:13.859Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef447c7f0d8ec0046a4257b"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Lot 28-30-32-34 Road No.9, Tan Duc Industrial Park, Duc Hoa Ha Commune, Long An, Vietnam",
    "bio": "Shoes\t\nShoes\n",
    "password": "wZQqUl1kII",
    "accountType": "COMPANY",
    "name": "Aster Co., Ltd",
    "nickname": "Aster Co., Ltd",
    "email": "aster.vn@gopy.io",
    "phone": "+842723769633",
    "companySize": {
      "$oid": "5d60c526dbbfb83ee8535611"
    },
    "companyIndustry": {
      "$oid": "5d60c570dbbfb83ee8535614"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef447c7f0d8ec0046a4257a"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-25T06:44:23.988Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5ef45623f0d8ec0046a4258a"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "No. 118, Long Binh (Amata) Industrial Park, Long Binh Ward, Dong Nai, Vietnam",
    "bio": "Bras\t\nUnderwear\n",
    "password": "daICgJoMNr",
    "accountType": "COMPANY",
    "name": "Quadrille Vietnam Co., Ltd",
    "nickname": "Quadrille Vietnam ",
    "email": "quadrille.vn@gopy.io",
    "phone": "+842513936660",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef45623f0d8ec0046a42589"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-25T07:45:39.751Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-06-25T08:03:00.695Z"
    }
  },
  {
    "_id": {
      "$oid": "5ef466987ac70e0246c0bf75"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Plot 66, the Map no. B4 (DC15), Binh Thuan Quarters, Thuan Giao Wards, BInh Duong, Vietnam",
    "bio": "Tops\t\nDresses\n",
    "password": "6MMe2C5Rdz",
    "accountType": "COMPANY",
    "name": "KNT Phoong Tin Vina 4 Co., Ltd",
    "nickname": "KNT Phoong Tin Vina 4 ",
    "email": "knt.poongin@gopy.io",
    "phone": "+842743716631",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef466987ac70e0246c0bf74"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-25T08:55:52.459Z"
    },
    "__v": 0,
    "updatedBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "updatedDate": {
      "$date": "2020-07-02T04:54:23.588Z"
    }
  },
  {
    "_id": {
      "$oid": "5ef561e27ac70e0246c0bf89"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "Thuong Commune, Tan Thong Hoi Village, Cu Chi district, Ho Chi Minh, Vietnam",
    "bio": "Jackets, T-shirts, pants, jackets\t\nPullovers\n",
    "password": "GWK0Bp1LuR",
    "accountType": "COMPANY",
    "name": "VN10030CHC (Cao Hoa Co.)",
    "nickname": "VN10030CHC (Cao Hoa Co.)",
    "email": "cao.hoa@gopy.io",
    "phone": "837901570",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c57adbbfb83ee8535615"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5ef561e27ac70e0246c0bf88"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-06-26T02:48:02.652Z"
    },
    "__v": 0
  },
  {
    "_id": {
      "$oid": "5f7558a07cffdf003d5436f2"
    },
    "avatar": "",
    "gopy": 20,
    "status": true,
    "active": true,
    "address": "231 Chiến Lược, Khu Phố 18, P, Bình Tân",
    "bio": "",
    "password": "dPhxRxWd7s",
    "accountType": "COMPANY",
    "name": "Cty TNHH Vạn Xuân",
    "nickname": "Cty TNHH Vạn Xuân",
    "email": "van.xuan@gopy.io",
    "phone": "0931288789",
    "companySize": {
      "$oid": "5d60c519dbbfb83ee8535610"
    },
    "companyIndustry": {
      "$oid": "5d60c5eddbbfb83ee853561d"
    },
    "companyType": {
      "$oid": "5d60c3fbdbbfb83ee8535604"
    },
    "taxCode": "",
    "companyId": {
      "$oid": "5f7558a07cffdf003d5436f1"
    },
    "createdBy": {
      "$oid": "5d903546d3097f2167da9cff"
    },
    "createdDate": {
      "$date": "2020-10-01T04:18:40.342Z"
    },
    "__v": 0
  }
]
export default DataCompanies;

const DataGroupId = [{
  "_id": {
    "$oid": "5ec253d6329e33023ffd6a17"
  },
  "status": false,
  "code": "BWV_W_V_Indabc",
  "subjectId": {
    "$oid": "5ec25364329e33023ffd6a10"
  },
  "name": [
    {
      "code": "EN",
      "text": "Questionnaire related to personal information of employees.",
      "_id": {
        "$oid": "5ec253d6329e33023ffd6a1a"
      }
    },
    {
      "code": "KO",
      "text": "Questionnaire related to personal information of employees.",
      "_id": {
        "$oid": "5ec253d6329e33023ffd6a19"
      }
    },
    {
      "code": "VI",
      "text": "Bộ câu hỏi liên quan đến thông tin cá nhân của NLĐ.",
      "_id": {
        "$oid": "5ec253d6329e33023ffd6a18"
      }
    }
  ],
  "description": [
    {
      "code": "VI",
      "text": "Bộ câu hỏi liên quan đến thông tin cá nhân của NLĐ.",
      "_id": {
        "$oid": "5ec253d6329e33023ffd6a1d"
      }
    },
    {
      "code": "EN",
      "text": "Questionnaire related to personal information of employees.",
      "_id": {
        "$oid": "5ec253d6329e33023ffd6a1c"
      }
    },
    {
      "code": "KO",
      "text": "Questionnaire related to personal information of employees.",
      "_id": {
        "$oid": "5ec253d6329e33023ffd6a1b"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-18T09:22:30.039Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-26T08:09:40.379Z"
  }
},{
  "_id": {
    "$oid": "5ec25530329e33023ffd6b8d"
  },
  "status": true,
  "code": "WHO_ILO_COVID_SAFE",
  "subjectId": {
    "$oid": "5ec254ee329e33023ffd6b86"
  },
  "name": [
    {
      "code": "EN",
      "text": "The COVID-19 Occupational Safety and Health (OSH) Self-Assessment Tools (COVID SAT)",
      "_id": {
        "$oid": "5ec25530329e33023ffd6b90"
      }
    },
    {
      "code": "KO",
      "text": "The COVID-19 Occupational Safety and Health (OSH) Self-Assessment Tools (COVID SAT)",
      "_id": {
        "$oid": "5ec25530329e33023ffd6b8f"
      }
    },
    {
      "code": "VI",
      "text": "Công cụ tự đánh giá An toàn và Sức khỏe Lao động (ATSKLĐ) trong dịch COVID-19 tại nơi làm việc cho các doanh nghiệp và người lao động (COVID-SAT)",
      "_id": {
        "$oid": "5ec25530329e33023ffd6b8e"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "The COVID-19 Occupational Safety and Health (OSH) Self-Assessment Tools (COVID SAT)",
      "_id": {
        "$oid": "5ec25530329e33023ffd6b93"
      }
    },
    {
      "code": "KO",
      "text": "The COVID-19 Occupational Safety and Health (OSH) Self-Assessment Tools (COVID SAT)",
      "_id": {
        "$oid": "5ec25530329e33023ffd6b92"
      }
    },
    {
      "code": "VI",
      "text": "Công cụ tự đánh giá An toàn và Sức khỏe Lao động (ATSKLĐ) trong dịch COVID-19 tại nơi làm việc cho các doanh nghiệp và người lao động (COVID-SAT)",
      "_id": {
        "$oid": "5ec25530329e33023ffd6b91"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-18T09:28:16.989Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ec335aa329e33023ffd6f77"
  },
  "status": true,
  "code": "LbC_PbC",
  "subjectId": {
    "$oid": "5ec33535329e33023ffd6f70"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ câu hỏi về hợp đồng lao động",
      "_id": {
        "$oid": "5ec4b4e1329e33023ffd7682"
      }
    },
    {
      "code": "KO",
      "text": "Question set about employment contracts",
      "_id": {
        "$oid": "5ec4b4e1329e33023ffd7681"
      }
    },
    {
      "code": "EN",
      "text": "Question set about employment contracts",
      "_id": {
        "$oid": "5ec4b4e1329e33023ffd7680"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "Question set about employment contracts",
      "_id": {
        "$oid": "5ec4b4e1329e33023ffd767f"
      }
    },
    {
      "code": "VI",
      "text": "Bộ câu hỏi về hợp đồng lao động",
      "_id": {
        "$oid": "5ec4b4e1329e33023ffd767e"
      }
    },
    {
      "code": "KO",
      "text": "Question set about employment contracts",
      "_id": {
        "$oid": "5ec4b4e1329e33023ffd767d"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-19T01:26:02.167Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-20T04:41:05.009Z"
  }
},{
  "_id": {
    "$oid": "5ec33602329e33023ffd6f7e"
  },
  "status": true,
  "code": "Met_Dis_Agr",
  "subjectId": {
    "$oid": "5ec33535329e33023ffd6f70"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ câu hỏi về đối thoại và thương lượng tập thể",
      "_id": {
        "$oid": "5ec4d599329e33023ffd7cba"
      }
    },
    {
      "code": "EN",
      "text": "Set of questions about dialogue and collective bargaining",
      "_id": {
        "$oid": "5ec4d599329e33023ffd7cb9"
      }
    },
    {
      "code": "KO",
      "text": "Set of questions about dialogue and collective bargaining",
      "_id": {
        "$oid": "5ec4d599329e33023ffd7cb8"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "Set of questions about dialogue and collective bargaining",
      "_id": {
        "$oid": "5ec4d599329e33023ffd7cb7"
      }
    },
    {
      "code": "VI",
      "text": "Bộ câu hỏi về đối thoại và thương lượng tập thể",
      "_id": {
        "$oid": "5ec4d599329e33023ffd7cb6"
      }
    },
    {
      "code": "KO",
      "text": "Set of questions about dialogue and collective bargaining",
      "_id": {
        "$oid": "5ec4d599329e33023ffd7cb5"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-19T01:27:30.034Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-20T07:00:41.557Z"
  }
},{
  "_id": {
    "$oid": "5ec33833329e33023ffd6f85"
  },
  "status": true,
  "code": "Slr_Pol",
  "subjectId": {
    "$oid": "5ec33535329e33023ffd6f70"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ câu hỏi về chính sách tiền lương và định mức lao động",
      "_id": {
        "$oid": "5ed0aae431c54f03bbb9ad84"
      }
    },
    {
      "code": "KO",
      "text": "Question set about salary policy and labor norms",
      "_id": {
        "$oid": "5ed0aae431c54f03bbb9ad83"
      }
    },
    {
      "code": "EN",
      "text": "Question set about salary policy and labor norms",
      "_id": {
        "$oid": "5ed0aae431c54f03bbb9ad82"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "Question set about salary policy and labor norms",
      "_id": {
        "$oid": "5ed0aae431c54f03bbb9ad81"
      }
    },
    {
      "code": "VI",
      "text": "Bộ câu hỏi về chính sách tiền lương và định mức lao động",
      "_id": {
        "$oid": "5ed0aae431c54f03bbb9ad80"
      }
    },
    {
      "code": "KO",
      "text": "Question set about salary policy and labor norms",
      "_id": {
        "$oid": "5ed0aae431c54f03bbb9ad7f"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-19T01:36:51.963Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-29T06:25:40.259Z"
  }
},{
  "_id": {
    "$oid": "5ec6269e329e33023ffd7cc8"
  },
  "status": true,
  "code": "SAF_L_PCC",
  "subjectId": {
    "$oid": "5ec6266c329e33023ffd7cc1"
  },
  "name": [
    {
      "code": "VI",
      "text": "Câu hỏi nhằm xác định nhà máy có tổ chức diễn tập PCCC không?",
      "_id": {
        "$oid": "5ec6269e329e33023ffd7ccb"
      }
    },
    {
      "code": "EN",
      "text": "The question is to determine whether the factory organizes fire drills?",
      "_id": {
        "$oid": "5ec6269e329e33023ffd7cca"
      }
    },
    {
      "code": "KO",
      "text": "The question is to determine whether the factory organizes fire drills?",
      "_id": {
        "$oid": "5ec6269e329e33023ffd7cc9"
      }
    }
  ],
  "description": [],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-21T06:58:38.294Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ec626c6329e33023ffd7ccc"
  },
  "status": true,
  "code": "SAF_L_LEA",
  "subjectId": {
    "$oid": "5ec6266c329e33023ffd7cc1"
  },
  "name": [
    {
      "code": "VI",
      "text": "Câu hỏi nhằm xác định các quyền lợi về ngày nghỉ của NLĐ",
      "_id": {
        "$oid": "5ec626c6329e33023ffd7ccf"
      }
    },
    {
      "code": "EN",
      "text": "The question is to determine employees' holiday benefits",
      "_id": {
        "$oid": "5ec626c6329e33023ffd7cce"
      }
    },
    {
      "code": "KO",
      "text": "The question is to determine employees' holiday benefits",
      "_id": {
        "$oid": "5ec626c6329e33023ffd7ccd"
      }
    }
  ],
  "description": [],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-21T06:59:18.340Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ec626ee329e33023ffd7cd0"
  },
  "status": true,
  "code": "SAF_L_WRT",
  "subjectId": {
    "$oid": "5ec6266c329e33023ffd7cc1"
  },
  "name": [
    {
      "code": "VI",
      "text": "Câu hỏi nhằm xác định thời gian làm việc bình thường theo ngày hoặc theo tuần của doanh nghiệp như thế nào?",
      "_id": {
        "$oid": "5ec626ee329e33023ffd7cd3"
      }
    },
    {
      "code": "EN",
      "text": "The question is to determine the normal working time by day or by week of the business.",
      "_id": {
        "$oid": "5ec626ee329e33023ffd7cd2"
      }
    },
    {
      "code": "KO",
      "text": "The question is to determine the normal working time by day or by week of the business.",
      "_id": {
        "$oid": "5ec626ee329e33023ffd7cd1"
      }
    }
  ],
  "description": [],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-21T06:59:58.073Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5eccc151329e33023ffd83f8"
  },
  "status": false,
  "code": "PCC_Exe",
  "subjectId": {
    "$oid": "5ec6266c329e33023ffd7cc1"
  },
  "name": [
    {
      "code": "VI",
      "text": "Quản lý, xây dựng nội quy về an toàn phòng cháy chữa cháy",
      "_id": {
        "$oid": "5eccc151329e33023ffd83fb"
      }
    },
    {
      "code": "EN",
      "text": "Manage and formulate internal rules on fire safety",
      "_id": {
        "$oid": "5eccc151329e33023ffd83fa"
      }
    },
    {
      "code": "KO",
      "text": "Manage and formulate internal rules on fire safety",
      "_id": {
        "$oid": "5eccc151329e33023ffd83f9"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "Manage and formulate internal rules on fire safety",
      "_id": {
        "$oid": "5eccc151329e33023ffd83fe"
      }
    },
    {
      "code": "KO",
      "text": "Manage and formulate internal rules on fire safety",
      "_id": {
        "$oid": "5eccc151329e33023ffd83fd"
      }
    },
    {
      "code": "VI",
      "text": "Quản lý, xây dựng nội quy về an toàn phòng cháy chữa cháy",
      "_id": {
        "$oid": "5eccc151329e33023ffd83fc"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-26T07:12:17.127Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-28T06:53:45.327Z"
  }
},{
  "_id": {
    "$oid": "5eccc1d8329e33023ffd852f"
  },
  "status": false,
  "code": "Spc_Lab",
  "subjectId": {
    "$oid": "5ec6266c329e33023ffd7cc1"
  },
  "name": [
    {
      "code": "VI",
      "text": "Quy định về các chế độ, phúc lợi cho lao động nữ",
      "_id": {
        "$oid": "5eccc1d8329e33023ffd8532"
      }
    },
    {
      "code": "EN",
      "text": "Regulations on regimes and benefits for female workers",
      "_id": {
        "$oid": "5eccc1d8329e33023ffd8531"
      }
    },
    {
      "code": "KO",
      "text": "Regulations on regimes and benefits for female workers",
      "_id": {
        "$oid": "5eccc1d8329e33023ffd8530"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "Regulations on regimes and benefits for female workers",
      "_id": {
        "$oid": "5eccc1d8329e33023ffd8535"
      }
    },
    {
      "code": "KO",
      "text": "Regulations on regimes and benefits for female workers",
      "_id": {
        "$oid": "5eccc1d8329e33023ffd8534"
      }
    },
    {
      "code": "VI",
      "text": "Quy định về các chế độ, phúc lợi cho lao động nữ",
      "_id": {
        "$oid": "5eccc1d8329e33023ffd8533"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-26T07:14:32.547Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-28T07:07:32.310Z"
  }
},{
  "_id": {
    "$oid": "5eccc817329e33023ffd874a"
  },
  "status": false,
  "code": "Wrk_Saf_Pol",
  "subjectId": {
    "$oid": "5ec6266c329e33023ffd7cc1"
  },
  "name": [
    {
      "code": "VI",
      "text": "Kế hoạch an toàn lao động, vệ sinh lao động hàng năm",
      "_id": {
        "$oid": "5eccc817329e33023ffd874d"
      }
    },
    {
      "code": "EN",
      "text": "Annual plan for labor safety and hygiene",
      "_id": {
        "$oid": "5eccc817329e33023ffd874c"
      }
    },
    {
      "code": "KO",
      "text": "Annual plan for labor safety and hygiene",
      "_id": {
        "$oid": "5eccc817329e33023ffd874b"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "Annual plan for labor safety and hygiene",
      "_id": {
        "$oid": "5eccc817329e33023ffd8750"
      }
    },
    {
      "code": "KO",
      "text": "Annual plan for labor safety and hygiene",
      "_id": {
        "$oid": "5eccc817329e33023ffd874f"
      }
    },
    {
      "code": "VI",
      "text": "Kế hoạch an toàn lao động, vệ sinh lao động hàng năm",
      "_id": {
        "$oid": "5eccc817329e33023ffd874e"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-26T07:41:11.998Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-28T07:14:31.360Z"
  }
},{
  "_id": {
    "$oid": "5ece156f329e33023ffd89f6"
  },
  "status": true,
  "code": "BWV_W_V_Ind",
  "subjectId": {
    "$oid": "5ec25364329e33023ffd6a10"
  },
  "name": [
    {
      "code": "VI",
      "text": "Mục đích của câu hỏi là để xác định thông tin của người lao động",
      "_id": {
        "$oid": "5ece156f329e33023ffd89f9"
      }
    },
    {
      "code": "EN",
      "text": "The purpose of the question is to identify employee information",
      "_id": {
        "$oid": "5ece156f329e33023ffd89f8"
      }
    },
    {
      "code": "KO",
      "text": "The purpose of the question is to identify employee information",
      "_id": {
        "$oid": "5ece156f329e33023ffd89f7"
      }
    }
  ],
  "description": [
    {
      "code": "VI",
      "text": "Mục đích của câu hỏi là để xác định thông tin của người lao động",
      "_id": {
        "$oid": "5ece156f329e33023ffd89fc"
      }
    },
    {
      "code": "EN",
      "text": "The purpose of the question is to identify employee information",
      "_id": {
        "$oid": "5ece156f329e33023ffd89fb"
      }
    },
    {
      "code": "KO",
      "text": "The purpose of the question is to identify employee information",
      "_id": {
        "$oid": "5ece156f329e33023ffd89fa"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-27T07:23:27.253Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ece18f2329e33023ffd8dd7"
  },
  "status": true,
  "code": "BWV_W_V_Wrk",
  "subjectId": {
    "$oid": "5ec25364329e33023ffd6a10"
  },
  "name": [
    {
      "code": "VI",
      "text": "Câu hỏi gợi ý nhằm kiểm tra vị trí hoặc chức vụ hiện tại của NLĐ tại nơi làm việc hiện tại.",
      "_id": {
        "$oid": "5ece18f2329e33023ffd8dda"
      }
    },
    {
      "code": "EN",
      "text": "Suggested questions to check the current position or position of employees in the current workplace.",
      "_id": {
        "$oid": "5ece18f2329e33023ffd8dd9"
      }
    },
    {
      "code": "KO",
      "text": "Suggested questions to check the current position or position of employees in the current workplace.",
      "_id": {
        "$oid": "5ece18f2329e33023ffd8dd8"
      }
    }
  ],
  "description": [],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-27T07:38:26.082Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ecf3875329e33023ffd93ee"
  },
  "status": true,
  "code": "BWV_W_V_Env",
  "subjectId": {
    "$oid": "5ec25364329e33023ffd6a10"
  },
  "name": [
    {
      "code": "VI",
      "text": "Doanh nghiệp phải có chính sách và quy định về an toàn và bảo hộ lao động trong hoạt động kinh doanh, sản xuất theo đúng quy định của pháp luật.",
      "_id": {
        "$oid": "5ecf3875329e33023ffd93f1"
      }
    },
    {
      "code": "EN",
      "text": "Enterprises must have policies and regulations on labor safety and protection in their business and production activities strictly according to law provisions.",
      "_id": {
        "$oid": "5ecf3875329e33023ffd93f0"
      }
    },
    {
      "code": "KO",
      "text": "Enterprises must have policies and regulations on labor safety and protection in their business and production activities strictly according to law provisions.",
      "_id": {
        "$oid": "5ecf3875329e33023ffd93ef"
      }
    }
  ],
  "description": [],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-28T04:05:09.535Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ecf38e5329e33023ffd9690"
  },
  "status": true,
  "code": "BWV_W_V_Csr",
  "subjectId": {
    "$oid": "5ec25364329e33023ffd6a10"
  },
  "name": [
    {
      "code": "VI",
      "text": "Doanh nghiệp phải có chính sách tuyển dụng, sử dụng và khuyến khích nhân tài, lao động có chuyên môn tay nghề cao trong từng lĩnh vực.",
      "_id": {
        "$oid": "5ecf38e5329e33023ffd9693"
      }
    },
    {
      "code": "EN",
      "text": "Enterprises must have policies to recruit, use and encourage talented people and workers with high professional skills in each field",
      "_id": {
        "$oid": "5ecf38e5329e33023ffd9692"
      }
    },
    {
      "code": "KO",
      "text": "Enterprises must have policies to recruit, use and encourage talented people and workers with high professional skills in each field",
      "_id": {
        "$oid": "5ecf38e5329e33023ffd9691"
      }
    }
  ],
  "description": [],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-28T04:07:01.873Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ecf5ca6329e33023ffd98e2"
  },
  "status": true,
  "code": "PCC_Exe_M",
  "subjectId": {
    "$oid": "5ec33535329e33023ffd6f70"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ câu hỏi về việc quản lý, xây dựng nội quy về an toàn phòng cháy chữa cháy",
      "_id": {
        "$oid": "5ed0aac231c54f03bbb9ad78"
      }
    },
    {
      "code": "EN",
      "text": "Question set regarding to management and formulation of rules on fire safety",
      "_id": {
        "$oid": "5ed0aac231c54f03bbb9ad77"
      }
    },
    {
      "code": "KO",
      "text": "Question set regarding to management and formulation of rules on fire safety",
      "_id": {
        "$oid": "5ed0aac231c54f03bbb9ad76"
      }
    }
  ],
  "description": [
    {
      "code": "VI",
      "text": "Bộ câu hỏi về việc quản lý, xây dựng nội quy về an toàn phòng cháy chữa cháy",
      "_id": {
        "$oid": "5ed0aac231c54f03bbb9ad75"
      }
    },
    {
      "code": "EN",
      "text": "Question set regarding to management and formulation of rules on fire safety",
      "_id": {
        "$oid": "5ed0aac231c54f03bbb9ad74"
      }
    },
    {
      "code": "KO",
      "text": "Question set regarding to management and formulation of rules on fire safety",
      "_id": {
        "$oid": "5ed0aac231c54f03bbb9ad73"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-28T06:39:34.404Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-29T06:25:06.949Z"
  }
},{
  "_id": {
    "$oid": "5ecf5d54329e33023ffd9af6"
  },
  "status": true,
  "code": "Spc_Lab_M",
  "subjectId": {
    "$oid": "5ec33535329e33023ffd6f70"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ câu hỏi về việc quy định về các chế độ, phúc lợi cho lao động nữ",
      "_id": {
        "$oid": "5ed0ab2231c54f03bbb9ad8a"
      }
    },
    {
      "code": "EN",
      "text": "Question set regarding to regulations and benefits for female workers",
      "_id": {
        "$oid": "5ed0ab2231c54f03bbb9ad89"
      }
    },
    {
      "code": "KO",
      "text": "Question set regarding to regulations and benefits for female workers",
      "_id": {
        "$oid": "5ed0ab2231c54f03bbb9ad88"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "Question set regarding to regulations and benefits for female workers",
      "_id": {
        "$oid": "5ed0ab2231c54f03bbb9ad87"
      }
    },
    {
      "code": "KO",
      "text": "Question set regarding to regulations and benefits for female workers",
      "_id": {
        "$oid": "5ed0ab2231c54f03bbb9ad86"
      }
    },
    {
      "code": "VI",
      "text": "Bộ câu hỏi về việc quy định về các chế độ, phúc lợi cho lao động nữ",
      "_id": {
        "$oid": "5ed0ab2231c54f03bbb9ad85"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-28T06:42:28.521Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-29T06:26:42.127Z"
  }
},{
  "_id": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "status": true,
  "code": "Wrk_Saf_Pol_M",
  "subjectId": {
    "$oid": "5ec33535329e33023ffd6f70"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ câu hỏi về việc kế hoạch an toàn lao động, vệ sinh lao động",
      "_id": {
        "$oid": "5ed0ab6131c54f03bbb9ad90"
      }
    },
    {
      "code": "EN",
      "text": "Question set regarding to labor safety and hygiene plans",
      "_id": {
        "$oid": "5ed0ab6131c54f03bbb9ad8f"
      }
    },
    {
      "code": "KO",
      "text": "Question set regarding to labor safety and hygiene plans",
      "_id": {
        "$oid": "5ed0ab6131c54f03bbb9ad8e"
      }
    }
  ],
  "description": [
    {
      "code": "VI",
      "text": "Bộ câu hỏi về việc kế hoạch an toàn lao động, vệ sinh lao động",
      "_id": {
        "$oid": "5ed0ab6131c54f03bbb9ad8d"
      }
    },
    {
      "code": "EN",
      "text": "Question set regarding to labor safety and hygiene plans",
      "_id": {
        "$oid": "5ed0ab6131c54f03bbb9ad8c"
      }
    },
    {
      "code": "KO",
      "text": "Question set regarding to labor safety and hygiene plans",
      "_id": {
        "$oid": "5ed0ab6131c54f03bbb9ad8b"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-28T07:10:31.425Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-05-29T06:27:45.284Z"
  }
},{
  "_id": {
    "$oid": "5ecf6db0329e33023ffda428"
  },
  "status": true,
  "code": "BWV_W_M_Wrk",
  "subjectId": {
    "$oid": "5ecf6d80329e33023ffda424"
  },
  "name": [
    {
      "code": "VI",
      "text": "Câu hỏi gợi ý nhằm kiểm tra vị trí hoặc chức vụ hiện tại của NLĐ tại nơi làm việc hiện tại.",
      "_id": {
        "$oid": "5ecf6db0329e33023ffda42b"
      }
    },
    {
      "code": "EN",
      "text": "Suggested questions to check the current position or position of employees in the current workplace.",
      "_id": {
        "$oid": "5ecf6db0329e33023ffda42a"
      }
    },
    {
      "code": "KO",
      "text": "Suggested questions to check the current position or position of employees in the current workplace.",
      "_id": {
        "$oid": "5ecf6db0329e33023ffda429"
      }
    }
  ],
  "description": [],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-28T07:52:16.392Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ecf6de7329e33023ffda42c"
  },
  "status": true,
  "code": "BWV_W_M_Csr",
  "subjectId": {
    "$oid": "5ecf6d80329e33023ffda424"
  },
  "name": [
    {
      "code": "VI",
      "text": "Câu hỏi gợi ý nhằm kiểm tra xem Doanh nghiệp có làm việc với thanh tra lao động chính phủ không?",
      "_id": {
        "$oid": "5ecf6de7329e33023ffda42f"
      }
    },
    {
      "code": "EN",
      "text": "Suggested question to check if the Enterprise works with government labor inspectorate?",
      "_id": {
        "$oid": "5ecf6de7329e33023ffda42e"
      }
    },
    {
      "code": "KO",
      "text": "Suggested question to check if the Enterprise works with government labor inspectorate?",
      "_id": {
        "$oid": "5ecf6de7329e33023ffda42d"
      }
    }
  ],
  "description": [],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-05-28T07:53:11.266Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f2bb13fab33ab00425a5742"
  },
  "status": true,
  "code": "MOD_LC_VN_V",
  "subjectId": {
    "$oid": "5f2baee78831b4003db4b9b8"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ Câu Hỏi Liên Quan Đến Hợp Đồng Lao Động",
      "_id": {
        "$oid": "5f2bb13fab33ab00425a5745"
      }
    },
    {
      "code": "EN",
      "text": "Question Set Regarding To Labor Contract",
      "_id": {
        "$oid": "5f2bb13fab33ab00425a5744"
      }
    },
    {
      "code": "KO",
      "text": "Question Set Regarding To Labor Contract",
      "_id": {
        "$oid": "5f2bb13fab33ab00425a5743"
      }
    }
  ],
  "description": [
    {
      "code": "VI",
      "text": "Bộ Câu Hỏi Liên Quan Đến Hợp Đồng Lao Động",
      "_id": {
        "$oid": "5f2bb13fab33ab00425a5748"
      }
    },
    {
      "code": "EN",
      "text": "Question Set Regarding To Labor Contract",
      "_id": {
        "$oid": "5f2bb13fab33ab00425a5747"
      }
    },
    {
      "code": "KO",
      "text": "Question Set Regarding To Labor Contract",
      "_id": {
        "$oid": "5f2bb13fab33ab00425a5746"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-08-06T07:29:03.219Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f2bb1a5ab33ab00425a5749"
  },
  "status": true,
  "code": "MOD_OSH_VN_V",
  "subjectId": {
    "$oid": "5f2baee78831b4003db4b9b8"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ Câu Hỏi Liên Quan Đến Vệ Sinh An Toàn Lao Động",
      "_id": {
        "$oid": "5f2bb1a5ab33ab00425a574c"
      }
    },
    {
      "code": "EN",
      "text": "Question Set Regarding To OSH",
      "_id": {
        "$oid": "5f2bb1a5ab33ab00425a574b"
      }
    },
    {
      "code": "KO",
      "text": "Question Set Regarding To OSH",
      "_id": {
        "$oid": "5f2bb1a5ab33ab00425a574a"
      }
    }
  ],
  "description": [
    {
      "code": "VI",
      "text": "Bộ Câu Hỏi Liên Quan Đến Vệ Sinh An Toàn Lao Động",
      "_id": {
        "$oid": "5f2bb1a5ab33ab00425a574f"
      }
    },
    {
      "code": "EN",
      "text": "Question Set Regarding To OSH",
      "_id": {
        "$oid": "5f2bb1a5ab33ab00425a574e"
      }
    },
    {
      "code": "KO",
      "text": "Question Set Regarding To OSH",
      "_id": {
        "$oid": "5f2bb1a5ab33ab00425a574d"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-08-06T07:30:45.910Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f2bb234ab33ab00425a5750"
  },
  "status": true,
  "code": "MOD_WB_VN_V",
  "subjectId": {
    "$oid": "5f2baee78831b4003db4b9b8"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ Câu Hỏi Liên Quan Đến Chế Độ Cho Người Lao Động",
      "_id": {
        "$oid": "5f2bb41bab33ab00425a59b9"
      }
    },
    {
      "code": "EN",
      "text": "Question Set Regarding To Wave and Benefits",
      "_id": {
        "$oid": "5f2bb41bab33ab00425a59b8"
      }
    },
    {
      "code": "KO",
      "text": "Question Set Regarding To Wave and Benefits",
      "_id": {
        "$oid": "5f2bb41bab33ab00425a59b7"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "Question Set Regarding To Wave and Benefits",
      "_id": {
        "$oid": "5f2bb41bab33ab00425a59b6"
      }
    },
    {
      "code": "KO",
      "text": "Question Set Regarding To Wave and Benefits",
      "_id": {
        "$oid": "5f2bb41bab33ab00425a59b5"
      }
    },
    {
      "code": "VI",
      "text": "Bộ Câu Hỏi Liên Quan Đến Chế Độ Cho Người Lao Động",
      "_id": {
        "$oid": "5f2bb41bab33ab00425a59b4"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-08-06T07:33:08.206Z"
  },
  "__v": 0,
  "updatedBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "updatedDate": {
    "$date": "2020-08-06T07:41:15.756Z"
  }
},{
  "_id": {
    "$oid": "5f2bb2dcab33ab00425a5757"
  },
  "status": true,
  "code": "MOD_MGR_VN_V",
  "subjectId": {
    "$oid": "5f2baee78831b4003db4b9b8"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ Câu Hỏi Liên Quan Đến Đời Sống Của NLĐ",
      "_id": {
        "$oid": "5f2bb2dcab33ab00425a575a"
      }
    },
    {
      "code": "EN",
      "text": "Question Set Regarding To Worker's Life",
      "_id": {
        "$oid": "5f2bb2dcab33ab00425a5759"
      }
    },
    {
      "code": "KO",
      "text": "Question Set Regarding To Worker's Life",
      "_id": {
        "$oid": "5f2bb2dcab33ab00425a5758"
      }
    }
  ],
  "description": [
    {
      "code": "VI",
      "text": "Question Set Regarding To Worker's Life",
      "_id": {
        "$oid": "5f2bb2dcab33ab00425a575c"
      }
    },
    {
      "code": "EN",
      "text": "Bộ Câu Hỏi Liên Quan Đến Đời Sống Của NLĐ",
      "_id": {
        "$oid": "5f2bb2dcab33ab00425a575b"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-08-06T07:35:56.944Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f2bb32dab33ab00425a575d"
  },
  "status": true,
  "code": "MOD_RLS_VN_V",
  "subjectId": {
    "$oid": "5f2baee78831b4003db4b9b8"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ Câu Hỏi Liên Quan Đến Các Mối Quan Hệ Của NLĐ",
      "_id": {
        "$oid": "5f2bb32dab33ab00425a5760"
      }
    },
    {
      "code": "EN",
      "text": "Question Set Regarding To Worker's Relationships",
      "_id": {
        "$oid": "5f2bb32dab33ab00425a575f"
      }
    },
    {
      "code": "KO",
      "text": "Question Set Regarding To Worker's Relationships",
      "_id": {
        "$oid": "5f2bb32dab33ab00425a575e"
      }
    }
  ],
  "description": [
    {
      "code": "EN",
      "text": "Question Set Regarding To Worker's Relationships",
      "_id": {
        "$oid": "5f2bb32dab33ab00425a5763"
      }
    },
    {
      "code": "KO",
      "text": "Question Set Regarding To Worker's Relationships",
      "_id": {
        "$oid": "5f2bb32dab33ab00425a5762"
      }
    },
    {
      "code": "VI",
      "text": "Bộ Câu Hỏi Liên Quan Đến Các Mối Quan Hệ Của NLĐ",
      "_id": {
        "$oid": "5f2bb32dab33ab00425a5761"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-08-06T07:37:17.093Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5f00c4526aae004205868f"
  },
  "status": true,
  "code": "BB_BW_VN",
  "subjectId": {
    "$oid": "5f572f86051e0d003d47133e"
  },
  "name": [
    {
      "code": "VI",
      "text": "Bộ khảo sát thực hành mua hàng của Better BuyingTM và Better Work.",
      "_id": {
        "$oid": "5f5f00c4526aae0042058692"
      }
    },
    {
      "code": "EN",
      "text": "The collaborated survey of Better BuyingTM and Better Work.",
      "_id": {
        "$oid": "5f5f00c4526aae0042058691"
      }
    },
    {
      "code": "KO",
      "text": "The collaborated survey of Better BuyingTM and Better Work.",
      "_id": {
        "$oid": "5f5f00c4526aae0042058690"
      }
    }
  ],
  "description": [
    {
      "code": "VI",
      "text": "Bộ khảo sát thực hành mua hàng của Better BuyingTM và Better Work.",
      "_id": {
        "$oid": "5f5f00c4526aae0042058695"
      }
    },
    {
      "code": "EN",
      "text": "The collaborated survey of Better BuyingTM and Better Work.",
      "_id": {
        "$oid": "5f5f00c4526aae0042058694"
      }
    },
    {
      "code": "KO",
      "text": "The collaborated survey of Better BuyingTM and Better Work.",
      "_id": {
        "$oid": "5f5f00c4526aae0042058693"
      }
    }
  ],
  "createdBy": {
    "$oid": "5d903546d3097f2167da9cff"
  },
  "createdDate": {
    "$date": "2020-09-14T05:33:56.574Z"
  },
  "__v": 0
}]
export default DataGroupId;

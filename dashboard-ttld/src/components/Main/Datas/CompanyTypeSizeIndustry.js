const companyTSIData = [{
  "_id": {
    "$oid": "5d60c3b5dbbfb83ee8535603"
  },
  "type": "COMPANY_TYPE",
  "value": "Private Company",
  "code": "CT_001",
  "create_date": {
    "$date": "2019-08-24T04:57:25.626Z"
  },
  "updated_date": {
    "$date": "2019-08-24T04:57:25.627Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c3fbdbbfb83ee8535604"
  },
  "type": "COMPANY_TYPE",
  "value": "Company Limited",
  "code": "CT_002",
  "create_date": {
    "$date": "2019-08-24T04:58:35.279Z"
  },
  "updated_date": {
    "$date": "2019-08-24T04:58:35.279Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c40bdbbfb83ee8535605"
  },
  "type": "COMPANY_TYPE",
  "value": "Joint Stock Company",
  "code": "CT_003",
  "create_date": {
    "$date": "2019-08-24T04:58:51.033Z"
  },
  "updated_date": {
    "$date": "2019-08-24T04:58:51.033Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c419dbbfb83ee8535606"
  },
  "type": "COMPANY_TYPE",
  "value": "Joint Venture Company",
  "code": "CT_004",
  "create_date": {
    "$date": "2019-08-24T04:59:05.891Z"
  },
  "updated_date": {
    "$date": "2019-08-24T04:59:05.891Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c426dbbfb83ee8535607"
  },
  "type": "COMPANY_TYPE",
  "value": "100% Foreign Owned Company",
  "code": "CT_005",
  "create_date": {
    "$date": "2019-08-24T04:59:18.480Z"
  },
  "updated_date": {
    "$date": "2019-08-24T04:59:18.480Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c496dbbfb83ee8535608"
  },
  "type": "COMPANY_SIZE",
  "value": "1 - 9",
  "code": "CS_001",
  "create_date": {
    "$date": "2019-08-24T05:01:10.169Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:01:10.169Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c4a7dbbfb83ee8535609"
  },
  "type": "COMPANY_SIZE",
  "value": "10 - 19",
  "code": "CS_002",
  "create_date": {
    "$date": "2019-08-24T05:01:27.641Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:01:27.641Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c4b8dbbfb83ee853560a"
  },
  "type": "COMPANY_SIZE",
  "value": "20 - 49",
  "code": "CS_003",
  "create_date": {
    "$date": "2019-08-24T05:01:44.792Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:01:44.792Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c4cadbbfb83ee853560b"
  },
  "type": "COMPANY_SIZE",
  "value": "50 - 99",
  "code": "CS_004",
  "create_date": {
    "$date": "2019-08-24T05:02:02.347Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:02:02.347Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c4efdbbfb83ee853560e"
  },
  "type": "COMPANY_SIZE",
  "value": "100 - 249",
  "code": "CS_005",
  "create_date": {
    "$date": "2019-08-24T05:02:39.431Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:02:39.431Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c506dbbfb83ee853560f"
  },
  "type": "COMPANY_SIZE",
  "value": "250 - 499",
  "code": "CS_006",
  "create_date": {
    "$date": "2019-08-24T05:03:02.057Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:03:02.057Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c519dbbfb83ee8535610"
  },
  "type": "COMPANY_SIZE",
  "value": "500 - 999",
  "code": "CS_007",
  "create_date": {
    "$date": "2019-08-24T05:03:21.174Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:03:21.174Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c526dbbfb83ee8535611"
  },
  "type": "COMPANY_SIZE",
  "value": "1,000 - 1,999",
  "code": "CS_008",
  "create_date": {
    "$date": "2019-08-24T05:03:34.270Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:03:34.270Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c534dbbfb83ee8535612"
  },
  "type": "COMPANY_SIZE",
  "value": "2,000 - 3,500",
  "code": "CS_009",
  "create_date": {
    "$date": "2019-08-24T05:03:48.318Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:03:48.318Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c565dbbfb83ee8535613"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Wood processing industry",
  "code": "CI_001",
  "create_date": {
    "$date": "2019-08-24T05:04:37.683Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:04:37.683Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c570dbbfb83ee8535614"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Leather and footwear industry",
  "code": "CI_002",
  "create_date": {
    "$date": "2019-08-24T05:04:48.762Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:04:48.763Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c57adbbfb83ee8535615"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Textile industry",
  "code": "CI_003",
  "create_date": {
    "$date": "2019-08-24T05:04:58.294Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:04:58.294Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c585dbbfb83ee8535616"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "\tElectronic manufacturing industry",
  "code": "CI_004",
  "create_date": {
    "$date": "2019-08-24T05:05:09.852Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:05:09.852Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c593dbbfb83ee8535617"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Fisheries",
  "code": "CI_005",
  "create_date": {
    "$date": "2019-08-24T05:05:23.523Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:05:23.523Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c59edbbfb83ee8535618"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Nonprofit",
  "code": "CI_006",
  "create_date": {
    "$date": "2019-08-24T05:05:34.567Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:05:34.567Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c5a9dbbfb83ee8535619"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Banking & Finance",
  "code": "CI_007",
  "create_date": {
    "$date": "2019-08-24T05:05:45.150Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:05:45.150Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c5b7dbbfb83ee853561a"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Government",
  "code": "CI_008",
  "create_date": {
    "$date": "2019-08-24T05:05:59.229Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:05:59.229Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c5c4dbbfb83ee853561b"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "General Business",
  "code": "CI_009",
  "create_date": {
    "$date": "2019-08-24T05:06:12.842Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:06:12.842Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c5d3dbbfb83ee853561c"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "General Labor",
  "code": "CI_010",
  "create_date": {
    "$date": "2019-08-24T05:06:27.821Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:06:27.821Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c5eddbbfb83ee853561d"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Pharmaceutical",
  "code": "CI_011",
  "create_date": {
    "$date": "2019-08-24T05:06:53.602Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:06:53.602Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c5f7dbbfb83ee853561e"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Automotive",
  "code": "CI_012",
  "create_date": {
    "$date": "2019-08-24T05:07:03.884Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:07:03.884Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c602dbbfb83ee853561f"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Professional Services",
  "code": "CI_013",
  "create_date": {
    "$date": "2019-08-24T05:07:14.653Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:07:14.653Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c610dbbfb83ee8535620"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Banking",
  "code": "CI_014",
  "create_date": {
    "$date": "2019-08-24T05:07:28.598Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:07:28.598Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c62adbbfb83ee8535621"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Education - Teaching",
  "code": "CI_015",
  "create_date": {
    "$date": "2019-08-24T05:07:54.627Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:07:54.627Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c63bdbbfb83ee8535622"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Legal Admin",
  "code": "CI_016",
  "create_date": {
    "$date": "2019-08-24T05:08:11.597Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:08:11.597Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c649dbbfb83ee8535623"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Strategy - Planning",
  "code": "CI_017",
  "create_date": {
    "$date": "2019-08-24T05:08:25.686Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:08:25.686Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c657dbbfb83ee8535624"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Management",
  "code": "CI_018",
  "create_date": {
    "$date": "2019-08-24T05:08:39.316Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:08:39.316Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c660dbbfb83ee8535625"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Telecommunications",
  "code": "CI_019",
  "create_date": {
    "$date": "2019-08-24T05:08:48.848Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:08:48.848Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5d60c66edbbfb83ee8535626"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Training",
  "code": "CI_020",
  "create_date": {
    "$date": "2019-08-24T05:09:02.205Z"
  },
  "updated_date": {
    "$date": "2019-08-24T05:09:02.205Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5e71ce273381c60202ee783e"
  },
  "type": "COMPANY_SIZE",
  "value": "3,500 - 5,000",
  "code": "CS_010",
  "__v": 0
},{
  "_id": {
    "$oid": "5e71ce773381c60202ee783f"
  },
  "type": "COMPANY_SIZE",
  "value": "> 5,000",
  "code": "CS_011",
  "__v": 0
},{
  "_id": {
    "$oid": "5ee305cc7a820d0297097833"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Logistics",
  "code": "CI_021",
  "__v": 0
},{
  "_id": {
    "$oid": "5ee311407a820d0297097834"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Manufacturing",
  "code": "CI_022",
  "__v": 0
},{
  "_id": {
    "$oid": "5ee317917a820d0297097835"
  },
  "type": "COMPANY_INDUSTRY",
  "value": "Foods and Beverages",
  "code": "CI_023",
  "__v": 0
},{
  "_id": {
    "$oid": "5f6c18ae170ee900428a2fbb"
  },
  "type": "COMPANY_TYPE",
  "value": "None Profit Organization",
  "code": "CT_007",
  "__v": 0
}]
//let companyTSIData = JSON.parse(Data);
export default companyTSIData
const DataAnswers = [{
  "_id": {
    "$oid": "5ec289f6329e33023ffd6f56"
  },
  "groupId": {
    "$oid": "5ec25530329e33023ffd6b8d"
  },
  "companyId": {
    "$oid": "5e71a43453d9d402ddccb02c"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f6f"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6ccb"
      },
      "questionCode": "ILO_COVID_SAFE_01",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6dbd"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f6e"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6d39"
      },
      "questionCode": "ILO_COVID_SAFE_02",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6e39"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f6d"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6bef"
      },
      "questionCode": "ILO_COVID_SAFE_03",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6e19"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f6c"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6b97"
      },
      "questionCode": "ILO_COVID_SAFE_04",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6eed"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f6b"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6c5d"
      },
      "questionCode": "ILO_COVID_SAFE_05",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6e7d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f6a"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6ce1"
      },
      "questionCode": "ILO_COVID_SAFE_06",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6de1"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f69"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6c05"
      },
      "questionCode": "ILO_COVID_SAFE_07",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6e0d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f68"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6bad"
      },
      "questionCode": "ILO_COVID_SAFE_08",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6dd5"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f67"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6c73"
      },
      "questionCode": "ILO_COVID_SAFE_09",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6e1d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f66"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6d4f"
      },
      "questionCode": "ILO_COVID_SAFE_10",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6ea9"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f65"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6cf7"
      },
      "questionCode": "ILO_COVID_SAFE_11",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6ef5"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f64"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6c1b"
      },
      "questionCode": "ILO_COVID_SAFE_12",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6dc9"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f63"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6bc3"
      },
      "questionCode": "ILO_COVID_SAFE_13",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6df9"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f62"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6c89"
      },
      "questionCode": "ILO_COVID_SAFE_14",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6dd9"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f61"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6d65"
      },
      "questionCode": "ILO_COVID_SAFE_15",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6f09"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f60"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6d0d"
      },
      "questionCode": "ILO_COVID_SAFE_16",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6ead"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f5f"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6c31"
      },
      "questionCode": "ILO_COVID_SAFE_17",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6ded"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f5e"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6bd9"
      },
      "questionCode": "ILO_COVID_SAFE_18",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6dcd"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f5d"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6c9f"
      },
      "questionCode": "ILO_COVID_SAFE_19",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6eb9"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f5c"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6d7b"
      },
      "questionCode": "ILO_COVID_SAFE_20",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6e91"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f5b"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6d23"
      },
      "questionCode": "ILO_COVID_SAFE_21",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6ecd"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f5a"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6c47"
      },
      "questionCode": "ILO_COVID_SAFE_22",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6e79"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f59"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6da7"
      },
      "questionCode": "ILO_COVID_SAFE_23",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6f11"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f58"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6cb5"
      },
      "questionCode": "ILO_COVID_SAFE_24",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6f19"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec289f6329e33023ffd6f57"
      },
      "questionId": {
        "$oid": "5ec25a23329e33023ffd6d91"
      },
      "questionCode": "ILO_COVID_SAFE_25",
      "answerId": {
        "$oid": "5ec25a2b329e33023ffd6ee1"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e780d51a123c5002889adfc"
  },
  "createdDate": {
    "$date": "2020-05-18T13:13:26.613Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ec62842329e33023ffd83c0"
  },
  "groupId": {
    "$oid": "5ec6269e329e33023ffd7cc8"
  },
  "companyId": {
    "$oid": "5e71a7c853d9d402ddccb03a"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83ca"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cea"
      },
      "questionCode": "SAF_L_PCC_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f68"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83c9"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7df2"
      },
      "questionCode": "SAF_L_PCC_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7fc4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83c8"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cd4"
      },
      "questionCode": "SAF_L_PCC_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ff4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83c7"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7dc6"
      },
      "questionCode": "SAF_L_PCC_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7fe8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83c6"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d00"
      },
      "questionCode": "SAF_L_PCC_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8044"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83c5"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d84"
      },
      "questionCode": "SAF_L_PCC_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8024"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83c4"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e60"
      },
      "questionCode": "SAF_L_PCC_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f6c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83c3"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d9a"
      },
      "questionCode": "SAF_L_PCC_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd804c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83c2"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d16"
      },
      "questionCode": "SAF_L_PCC_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd803c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec62842329e33023ffd83c1"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7ddc"
      },
      "questionCode": "SAF_L_PCC_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f78"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e71aea649406d00ac89d429"
  },
  "createdDate": {
    "$date": "2020-05-21T07:05:38.796Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ec628a7329e33023ffd83cc"
  },
  "groupId": {
    "$oid": "5ec626c6329e33023ffd7ccc"
  },
  "companyId": {
    "$oid": "5e71a7c853d9d402ddccb03a"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83d6"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e76"
      },
      "questionCode": "SAF_L_LEA_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8028"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83d5"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e1e"
      },
      "questionCode": "SAF_L_LEA_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd811c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83d4"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7db0"
      },
      "questionCode": "SAF_L_LEA_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8170"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83d3"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d2c"
      },
      "questionCode": "SAF_L_LEA_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ffc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83d2"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f10"
      },
      "questionCode": "SAF_L_LEA_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8100"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83d1"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e08"
      },
      "questionCode": "SAF_L_LEA_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd814c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83d0"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e8c"
      },
      "questionCode": "SAF_L_LEA_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8084"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83cf"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ece"
      },
      "questionCode": "SAF_L_LEA_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80e4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83ce"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d42"
      },
      "questionCode": "SAF_L_LEA_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8164"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628a7329e33023ffd83cd"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f26"
      },
      "questionCode": "SAF_L_LEA_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8104"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e71aea649406d00ac89d429"
  },
  "createdDate": {
    "$date": "2020-05-21T07:07:19.021Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ec628ea329e33023ffd83d8"
  },
  "groupId": {
    "$oid": "5ec626ee329e33023ffd7cd0"
  },
  "companyId": {
    "$oid": "5e71a7c853d9d402ddccb03a"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83e2"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ea2"
      },
      "questionCode": "SAF_L_WRT_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80a8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83e1"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e4a"
      },
      "questionCode": "SAF_L_WRT_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8080"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83e0"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ee4"
      },
      "questionCode": "SAF_L_WRT_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80e8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83df"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d58"
      },
      "questionCode": "SAF_L_WRT_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8178"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83de"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f3c"
      },
      "questionCode": "SAF_L_WRT_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8094"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ec627d1329e33023ffd83ab"
        },
        {
          "$oid": "5ec627d1329e33023ffd835f"
        }
      ],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83dd"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e34"
      },
      "questionCode": "SAF_L_WRT_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8108"
      },
      "subQuestionId": {
        "$oid": "5ec627c7329e33023ffd832f"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83dc"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7eb8"
      },
      "questionCode": "SAF_L_WRT_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd812c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83db"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7efa"
      },
      "questionCode": "SAF_L_WRT_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8058"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83da"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d6e"
      },
      "questionCode": "SAF_L_WRT_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80ec"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ec628ea329e33023ffd83d9"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f52"
      },
      "questionCode": "SAF_L_WRT_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8160"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e71aea649406d00ac89d429"
  },
  "createdDate": {
    "$date": "2020-05-21T07:08:26.298Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5eccc9f2329e33023ffd89e0"
  },
  "groupId": {
    "$oid": "5eccc1d8329e33023ffd852f"
  },
  "companyId": {
    "$oid": "5e71a43453d9d402ddccb02c"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89ee"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8562"
      },
      "questionCode": "LSI10.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89ed"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8654"
      },
      "questionCode": "LSI10.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8676"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89ec"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85d0"
      },
      "questionCode": "LSI10.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8706"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89eb"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8612"
      },
      "questionCode": "LSI10.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8722"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89ea"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8536"
      },
      "questionCode": "LSI9.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8686"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89e9"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8628"
      },
      "questionCode": "LSI9.1.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86c2"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89e8"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85e6"
      },
      "questionCode": "LSI9.1.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8726"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89e7"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85a4"
      },
      "questionCode": "LSI9.1.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd869a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89e6"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8578"
      },
      "questionCode": "LSI9.1.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86ae"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89e5"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd854c"
      },
      "questionCode": "LSI9.1.5",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd866a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89e4"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd863e"
      },
      "questionCode": "LSI9.1.6",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8696"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89e3"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85ba"
      },
      "questionCode": "LSI9.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd871e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89e2"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85fc"
      },
      "questionCode": "LSI9.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86e2"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5eccc9f2329e33023ffd89e1"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd858e"
      },
      "questionCode": "LSI9.4",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873e"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e780d51a123c5002889adfc"
  },
  "createdDate": {
    "$date": "2020-05-26T07:49:06.197Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ecf65fe329e33023ffda40b"
  },
  "groupId": {
    "$oid": "5ecf5ca6329e33023ffd98e2"
  },
  "companyId": {
    "$oid": "5e71a43453d9d402ddccb02c"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf65fe329e33023ffda413"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd83ff"
      },
      "questionCode": "LSI8.1",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf65fe329e33023ffda412"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8499"
      },
      "questionCode": "LSI8.2",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84df"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf65fe329e33023ffda411"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8483"
      },
      "questionCode": "LSI8.3",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf65fe329e33023ffda410"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8457"
      },
      "questionCode": "LSI8.4",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf65fe329e33023ffda40f"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd842b"
      },
      "questionCode": "LSI8.5",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd852b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf65fe329e33023ffda40e"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8415"
      },
      "questionCode": "LSI8.6",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf65fe329e33023ffda40d"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8441"
      },
      "questionCode": "LSI8.7",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd851f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf65fe329e33023ffda40c"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd846d"
      },
      "questionCode": "LSI8.8",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ff"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e780d51a123c5002889adfc"
  },
  "createdDate": {
    "$date": "2020-05-28T07:19:26.961Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ecf6653329e33023ffda414"
  },
  "groupId": {
    "$oid": "5ec33833329e33023ffd6f85"
  },
  "companyId": {
    "$oid": "5e71a43453d9d402ddccb02c"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda423"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7210"
      },
      "questionCode": "LSI5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7313"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda422"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71b8"
      },
      "questionCode": "LSI5.10",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7307"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda421"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd727e"
      },
      "questionCode": "LSI5.11",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd74a4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda420"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70f2"
      },
      "questionCode": "LSI5.12",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72f7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda41f"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd723c"
      },
      "questionCode": "LSI5.13",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd745c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda41e"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd714a"
      },
      "questionCode": "LSI5.14",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd749c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda41d"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd711e"
      },
      "questionCode": "LSI5.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73c3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda41c"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71a2"
      },
      "questionCode": "LSI5.4",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda41b"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7268"
      },
      "questionCode": "LSI5.5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7337"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda41a"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70dc"
      },
      "questionCode": "LSI5.6",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7458"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda419"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7226"
      },
      "questionCode": "LSI5.7",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7498"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda418"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7134"
      },
      "questionCode": "LSI5.9",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73f6"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda417"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71ce"
      },
      "questionCode": "LSI6.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7494"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda416"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7294"
      },
      "questionCode": "LSI6.1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73b7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ecf6653329e33023ffda415"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7108"
      },
      "questionCode": "LSI6.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7373"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e780d51a123c5002889adfc"
  },
  "createdDate": {
    "$date": "2020-05-28T07:20:51.361Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ed0ca7131c54f03bbb9b07a"
  },
  "groupId": {
    "$oid": "5ecf6de7329e33023ffda42c"
  },
  "companyId": {
    "$oid": "5e71a7c853d9d402ddccb03a"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b08b"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9ad97"
      },
      "questionCode": "BWV_W_M_Csr_13",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9afb5"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b08a"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9adc3"
      },
      "questionCode": "BWV_W_M_Csr_14",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9af9d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b089"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9aecb"
      },
      "questionCode": "BWV_W_M_Csr_15",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9afa9"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b088"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9add9"
      },
      "questionCode": "BWV_W_M_Csr_16",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9af71"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b087"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9ae05"
      },
      "questionCode": "BWV_W_M_Csr_17",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9afc1"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b086"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9adad"
      },
      "questionCode": "BWV_W_M_Csr_18",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9afb9"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b085"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9adef"
      },
      "questionCode": "BWV_W_M_Csr_19",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9afe9"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b084"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9aee1"
      },
      "questionCode": "BWV_W_M_Csr_20",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9b051"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b083"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9ae5d"
      },
      "questionCode": "BWV_W_M_Csr_21",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9afad"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b082"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9ae1b"
      },
      "questionCode": "BWV_W_M_Csr_22",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9af29"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b081"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9ae31"
      },
      "questionCode": "BWV_W_M_Csr_23",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9b039"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b080"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9ae89"
      },
      "questionCode": "BWV_W_M_Csr_24",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9afa5"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b07f"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9aef7"
      },
      "questionCode": "BWV_W_M_Csr_25",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9afd1"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b07e"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9ae73"
      },
      "questionCode": "BWV_W_M_Csr_26",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9afb1"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b07d"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9aeb5"
      },
      "questionCode": "BWV_W_M_Csr_27",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9b005"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b07c"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9ae47"
      },
      "questionCode": "BWV_W_M_Csr_28",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9af1d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ed0ca7131c54f03bbb9b07b"
      },
      "questionId": {
        "$oid": "5ed0c9c131c54f03bbb9ae9f"
      },
      "questionCode": "BWV_W_M_Csr_29",
      "answerId": {
        "$oid": "5ed0c9c931c54f03bbb9af61"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e71aea649406d00ac89d429"
  },
  "createdDate": {
    "$date": "2020-05-29T08:40:17.094Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5ef59436862b760278cef589"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef59a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef599"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef598"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef597"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef596"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef595"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef594"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef593"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef592"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef591"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88ef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef590"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef58f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef58e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef58d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88f7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef58c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef58b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5ef59436862b760278cef58a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8903"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e75a52a5116f600bd7426ab"
  },
  "createdDate": {
    "$date": "2020-06-26T06:22:46.502Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5efd5aec30d0d4002cdce901"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5ec623dda6cf59022178548d"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce912"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce911"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce910"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce90f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce90e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce90d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce90c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce90b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce90a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce909"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce908"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8983"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce907"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8957"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce906"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce905"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce904"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce903"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5aec30d0d4002cdce902"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ed892222b41170035631e75"
  },
  "createdDate": {
    "$date": "2020-07-02T03:56:28.753Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5efd5b3d30d0d4002cdce913"
  },
  "groupId": {
    "$oid": "5ecf5ca6329e33023ffd98e2"
  },
  "companyId": {
    "$oid": "5ec623dda6cf59022178548d"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5b3d30d0d4002cdce91b"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd83ff"
      },
      "questionCode": "LSI8.1",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5b3d30d0d4002cdce91a"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8499"
      },
      "questionCode": "LSI8.2",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84df"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5b3d30d0d4002cdce919"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8483"
      },
      "questionCode": "LSI8.3",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5b3d30d0d4002cdce918"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8457"
      },
      "questionCode": "LSI8.4",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5b3d30d0d4002cdce917"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd842b"
      },
      "questionCode": "LSI8.5",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd852b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5b3d30d0d4002cdce916"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8415"
      },
      "questionCode": "LSI8.6",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5b3d30d0d4002cdce915"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8441"
      },
      "questionCode": "LSI8.7",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd851f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5b3d30d0d4002cdce914"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd846d"
      },
      "questionCode": "LSI8.8",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ff"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ed892222b41170035631e75"
  },
  "createdDate": {
    "$date": "2020-07-02T03:57:49.062Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5efd5c4a30d0d4002cdce91c"
  },
  "groupId": {
    "$oid": "5ece156f329e33023ffd89f6"
  },
  "companyId": {
    "$oid": "5ec623dda6cf59022178548d"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce927"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a1e"
      },
      "questionCode": "BWV_W_V_Ind_1",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b09"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece159e329e33023ffd8bf9"
        }
      ],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce926"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd89fd"
      },
      "questionCode": "BWV_W_V_Ind_10",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b4d"
      },
      "subQuestionId": {
        "$oid": "5ece1596329e33023ffd8bc5"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce925"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a67"
      },
      "questionCode": "BWV_W_V_Ind_11",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b01"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce924"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a60"
      },
      "questionCode": "BWV_W_V_Ind_2",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b89"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce923"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a4a"
      },
      "questionCode": "BWV_W_V_Ind_3",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b55"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce922"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a34"
      },
      "questionCode": "BWV_W_V_Ind_4",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b99"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce921"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a76"
      },
      "questionCode": "BWV_W_V_Ind_5",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b29"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce920"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a8c"
      },
      "questionCode": "BWV_W_V_Ind_6",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b41"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce91f"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a7d"
      },
      "questionCode": "BWV_W_V_Ind_7",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b21"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce91e"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8aa9"
      },
      "questionCode": "BWV_W_V_Ind_8",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b19"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5efd5c4a30d0d4002cdce91d"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a93"
      },
      "questionCode": "BWV_W_V_Ind_9",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b9d"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ed892222b41170035631e75"
  },
  "createdDate": {
    "$date": "2020-07-02T04:02:18.864Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f02a4942c4c06003006ad0a"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad1b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad1a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad19"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad18"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad17"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8927"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad16"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad15"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad14"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad13"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad12"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad11"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8983"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad10"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad0f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad0e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad0d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad0c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4942c4c06003006ad0b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ece158fc2777a0083a0cc99"
  },
  "createdDate": {
    "$date": "2020-07-06T04:12:04.954Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f02a4f52c4c06003006ad1c"
  },
  "groupId": {
    "$oid": "5ecf5ca6329e33023ffd98e2"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4f52c4c06003006ad24"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd83ff"
      },
      "questionCode": "LSI8.1",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4f52c4c06003006ad23"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8499"
      },
      "questionCode": "LSI8.2",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84df"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4f52c4c06003006ad22"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8483"
      },
      "questionCode": "LSI8.3",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4f52c4c06003006ad21"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8457"
      },
      "questionCode": "LSI8.4",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4f52c4c06003006ad20"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd842b"
      },
      "questionCode": "LSI8.5",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd852b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4f52c4c06003006ad1f"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8415"
      },
      "questionCode": "LSI8.6",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4f52c4c06003006ad1e"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8441"
      },
      "questionCode": "LSI8.7",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd851f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f02a4f52c4c06003006ad1d"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd846d"
      },
      "questionCode": "LSI8.8",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ff"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ece158fc2777a0083a0cc99"
  },
  "createdDate": {
    "$date": "2020-07-06T04:13:41.618Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f184306082818003dce693e"
  },
  "groupId": {
    "$oid": "5ecf5ca6329e33023ffd98e2"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184306082818003dce6946"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd83ff"
      },
      "questionCode": "LSI8.1",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184306082818003dce6945"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8499"
      },
      "questionCode": "LSI8.2",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84df"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184306082818003dce6944"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8483"
      },
      "questionCode": "LSI8.3",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184306082818003dce6943"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8457"
      },
      "questionCode": "LSI8.4",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184306082818003dce6942"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd842b"
      },
      "questionCode": "LSI8.5",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd852b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184306082818003dce6941"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8415"
      },
      "questionCode": "LSI8.6",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184306082818003dce6940"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8441"
      },
      "questionCode": "LSI8.7",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd851f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184306082818003dce693f"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd846d"
      },
      "questionCode": "LSI8.8",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ff"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T13:45:42.055Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f184328082818003dce6947"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6958"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6957"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6956"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6955"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6954"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6953"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6952"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6951"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6950"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce694f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce694e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce694d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce694c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce694b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce694a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6949"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184328082818003dce6948"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T13:46:16.308Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f1843c789f0d5003dd31046"
  },
  "groupId": {
    "$oid": "5ecf38e5329e33023ffd9690"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd31053"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9694"
      },
      "questionCode": "BWV_W_V_Csr_1",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97fe"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd31052"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd979c"
      },
      "questionCode": "BWV_W_V_Csr_10",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97be"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd31051"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9770"
      },
      "questionCode": "BWV_W_V_Csr_11",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9856"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd31050"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9786"
      },
      "questionCode": "BWV_W_V_Csr_12",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9846"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd3104f"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd975a"
      },
      "questionCode": "BWV_W_V_Csr_13",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9872"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd3104e"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96c0"
      },
      "questionCode": "BWV_W_V_Csr_2",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97e6"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd3104d"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd972e"
      },
      "questionCode": "BWV_W_V_Csr_3",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9842"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ecf396c329e33023ffd98be"
        },
        {
          "$oid": "5ecf396c329e33023ffd98ca"
        },
        {
          "$oid": "5ecf396c329e33023ffd98b6"
        }
      ],
      "_id": {
        "$oid": "5f1843c789f0d5003dd3104c"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96ec"
      },
      "questionCode": "BWV_W_V_Csr_4",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd98a6"
      },
      "subQuestionId": {
        "$oid": "5ecf390b329e33023ffd98ae"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd3104b"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9718"
      },
      "questionCode": "BWV_W_V_Csr_5",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd981a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd3104a"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96aa"
      },
      "questionCode": "BWV_W_V_Csr_6",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9896"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd31049"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96d6"
      },
      "questionCode": "BWV_W_V_Csr_7",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd983e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd31048"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9744"
      },
      "questionCode": "BWV_W_V_Csr_8",
      "answerId": {
        "$oid": "5ecf3903329e33023ffd97ba"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843c789f0d5003dd31047"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9702"
      },
      "questionCode": "BWV_W_V_Csr_9",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd98aa"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T13:48:55.787Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f1843e689f0d5003dd31054"
  },
  "groupId": {
    "$oid": "5ec335aa329e33023ffd6f77"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843e689f0d5003dd3105c"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6f92"
      },
      "questionCode": "LSI1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd737f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843e689f0d5003dd3105b"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fbe"
      },
      "questionCode": "LSI1.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7363"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843e689f0d5003dd3105a"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7016"
      },
      "questionCode": "LSI2.2.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843e689f0d5003dd31059"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7058"
      },
      "questionCode": "LSI2.2.3",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74d8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843e689f0d5003dd31058"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd702c"
      },
      "questionCode": "LSI2.2.4",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74f4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843e689f0d5003dd31057"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fa8"
      },
      "questionCode": "LSI2.2.5",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74d4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843e689f0d5003dd31056"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7042"
      },
      "questionCode": "LSI2.2.6",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd7500"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1843e689f0d5003dd31055"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fd4"
      },
      "questionCode": "LSI2.2.7",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74cc"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T13:49:26.345Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f18447989f0d5003dd3105d"
  },
  "groupId": {
    "$oid": "5ecf3875329e33023ffd93ee"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18447989f0d5003dd31068"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd94a2"
      },
      "questionCode": "BWV_W_V_Env_1",
      "answerId": {
        "$oid": "5ecf3899329e33023ffd9504"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18447989f0d5003dd31067"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9460"
      },
      "questionCode": "BWV_W_V_Env_10",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9588"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18447989f0d5003dd31066"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd94ce"
      },
      "questionCode": "BWV_W_V_Env_11",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9580"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ecf38ad329e33023ffd95cc"
        }
      ],
      "_id": {
        "$oid": "5f18447989f0d5003dd31065"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd93f2"
      },
      "questionCode": "BWV_W_V_Env_2",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd956c"
      },
      "subQuestionId": {
        "$oid": "5ecf38a5329e33023ffd95c4"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18447989f0d5003dd31064"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd941e"
      },
      "questionCode": "BWV_W_V_Env_3",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9550"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18447989f0d5003dd31063"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9476"
      },
      "questionCode": "BWV_W_V_Env_4",
      "answerId": {
        "$oid": "5ecf3899329e33023ffd94f8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18447989f0d5003dd31062"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd944a"
      },
      "questionCode": "BWV_W_V_Env_5",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9564"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ecf38ad329e33023ffd9664"
        }
      ],
      "_id": {
        "$oid": "5f18447989f0d5003dd31061"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd94b8"
      },
      "questionCode": "BWV_W_V_Env_6",
      "answerId": {
        "$oid": "5ecf3899329e33023ffd94e8"
      },
      "subQuestionId": {
        "$oid": "5ecf38a5329e33023ffd95b8"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18447989f0d5003dd31060"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd948c"
      },
      "questionCode": "BWV_W_V_Env_7",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9554"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18447989f0d5003dd3105f"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9408"
      },
      "questionCode": "BWV_W_V_Env_8",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9598"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18447989f0d5003dd3105e"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9434"
      },
      "questionCode": "BWV_W_V_Env_9",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9574"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T13:51:53.989Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f1844d689f0d5003dd31069"
  },
  "groupId": {
    "$oid": "5ec33602329e33023ffd6f7e"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd31076"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7160"
      },
      "questionCode": "LSI3.1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd737b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd31075"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd706e"
      },
      "questionCode": "LSI3.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7397"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd31074"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd709a"
      },
      "questionCode": "LSI3.2.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7367"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd31073"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71e4"
      },
      "questionCode": "LSI3.2.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd733f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd31072"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fea"
      },
      "questionCode": "LSI3.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7353"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd31071"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7176"
      },
      "questionCode": "LSI3.3.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72aa"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd31070"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7084"
      },
      "questionCode": "LSI4.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd731b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd3106f"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70b0"
      },
      "questionCode": "LSI4.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7484"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd3106e"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71fa"
      },
      "questionCode": "LSI4.2.2",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74f0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd3106d"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7000"
      },
      "questionCode": "LSI4.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd3106c"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd718c"
      },
      "questionCode": "LSI4.4",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7333"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd3106b"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7252"
      },
      "questionCode": "LSI4.5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7438"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1844d689f0d5003dd3106a"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70c6"
      },
      "questionCode": "LSI4.6",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73da"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T13:53:26.167Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f18454489f0d5003dd31077"
  },
  "groupId": {
    "$oid": "5ece156f329e33023ffd89f6"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd31082"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a1e"
      },
      "questionCode": "BWV_W_V_Ind_1",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8aed"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd31081"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd89fd"
      },
      "questionCode": "BWV_W_V_Ind_10",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b35"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd31080"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a67"
      },
      "questionCode": "BWV_W_V_Ind_11",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b45"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd3107f"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a60"
      },
      "questionCode": "BWV_W_V_Ind_2",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b89"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd3107e"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a4a"
      },
      "questionCode": "BWV_W_V_Ind_3",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b55"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd3107d"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a34"
      },
      "questionCode": "BWV_W_V_Ind_4",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b51"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece159e329e33023ffd8bd1"
        }
      ],
      "_id": {
        "$oid": "5f18454489f0d5003dd3107c"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a76"
      },
      "questionCode": "BWV_W_V_Ind_5",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b15"
      },
      "subQuestionId": {
        "$oid": "5ece1596329e33023ffd8bbd"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd3107b"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a8c"
      },
      "questionCode": "BWV_W_V_Ind_6",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b41"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd3107a"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a7d"
      },
      "questionCode": "BWV_W_V_Ind_7",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b21"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd31079"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8aa9"
      },
      "questionCode": "BWV_W_V_Ind_8",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b19"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18454489f0d5003dd31078"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a93"
      },
      "questionCode": "BWV_W_V_Ind_9",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b9d"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T13:55:16.460Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f18459089f0d5003dd31083"
  },
  "groupId": {
    "$oid": "5ecf5d54329e33023ffd9af6"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459189f0d5003dd31091"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8562"
      },
      "questionCode": "LSI10.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459189f0d5003dd31090"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8654"
      },
      "questionCode": "LSI10.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8676"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459189f0d5003dd3108f"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85d0"
      },
      "questionCode": "LSI10.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86be"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459189f0d5003dd3108e"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8612"
      },
      "questionCode": "LSI10.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86da"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459189f0d5003dd3108d"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8536"
      },
      "questionCode": "LSI9.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8686"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459189f0d5003dd3108c"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8628"
      },
      "questionCode": "LSI9.1.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd867e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459189f0d5003dd3108b"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85e6"
      },
      "questionCode": "LSI9.1.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86de"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459189f0d5003dd3108a"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85a4"
      },
      "questionCode": "LSI9.1.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd869a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459189f0d5003dd31089"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8578"
      },
      "questionCode": "LSI9.1.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459089f0d5003dd31088"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd854c"
      },
      "questionCode": "LSI9.1.5",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd866a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459089f0d5003dd31087"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd863e"
      },
      "questionCode": "LSI9.1.6",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8696"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459089f0d5003dd31086"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85ba"
      },
      "questionCode": "LSI9.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd871e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459089f0d5003dd31085"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85fc"
      },
      "questionCode": "LSI9.3",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18459089f0d5003dd31084"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd858e"
      },
      "questionCode": "LSI9.4",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873e"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T13:56:33.005Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f18463389f0d5003dd31092"
  },
  "groupId": {
    "$oid": "5ece18f2329e33023ffd8dd7"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd3109e"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ddb"
      },
      "questionCode": "BWV_W_V_Wrk_1",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f5f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece1922329e33023ffd904f"
        }
      ],
      "_id": {
        "$oid": "5f18463389f0d5003dd3109d"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e49"
      },
      "questionCode": "BWV_W_V_Wrk_10",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f67"
      },
      "subQuestionId": {
        "$oid": "5ece191b329e33023ffd8feb"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd3109c"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ecd"
      },
      "questionCode": "BWV_W_V_Wrk_11",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fab"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd3109b"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e8b"
      },
      "questionCode": "BWV_W_V_Wrk_12",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fd3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd3109a"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e33"
      },
      "questionCode": "BWV_W_V_Wrk_2",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f03"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd31099"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ea1"
      },
      "questionCode": "BWV_W_V_Wrk_3",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8eef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd31098"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e07"
      },
      "questionCode": "BWV_W_V_Wrk_4",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fbf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd31097"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e5f"
      },
      "questionCode": "BWV_W_V_Wrk_5",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fb7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece1922329e33023ffd904b"
        }
      ],
      "_id": {
        "$oid": "5f18463389f0d5003dd31096"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8df1"
      },
      "questionCode": "BWV_W_V_Wrk_6",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f63"
      },
      "subQuestionId": {
        "$oid": "5ece191b329e33023ffd8fe7"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd31095"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e75"
      },
      "questionCode": "BWV_W_V_Wrk_7",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f13"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd31094"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e1d"
      },
      "questionCode": "BWV_W_V_Wrk_8",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f8f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f18463389f0d5003dd31093"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8eb7"
      },
      "questionCode": "BWV_W_V_Wrk_9",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f2b"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T13:59:15.822Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f1846daf72ebe0042346530"
  },
  "groupId": {
    "$oid": "5ece18f2329e33023ffd8dd7"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe004234653c"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ddb"
      },
      "questionCode": "BWV_W_V_Wrk_1",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f5f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece1922329e33023ffd904f"
        }
      ],
      "_id": {
        "$oid": "5f1846daf72ebe004234653b"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e49"
      },
      "questionCode": "BWV_W_V_Wrk_10",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f67"
      },
      "subQuestionId": {
        "$oid": "5ece191b329e33023ffd8feb"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe004234653a"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ecd"
      },
      "questionCode": "BWV_W_V_Wrk_11",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fab"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe0042346539"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e8b"
      },
      "questionCode": "BWV_W_V_Wrk_12",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fd3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe0042346538"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e33"
      },
      "questionCode": "BWV_W_V_Wrk_2",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f03"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe0042346537"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ea1"
      },
      "questionCode": "BWV_W_V_Wrk_3",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8eef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe0042346536"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e07"
      },
      "questionCode": "BWV_W_V_Wrk_4",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fbf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe0042346535"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e5f"
      },
      "questionCode": "BWV_W_V_Wrk_5",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fb7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece1922329e33023ffd904b"
        },
        {
          "$oid": "5ece1922329e33023ffd8ff7"
        }
      ],
      "_id": {
        "$oid": "5f1846daf72ebe0042346534"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8df1"
      },
      "questionCode": "BWV_W_V_Wrk_6",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f63"
      },
      "subQuestionId": {
        "$oid": "5ece191b329e33023ffd8fe7"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe0042346533"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e75"
      },
      "questionCode": "BWV_W_V_Wrk_7",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f13"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe0042346532"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e1d"
      },
      "questionCode": "BWV_W_V_Wrk_8",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f8f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f1846daf72ebe0042346531"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8eb7"
      },
      "questionCode": "BWV_W_V_Wrk_9",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f2b"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T14:02:02.622Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f184760f72ebe004234653d"
  },
  "groupId": {
    "$oid": "5ec33833329e33023ffd6f85"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe004234654c"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7210"
      },
      "questionCode": "LSI5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7313"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe004234654b"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71b8"
      },
      "questionCode": "LSI5.10",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7307"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe004234654a"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd727e"
      },
      "questionCode": "LSI5.11",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd74a4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346549"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70f2"
      },
      "questionCode": "LSI5.12",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72f7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346548"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd723c"
      },
      "questionCode": "LSI5.13",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73ee"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346547"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd714a"
      },
      "questionCode": "LSI5.14",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346546"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd711e"
      },
      "questionCode": "LSI5.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73c3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346545"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71a2"
      },
      "questionCode": "LSI5.4",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7387"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346544"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7268"
      },
      "questionCode": "LSI5.5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd740e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346543"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70dc"
      },
      "questionCode": "LSI5.6",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7458"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346542"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7226"
      },
      "questionCode": "LSI5.7",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7498"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346541"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7134"
      },
      "questionCode": "LSI5.9",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7448"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe0042346540"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71ce"
      },
      "questionCode": "LSI6.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7494"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe004234653f"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7294"
      },
      "questionCode": "LSI6.1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73fa"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184760f72ebe004234653e"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7108"
      },
      "questionCode": "LSI6.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7373"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T14:04:16.063Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f184790f72ebe004234654d"
  },
  "groupId": {
    "$oid": "5ec33833329e33023ffd6f85"
  },
  "companyId": {
    "$oid": "5ec62451a6cf59022178548f"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe004234655c"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7210"
      },
      "questionCode": "LSI5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7313"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe004234655b"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71b8"
      },
      "questionCode": "LSI5.10",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7307"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe004234655a"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd727e"
      },
      "questionCode": "LSI5.11",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd74a4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346559"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70f2"
      },
      "questionCode": "LSI5.12",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72f7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346558"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd723c"
      },
      "questionCode": "LSI5.13",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73ee"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346557"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd714a"
      },
      "questionCode": "LSI5.14",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346556"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd711e"
      },
      "questionCode": "LSI5.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73c3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346555"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71a2"
      },
      "questionCode": "LSI5.4",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7387"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346554"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7268"
      },
      "questionCode": "LSI5.5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd740e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346553"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70dc"
      },
      "questionCode": "LSI5.6",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7458"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346552"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7226"
      },
      "questionCode": "LSI5.7",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7498"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346551"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7134"
      },
      "questionCode": "LSI5.9",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7448"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe0042346550"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71ce"
      },
      "questionCode": "LSI6.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7494"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe004234654f"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7294"
      },
      "questionCode": "LSI6.1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73fa"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f184790f72ebe004234654e"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7108"
      },
      "questionCode": "LSI6.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7373"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecb3e2dc2777a0083a0cc2d"
  },
  "createdDate": {
    "$date": "2020-07-22T14:05:04.093Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f19a008082818003dce6959"
  },
  "groupId": {
    "$oid": "5ec335aa329e33023ffd6f77"
  },
  "companyId": {
    "$oid": "5e71a43453d9d402ddccb02c"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a008082818003dce6961"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6f92"
      },
      "questionCode": "LSI1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd737f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a008082818003dce6960"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fbe"
      },
      "questionCode": "LSI1.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7363"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a008082818003dce695f"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7016"
      },
      "questionCode": "LSI2.2.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a008082818003dce695e"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7058"
      },
      "questionCode": "LSI2.2.3",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74d8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a008082818003dce695d"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd702c"
      },
      "questionCode": "LSI2.2.4",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74f4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a008082818003dce695c"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fa8"
      },
      "questionCode": "LSI2.2.5",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74d4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a008082818003dce695b"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7042"
      },
      "questionCode": "LSI2.2.6",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd7500"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a008082818003dce695a"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fd4"
      },
      "questionCode": "LSI2.2.7",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74cc"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ec1f35a5454ba0073eae1e5"
  },
  "createdDate": {
    "$date": "2020-07-23T14:34:48.321Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f19a05f89f0d5003dd3109f"
  },
  "groupId": {
    "$oid": "5ec33602329e33023ffd6f7e"
  },
  "companyId": {
    "$oid": "5e71a43453d9d402ddccb02c"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310ac"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7160"
      },
      "questionCode": "LSI3.1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd737b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310ab"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd706e"
      },
      "questionCode": "LSI3.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7323"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310aa"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd709a"
      },
      "questionCode": "LSI3.2.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7367"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a9"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71e4"
      },
      "questionCode": "LSI3.2.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd733f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a8"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fea"
      },
      "questionCode": "LSI3.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7353"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a7"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7176"
      },
      "questionCode": "LSI3.3.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72aa"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a6"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7084"
      },
      "questionCode": "LSI4.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd731b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a5"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70b0"
      },
      "questionCode": "LSI4.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7484"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a4"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71fa"
      },
      "questionCode": "LSI4.2.2",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74f0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a3"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7000"
      },
      "questionCode": "LSI4.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a2"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd718c"
      },
      "questionCode": "LSI4.4",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7333"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a1"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7252"
      },
      "questionCode": "LSI4.5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7438"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a05f89f0d5003dd310a0"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70c6"
      },
      "questionCode": "LSI4.6",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73da"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ec1f35a5454ba0073eae1e5"
  },
  "createdDate": {
    "$date": "2020-07-23T14:36:15.689Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f19a090082818003dce6962"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a43453d9d402ddccb02c"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6973"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6972"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6971"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6970"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce696f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce696e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce696d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce696c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce696b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce696a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6969"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6968"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6967"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6966"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6965"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6964"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f19a090082818003dce6963"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ec1f35a5454ba0073eae1e5"
  },
  "createdDate": {
    "$date": "2020-07-23T14:37:04.015Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f55cd33526aae0042058647"
  },
  "groupId": {
    "$oid": "5ecf5d54329e33023ffd9af6"
  },
  "companyId": {
    "$oid": "5ecc8b65a6cf590221785493"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae0042058655"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8562"
      },
      "questionCode": "LSI10.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae0042058654"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8654"
      },
      "questionCode": "LSI10.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8676"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae0042058653"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85d0"
      },
      "questionCode": "LSI10.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86be"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae0042058652"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8612"
      },
      "questionCode": "LSI10.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86da"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae0042058651"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8536"
      },
      "questionCode": "LSI9.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8686"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae0042058650"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8628"
      },
      "questionCode": "LSI9.1.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd867e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae004205864f"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85e6"
      },
      "questionCode": "LSI9.1.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86de"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae004205864e"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85a4"
      },
      "questionCode": "LSI9.1.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd869a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae004205864d"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8578"
      },
      "questionCode": "LSI9.1.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae004205864c"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd854c"
      },
      "questionCode": "LSI9.1.5",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd866a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae004205864b"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd863e"
      },
      "questionCode": "LSI9.1.6",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd869e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae004205864a"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85ba"
      },
      "questionCode": "LSI9.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd871e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae0042058649"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85fc"
      },
      "questionCode": "LSI9.3",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f55cd33526aae0042058648"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd858e"
      },
      "questionCode": "LSI9.4",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873e"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ecde2aec2777a0083a0cc69"
  },
  "createdDate": {
    "$date": "2020-09-07T06:03:31.447Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f57460419c70a003d158993"
  },
  "groupId": {
    "$oid": "5ec33833329e33023ffd6f85"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d1589a2"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7210"
      },
      "questionCode": "LSI5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7313"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d1589a1"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71b8"
      },
      "questionCode": "LSI5.10",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7307"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d1589a0"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd727e"
      },
      "questionCode": "LSI5.11",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd74a4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d15899f"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70f2"
      },
      "questionCode": "LSI5.12",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72f7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d15899e"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd723c"
      },
      "questionCode": "LSI5.13",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73ee"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d15899d"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd714a"
      },
      "questionCode": "LSI5.14",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d15899c"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd711e"
      },
      "questionCode": "LSI5.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73c3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d15899b"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71a2"
      },
      "questionCode": "LSI5.4",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7387"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d15899a"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7268"
      },
      "questionCode": "LSI5.5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd740e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d158999"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70dc"
      },
      "questionCode": "LSI5.6",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7458"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d158998"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7226"
      },
      "questionCode": "LSI5.7",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7498"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d158997"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7134"
      },
      "questionCode": "LSI5.9",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7448"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d158996"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71ce"
      },
      "questionCode": "LSI6.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7494"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d158995"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7294"
      },
      "questionCode": "LSI6.1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73fa"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f57460419c70a003d158994"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7108"
      },
      "questionCode": "LSI6.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7373"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ea686b05454ba0073eae1de"
  },
  "createdDate": {
    "$date": "2020-09-08T08:51:16.123Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5aee5a19c70a003d1589ab"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a5e353d9d402ddccb032"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589bc"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589bb"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589ba"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b9"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b7"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd896f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b6"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b5"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b2"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b1"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589b0"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589af"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589ae"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589ad"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aee5a19c70a003d1589ac"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e71df2249406d00ac89d430"
  },
  "createdDate": {
    "$date": "2020-09-11T03:26:18.214Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5aeeb719c70a003d1589bd"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a5e353d9d402ddccb032"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589ce"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589cd"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589cc"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589cb"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589ca"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c9"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c7"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c6"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c5"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c2"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c1"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589c0"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589bf"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5aeeb719c70a003d1589be"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e72f9875116f600bd7426a1"
  },
  "createdDate": {
    "$date": "2020-09-11T03:27:51.378Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5af050051e0d003d47134c"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a5e353d9d402ddccb032"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d47135d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d47135c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d47135b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8967"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d47135a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471359"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8947"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471358"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471357"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471356"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471355"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471354"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471353"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471352"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd892b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471351"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d471350"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88f7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d47134f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d47134e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd896b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5af050051e0d003d47134d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e71df7349406d00ac89d432"
  },
  "createdDate": {
    "$date": "2020-09-11T03:34:40.927Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5b0321051e0d003d47135e"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5ee83eed83101402549a5afd"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d47136f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d47136e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d47136d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d47136c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d47136b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d47136a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471369"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471368"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471367"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471366"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471365"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8983"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471364"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471363"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471362"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471361"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d471360"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5b0321051e0d003d47135f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e72f6755116f600bd74269f"
  },
  "createdDate": {
    "$date": "2020-09-11T04:54:57.758Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5edb4819c70a003d1589d3"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589e4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589e3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589e2"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589e1"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd898f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589e0"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589df"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589de"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589dd"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589dc"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589db"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589da"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589d9"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589d8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8973"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589d7"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88f7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589d6"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89ab"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589d5"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd896b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edb4819c70a003d1589d4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8903"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f36141ff99867003d7171c3"
  },
  "createdDate": {
    "$date": "2020-09-14T02:54:00.512Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5edba3051e0d003d471374"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471385"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471384"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471383"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8977"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471382"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471381"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471380"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd896f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d47137f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d47137e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d47137d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89b7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d47137c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d47137b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd891b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d47137a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471379"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471378"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd899f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471377"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471376"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edba3051e0d003d471375"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8937"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f364bf9f99867003d7171c4"
  },
  "createdDate": {
    "$date": "2020-09-14T02:55:31.751Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5edbad051e0d003d471386"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471397"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd891f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471396"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd893f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471395"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8977"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471394"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471393"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8927"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471392"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89cb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471391"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471390"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d47138f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89b7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d47138e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d47138d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8983"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d47138c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8957"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d47138b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8973"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d47138a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471389"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471388"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edbad051e0d003d471387"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e9d5aaa271e900053f1561e"
  },
  "createdDate": {
    "$date": "2020-09-14T02:55:41.007Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5edc54526aae004205866e"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae004205867f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae004205867e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae004205867d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8977"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae004205867c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae004205867b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae004205867a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058679"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058678"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058677"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89b7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058676"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058675"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058674"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058673"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8963"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058672"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd899f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058671"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8923"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae0042058670"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc54526aae004205866f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8937"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5ef0388c1166ab004e6c1ffd"
  },
  "createdDate": {
    "$date": "2020-09-14T02:58:28.466Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5edc7619c70a003d1589e7"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589f8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589f7"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd893f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589f6"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589f5"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589f4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589f3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589f2"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589f1"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589f0"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589ef"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589ee"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589ed"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589ec"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589eb"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd899f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589ea"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8923"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589e9"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edc7619c70a003d1589e8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e8c4b5f9796cb003ec16db3"
  },
  "createdDate": {
    "$date": "2020-09-14T02:59:02.910Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5edd67051e0d003d471399"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713aa"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a9"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8977"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a7"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a6"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a5"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd896f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a2"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a1"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d4713a0"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d47139f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d47139e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d47139d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd899f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d47139c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d47139b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5edd67051e0d003d47139a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e75a52a5116f600bd7426ab"
  },
  "createdDate": {
    "$date": "2020-09-14T03:03:03.848Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5ede58526aae0042058680"
  },
  "groupId": {
    "$oid": "5ecf5d54329e33023ffd9af6"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae004205868e"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8562"
      },
      "questionCode": "LSI10.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86b2"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae004205868d"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8654"
      },
      "questionCode": "LSI10.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86b6"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae004205868c"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85d0"
      },
      "questionCode": "LSI10.3",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd8736"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae004205868b"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8612"
      },
      "questionCode": "LSI10.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86da"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae004205868a"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8536"
      },
      "questionCode": "LSI9.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd868e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae0042058689"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8628"
      },
      "questionCode": "LSI9.1.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd867e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae0042058688"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85e6"
      },
      "questionCode": "LSI9.1.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86de"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae0042058687"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85a4"
      },
      "questionCode": "LSI9.1.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8692"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae0042058686"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8578"
      },
      "questionCode": "LSI9.1.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae0042058685"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd854c"
      },
      "questionCode": "LSI9.1.5",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd866a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae0042058684"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd863e"
      },
      "questionCode": "LSI9.1.6",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8696"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae0042058683"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85ba"
      },
      "questionCode": "LSI9.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd871e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae0042058682"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85fc"
      },
      "questionCode": "LSI9.3",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ede58526aae0042058681"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd858e"
      },
      "questionCode": "LSI9.4",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873e"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f5ed3986632cc003d150ebe"
  },
  "createdDate": {
    "$date": "2020-09-14T03:07:04.107Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5ee05b051e0d003d4713ab"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713bc"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713bb"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd893f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713ba"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8977"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b9"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b7"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd896f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b6"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b5"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b2"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd891b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b1"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713b0"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8963"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713af"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd899f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713ae"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8923"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713ad"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee05b051e0d003d4713ac"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8937"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e943dde271e900053f15617"
  },
  "createdDate": {
    "$date": "2020-09-14T03:15:39.108Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f5ee0a919c70a003d1589f9"
  },
  "groupId": {
    "$oid": "5ecf5d54329e33023ffd9af6"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d158a07"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8562"
      },
      "questionCode": "LSI10.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d158a06"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8654"
      },
      "questionCode": "LSI10.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8676"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d158a05"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85d0"
      },
      "questionCode": "LSI10.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86be"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d158a04"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8612"
      },
      "questionCode": "LSI10.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86da"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d158a03"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8536"
      },
      "questionCode": "LSI9.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8686"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d158a02"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8628"
      },
      "questionCode": "LSI9.1.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86c2"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d158a01"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85e6"
      },
      "questionCode": "LSI9.1.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86de"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d158a00"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85a4"
      },
      "questionCode": "LSI9.1.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd869a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d1589ff"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8578"
      },
      "questionCode": "LSI9.1.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86ae"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d1589fe"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd854c"
      },
      "questionCode": "LSI9.1.5",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd866a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d1589fd"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd863e"
      },
      "questionCode": "LSI9.1.6",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8696"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d1589fc"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85ba"
      },
      "questionCode": "LSI9.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd871e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d1589fb"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85fc"
      },
      "questionCode": "LSI9.3",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f5ee0a919c70a003d1589fa"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd858e"
      },
      "questionCode": "LSI9.4",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873e"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e8c3cc49796cb003ec16db0"
  },
  "createdDate": {
    "$date": "2020-09-14T03:16:57.751Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f684e425a3a36003df2299e"
  },
  "groupId": {
    "$oid": "5ec335aa329e33023ffd6f77"
  },
  "companyId": {
    "$oid": "5ee83eed83101402549a5afd"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f684e425a3a36003df229a6"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6f92"
      },
      "questionCode": "LSI1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd737f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f684e425a3a36003df229a5"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fbe"
      },
      "questionCode": "LSI1.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7363"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f684e425a3a36003df229a4"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7016"
      },
      "questionCode": "LSI2.2.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f684e425a3a36003df229a3"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7058"
      },
      "questionCode": "LSI2.2.3",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74d8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f684e425a3a36003df229a2"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd702c"
      },
      "questionCode": "LSI2.2.4",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74f4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f684e425a3a36003df229a1"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fa8"
      },
      "questionCode": "LSI2.2.5",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74d4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f684e425a3a36003df229a0"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7042"
      },
      "questionCode": "LSI2.2.6",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd7500"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f684e425a3a36003df2299f"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fd4"
      },
      "questionCode": "LSI2.2.7",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74cc"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e72f6755116f600bd74269f"
  },
  "createdDate": {
    "$date": "2020-09-21T06:54:58.082Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6abd9f522d92003dab20ce"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5ee3208db2a6db0265fdab75"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20df"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20de"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20dd"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20dc"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20db"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20da"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d9"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d7"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d6"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d5"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d2"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d1"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20d0"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abd9f522d92003dab20cf"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f10187315dfc0002b5f6b5a"
  },
  "createdDate": {
    "$date": "2020-09-23T03:14:39.212Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6abe395a3a36003df229a7"
  },
  "groupId": {
    "$oid": "5ec33833329e33023ffd6f85"
  },
  "companyId": {
    "$oid": "5ee3208db2a6db0265fdab75"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229b6"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7210"
      },
      "questionCode": "LSI5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7313"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229b5"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71b8"
      },
      "questionCode": "LSI5.10",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7307"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229b4"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd727e"
      },
      "questionCode": "LSI5.11",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd74a4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229b3"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70f2"
      },
      "questionCode": "LSI5.12",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72f7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229b2"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd723c"
      },
      "questionCode": "LSI5.13",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73e6"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229b1"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd714a"
      },
      "questionCode": "LSI5.14",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229b0"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd711e"
      },
      "questionCode": "LSI5.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73c3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229af"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71a2"
      },
      "questionCode": "LSI5.4",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7387"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229ae"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7268"
      },
      "questionCode": "LSI5.5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd740e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229ad"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70dc"
      },
      "questionCode": "LSI5.6",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7458"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229ac"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7226"
      },
      "questionCode": "LSI5.7",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7498"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229ab"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7134"
      },
      "questionCode": "LSI5.9",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7448"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229aa"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71ce"
      },
      "questionCode": "LSI6.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7494"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229a9"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7294"
      },
      "questionCode": "LSI6.1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73fa"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abe395a3a36003df229a8"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7108"
      },
      "questionCode": "LSI6.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7373"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f10187315dfc0002b5f6b5a"
  },
  "createdDate": {
    "$date": "2020-09-23T03:17:13.823Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6abfde522d92003dab20e2"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5ee30a55b2a6db0265fdab3b"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20f3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20f2"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20f1"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20f0"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20ef"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20ee"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20ed"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20ec"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20eb"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20ea"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20e9"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20e8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20e7"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20e6"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20e5"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20e4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfde522d92003dab20e3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f6abb5e7cf736003c656b83"
  },
  "createdDate": {
    "$date": "2020-09-23T03:24:14.418Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6abfabb318750048ca5c16"
  },
  "groupId": {
    "$oid": "5ece156f329e33023ffd89f6"
  },
  "companyId": {
    "$oid": "5ee3208db2a6db0265fdab75"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c21"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a1e"
      },
      "questionCode": "BWV_W_V_Ind_1",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b09"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece159e329e33023ffd8c0d"
        }
      ],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c20"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd89fd"
      },
      "questionCode": "BWV_W_V_Ind_10",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b4d"
      },
      "subQuestionId": {
        "$oid": "5ece1596329e33023ffd8bc5"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c1f"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a67"
      },
      "questionCode": "BWV_W_V_Ind_11",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b45"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c1e"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a60"
      },
      "questionCode": "BWV_W_V_Ind_2",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b69"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c1d"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a4a"
      },
      "questionCode": "BWV_W_V_Ind_3",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b39"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c1c"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a34"
      },
      "questionCode": "BWV_W_V_Ind_4",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b99"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece159e329e33023ffd8bcd"
        }
      ],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c1b"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a76"
      },
      "questionCode": "BWV_W_V_Ind_5",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b15"
      },
      "subQuestionId": {
        "$oid": "5ece1596329e33023ffd8bbd"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c1a"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a8c"
      },
      "questionCode": "BWV_W_V_Ind_6",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b0d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c19"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a7d"
      },
      "questionCode": "BWV_W_V_Ind_7",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b8d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c18"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8aa9"
      },
      "questionCode": "BWV_W_V_Ind_8",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b65"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6abfabb318750048ca5c17"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a93"
      },
      "questionCode": "BWV_W_V_Ind_9",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b95"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f10187315dfc0002b5f6b5a"
  },
  "createdDate": {
    "$date": "2020-09-23T03:23:23.582Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6ac0fa522d92003dab20f4"
  },
  "groupId": {
    "$oid": "5ecf5d54329e33023ffd9af6"
  },
  "companyId": {
    "$oid": "5ee3208db2a6db0265fdab75"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab2102"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8562"
      },
      "questionCode": "LSI10.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab2101"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8654"
      },
      "questionCode": "LSI10.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8676"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab2100"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85d0"
      },
      "questionCode": "LSI10.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86be"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20ff"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8612"
      },
      "questionCode": "LSI10.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86da"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20fe"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8536"
      },
      "questionCode": "LSI9.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8686"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20fd"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8628"
      },
      "questionCode": "LSI9.1.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd867e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20fc"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85e6"
      },
      "questionCode": "LSI9.1.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86de"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20fb"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85a4"
      },
      "questionCode": "LSI9.1.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd869a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20fa"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8578"
      },
      "questionCode": "LSI9.1.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20f9"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd854c"
      },
      "questionCode": "LSI9.1.5",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd866a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20f8"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd863e"
      },
      "questionCode": "LSI9.1.6",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8696"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20f7"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85ba"
      },
      "questionCode": "LSI9.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd871e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20f6"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85fc"
      },
      "questionCode": "LSI9.3",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac0fa522d92003dab20f5"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd858e"
      },
      "questionCode": "LSI9.4",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873e"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f10187315dfc0002b5f6b5a"
  },
  "createdDate": {
    "$date": "2020-09-23T03:28:58.973Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6ac14f522d92003dab2103"
  },
  "groupId": {
    "$oid": "5ecf5ca6329e33023ffd98e2"
  },
  "companyId": {
    "$oid": "5ee3208db2a6db0265fdab75"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac14f522d92003dab210b"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd83ff"
      },
      "questionCode": "LSI8.1",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac14f522d92003dab210a"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8499"
      },
      "questionCode": "LSI8.2",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84df"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac14f522d92003dab2109"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8483"
      },
      "questionCode": "LSI8.3",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac14f522d92003dab2108"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8457"
      },
      "questionCode": "LSI8.4",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac14f522d92003dab2107"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd842b"
      },
      "questionCode": "LSI8.5",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd852b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac14f522d92003dab2106"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8415"
      },
      "questionCode": "LSI8.6",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84f3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac14f522d92003dab2105"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd8441"
      },
      "questionCode": "LSI8.7",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd851f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac14f522d92003dab2104"
      },
      "questionId": {
        "$oid": "5eccc184329e33023ffd846d"
      },
      "questionCode": "LSI8.8",
      "answerId": {
        "$oid": "5eccc18c329e33023ffd84ff"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f10187315dfc0002b5f6b5a"
  },
  "createdDate": {
    "$date": "2020-09-23T03:30:23.586Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6ac156b318750048ca5c22"
  },
  "groupId": {
    "$oid": "5ec33602329e33023ffd6f7e"
  },
  "companyId": {
    "$oid": "5ee3208db2a6db0265fdab75"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c2f"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7160"
      },
      "questionCode": "LSI3.1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd737b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c2e"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd706e"
      },
      "questionCode": "LSI3.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7397"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c2d"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd709a"
      },
      "questionCode": "LSI3.2.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7367"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c2c"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71e4"
      },
      "questionCode": "LSI3.2.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd733f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c2b"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fea"
      },
      "questionCode": "LSI3.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7353"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c2a"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7176"
      },
      "questionCode": "LSI3.3.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72aa"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c29"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7084"
      },
      "questionCode": "LSI4.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd731b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c28"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70b0"
      },
      "questionCode": "LSI4.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7484"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c27"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd71fa"
      },
      "questionCode": "LSI4.2.2",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74f0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c26"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7000"
      },
      "questionCode": "LSI4.3",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c25"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd718c"
      },
      "questionCode": "LSI4.4",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7333"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c24"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7252"
      },
      "questionCode": "LSI4.5",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7438"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ac156b318750048ca5c23"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd70c6"
      },
      "questionCode": "LSI4.6",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd73da"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f10187315dfc0002b5f6b5a"
  },
  "createdDate": {
    "$date": "2020-09-23T03:30:30.939Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6ad567b318750048ca5c30"
  },
  "groupId": {
    "$oid": "5ec335aa329e33023ffd6f77"
  },
  "companyId": {
    "$oid": "5ee3208db2a6db0265fdab75"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ad567b318750048ca5c38"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6f92"
      },
      "questionCode": "LSI1.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd737f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ad567b318750048ca5c37"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fbe"
      },
      "questionCode": "LSI1.2",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd7363"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ad567b318750048ca5c36"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7016"
      },
      "questionCode": "LSI2.2.1",
      "answerId": {
        "$oid": "5ec338e5329e33023ffd72e7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ad567b318750048ca5c35"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7058"
      },
      "questionCode": "LSI2.2.3",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74d8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ad567b318750048ca5c34"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd702c"
      },
      "questionCode": "LSI2.2.4",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74f4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ad567b318750048ca5c33"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fa8"
      },
      "questionCode": "LSI2.2.5",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74e4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ad567b318750048ca5c32"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd7042"
      },
      "questionCode": "LSI2.2.6",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74bc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6ad567b318750048ca5c31"
      },
      "questionId": {
        "$oid": "5ec338d2329e33023ffd6fd4"
      },
      "questionCode": "LSI2.2.7",
      "answerId": {
        "$oid": "5ec33af4329e33023ffd74cc"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f10187315dfc0002b5f6b5a"
  },
  "createdDate": {
    "$date": "2020-09-23T04:56:07.588Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6b309b760a3e0049856684"
  },
  "groupId": {
    "$oid": "5ecf3875329e33023ffd93ee"
  },
  "companyId": {
    "$oid": "5ee2fe7ab2a6db0265fdab1b"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e004985668f"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd94a2"
      },
      "questionCode": "BWV_W_V_Env_1",
      "answerId": {
        "$oid": "5ecf3899329e33023ffd9504"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e004985668e"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9460"
      },
      "questionCode": "BWV_W_V_Env_10",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9588"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e004985668d"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd94ce"
      },
      "questionCode": "BWV_W_V_Env_11",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd95a4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e004985668c"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd93f2"
      },
      "questionCode": "BWV_W_V_Env_2",
      "answerId": {
        "$oid": "5ecf3899329e33023ffd9508"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e004985668b"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd941e"
      },
      "questionCode": "BWV_W_V_Env_3",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9550"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e004985668a"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9476"
      },
      "questionCode": "BWV_W_V_Env_4",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9594"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e0049856689"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd944a"
      },
      "questionCode": "BWV_W_V_Env_5",
      "answerId": {
        "$oid": "5ecf3899329e33023ffd9520"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ecf38ad329e33023ffd9610"
        }
      ],
      "_id": {
        "$oid": "5f6b309b760a3e0049856688"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd94b8"
      },
      "questionCode": "BWV_W_V_Env_6",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9578"
      },
      "subQuestionId": {
        "$oid": "5ecf38a5329e33023ffd95b4"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e0049856687"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd948c"
      },
      "questionCode": "BWV_W_V_Env_7",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9554"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e0049856686"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9408"
      },
      "questionCode": "BWV_W_V_Env_8",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9598"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b309b760a3e0049856685"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9434"
      },
      "questionCode": "BWV_W_V_Env_9",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9574"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f466b868d5548002c3e65e9"
  },
  "createdDate": {
    "$date": "2020-09-23T11:25:15.411Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6b3193821a01004868ae73"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5ee2fe7ab2a6db0265fdab1b"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae84"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae83"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae82"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae81"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae80"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8997"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae7f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae7e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae7d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae7c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae7b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae7a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae79"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae78"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae77"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae76"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae75"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3193821a01004868ae74"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f466b868d5548002c3e65e9"
  },
  "createdDate": {
    "$date": "2020-09-23T11:29:23.394Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6b39f9821a01004868ae85"
  },
  "groupId": {
    "$oid": "5ecf38e5329e33023ffd9690"
  },
  "companyId": {
    "$oid": "5ee2fe7ab2a6db0265fdab1b"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae92"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9694"
      },
      "questionCode": "BWV_W_V_Csr_1",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97fe"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae91"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd979c"
      },
      "questionCode": "BWV_W_V_Csr_10",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97be"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae90"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9770"
      },
      "questionCode": "BWV_W_V_Csr_11",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd98a2"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae8f"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9786"
      },
      "questionCode": "BWV_W_V_Csr_12",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9846"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae8e"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd975a"
      },
      "questionCode": "BWV_W_V_Csr_13",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9872"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae8d"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96c0"
      },
      "questionCode": "BWV_W_V_Csr_2",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97e6"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae8c"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd972e"
      },
      "questionCode": "BWV_W_V_Csr_3",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9842"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ecf396c329e33023ffd98ca"
        }
      ],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae8b"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96ec"
      },
      "questionCode": "BWV_W_V_Csr_4",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd98a6"
      },
      "subQuestionId": {
        "$oid": "5ecf390b329e33023ffd98ae"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae8a"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9718"
      },
      "questionCode": "BWV_W_V_Csr_5",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97ea"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae89"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96aa"
      },
      "questionCode": "BWV_W_V_Csr_6",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97c2"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae88"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96d6"
      },
      "questionCode": "BWV_W_V_Csr_7",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9862"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae87"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9744"
      },
      "questionCode": "BWV_W_V_Csr_8",
      "answerId": {
        "$oid": "5ecf3903329e33023ffd97ba"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b39f9821a01004868ae86"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9702"
      },
      "questionCode": "BWV_W_V_Csr_9",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9886"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f466b868d5548002c3e65e9"
  },
  "createdDate": {
    "$date": "2020-09-23T12:05:13.895Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6b3abb821a01004868ae93"
  },
  "groupId": {
    "$oid": "5ece18f2329e33023ffd8dd7"
  },
  "companyId": {
    "$oid": "5ee2fe7ab2a6db0265fdab1b"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae9f"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ddb"
      },
      "questionCode": "BWV_W_V_Wrk_1",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f5f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae9e"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e49"
      },
      "questionCode": "BWV_W_V_Wrk_10",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fc7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae9d"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ecd"
      },
      "questionCode": "BWV_W_V_Wrk_11",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fab"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae9c"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e8b"
      },
      "questionCode": "BWV_W_V_Wrk_12",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fd3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae9b"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e33"
      },
      "questionCode": "BWV_W_V_Wrk_2",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f03"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae9a"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ea1"
      },
      "questionCode": "BWV_W_V_Wrk_3",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f37"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae99"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e07"
      },
      "questionCode": "BWV_W_V_Wrk_4",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8eeb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae98"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e5f"
      },
      "questionCode": "BWV_W_V_Wrk_5",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f83"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece1922329e33023ffd904b"
        }
      ],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae97"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8df1"
      },
      "questionCode": "BWV_W_V_Wrk_6",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f63"
      },
      "subQuestionId": {
        "$oid": "5ece191b329e33023ffd8fe7"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae96"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e75"
      },
      "questionCode": "BWV_W_V_Wrk_7",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f4b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae95"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e1d"
      },
      "questionCode": "BWV_W_V_Wrk_8",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f8f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3abb821a01004868ae94"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8eb7"
      },
      "questionCode": "BWV_W_V_Wrk_9",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f2b"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f466b868d5548002c3e65e9"
  },
  "createdDate": {
    "$date": "2020-09-23T12:08:27.392Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f6b3b9b821a01004868aea0"
  },
  "groupId": {
    "$oid": "5ece156f329e33023ffd89f6"
  },
  "companyId": {
    "$oid": "5ee2fe7ab2a6db0265fdab1b"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aeab"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a1e"
      },
      "questionCode": "BWV_W_V_Ind_1",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8aed"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aeaa"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd89fd"
      },
      "questionCode": "BWV_W_V_Ind_10",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8bad"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aea9"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a67"
      },
      "questionCode": "BWV_W_V_Ind_11",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b45"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aea8"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a60"
      },
      "questionCode": "BWV_W_V_Ind_2",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b1d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aea7"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a4a"
      },
      "questionCode": "BWV_W_V_Ind_3",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b71"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aea6"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a34"
      },
      "questionCode": "BWV_W_V_Ind_4",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b99"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece159e329e33023ffd8bd9"
        }
      ],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aea5"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a76"
      },
      "questionCode": "BWV_W_V_Ind_5",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b15"
      },
      "subQuestionId": {
        "$oid": "5ece1596329e33023ffd8bbd"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aea4"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a8c"
      },
      "questionCode": "BWV_W_V_Ind_6",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b41"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aea3"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a7d"
      },
      "questionCode": "BWV_W_V_Ind_7",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b8d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aea2"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8aa9"
      },
      "questionCode": "BWV_W_V_Ind_8",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b75"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f6b3b9b821a01004868aea1"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a93"
      },
      "questionCode": "BWV_W_V_Ind_9",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b9d"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f466b868d5548002c3e65e9"
  },
  "createdDate": {
    "$date": "2020-09-23T12:12:11.703Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f755316349690003d75b45c"
  },
  "groupId": {
    "$oid": "5ec626ee329e33023ffd7cd0"
  },
  "companyId": {
    "$oid": "5ef1cdf57ac70e0246c0be55"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755316349690003d75b466"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ea2"
      },
      "questionCode": "SAF_L_WRT_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80a8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755316349690003d75b465"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e4a"
      },
      "questionCode": "SAF_L_WRT_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80d8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755316349690003d75b464"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ee4"
      },
      "questionCode": "SAF_L_WRT_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80dc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755316349690003d75b463"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d58"
      },
      "questionCode": "SAF_L_WRT_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8178"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755316349690003d75b462"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f3c"
      },
      "questionCode": "SAF_L_WRT_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8094"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ec627d1329e33023ffd83ab"
        }
      ],
      "_id": {
        "$oid": "5f755316349690003d75b461"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e34"
      },
      "questionCode": "SAF_L_WRT_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8108"
      },
      "subQuestionId": {
        "$oid": "5ec627c7329e33023ffd832f"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755316349690003d75b460"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7eb8"
      },
      "questionCode": "SAF_L_WRT_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd812c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755316349690003d75b45f"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7efa"
      },
      "questionCode": "SAF_L_WRT_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd808c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755316349690003d75b45e"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d6e"
      },
      "questionCode": "SAF_L_WRT_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80ec"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755316349690003d75b45d"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f52"
      },
      "questionCode": "SAF_L_WRT_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd816c"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f6d6093f4033d0048c4f859"
  },
  "createdDate": {
    "$date": "2020-10-01T03:55:02.133Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f755440e8894e003dd48128"
  },
  "groupId": {
    "$oid": "5ec6269e329e33023ffd7cc8"
  },
  "companyId": {
    "$oid": "5ef1cdf57ac70e0246c0be55"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd48132"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cea"
      },
      "questionCode": "SAF_L_PCC_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8014"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd48131"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7df2"
      },
      "questionCode": "SAF_L_PCC_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80b0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd48130"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cd4"
      },
      "questionCode": "SAF_L_PCC_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ff4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd4812f"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7dc6"
      },
      "questionCode": "SAF_L_PCC_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd801c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd4812e"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d00"
      },
      "questionCode": "SAF_L_PCC_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8044"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd4812d"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d84"
      },
      "questionCode": "SAF_L_PCC_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f8c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd4812c"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e60"
      },
      "questionCode": "SAF_L_PCC_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f6c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd4812b"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d9a"
      },
      "questionCode": "SAF_L_PCC_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd804c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd4812a"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d16"
      },
      "questionCode": "SAF_L_PCC_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd803c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755440e8894e003dd48129"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7ddc"
      },
      "questionCode": "SAF_L_PCC_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f78"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f6d6093f4033d0048c4f859"
  },
  "createdDate": {
    "$date": "2020-10-01T04:00:00.383Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f755543e8894e003dd48133"
  },
  "groupId": {
    "$oid": "5ec626c6329e33023ffd7ccc"
  },
  "companyId": {
    "$oid": "5ef1cdf57ac70e0246c0be55"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd4813d"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e76"
      },
      "questionCode": "SAF_L_LEA_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7fb4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd4813c"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e1e"
      },
      "questionCode": "SAF_L_LEA_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8128"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd4813b"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7db0"
      },
      "questionCode": "SAF_L_LEA_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8170"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd4813a"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d2c"
      },
      "questionCode": "SAF_L_LEA_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ffc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd48139"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f10"
      },
      "questionCode": "SAF_L_LEA_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8130"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd48138"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e08"
      },
      "questionCode": "SAF_L_LEA_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd814c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd48137"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e8c"
      },
      "questionCode": "SAF_L_LEA_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8084"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd48136"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ece"
      },
      "questionCode": "SAF_L_LEA_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8140"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd48135"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d42"
      },
      "questionCode": "SAF_L_LEA_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8050"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f755543e8894e003dd48134"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f26"
      },
      "questionCode": "SAF_L_LEA_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8068"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f6d6093f4033d0048c4f859"
  },
  "createdDate": {
    "$date": "2020-10-01T04:04:19.695Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f77c0aee8894e003dd48152"
  },
  "groupId": {
    "$oid": "5ec626ee329e33023ffd7cd0"
  },
  "companyId": {
    "$oid": "5ee2fe7ab2a6db0265fdab1b"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c0aee8894e003dd4815c"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ea2"
      },
      "questionCode": "SAF_L_WRT_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80a8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c0aee8894e003dd4815b"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e4a"
      },
      "questionCode": "SAF_L_WRT_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8080"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c0aee8894e003dd4815a"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ee4"
      },
      "questionCode": "SAF_L_WRT_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80dc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c0aee8894e003dd48159"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d58"
      },
      "questionCode": "SAF_L_WRT_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8178"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c0aee8894e003dd48158"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f3c"
      },
      "questionCode": "SAF_L_WRT_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8094"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ec627d1329e33023ffd83ab"
        }
      ],
      "_id": {
        "$oid": "5f77c0aee8894e003dd48157"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e34"
      },
      "questionCode": "SAF_L_WRT_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8108"
      },
      "subQuestionId": {
        "$oid": "5ec627c7329e33023ffd832f"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c0aee8894e003dd48156"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7eb8"
      },
      "questionCode": "SAF_L_WRT_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd812c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c0aee8894e003dd48155"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7efa"
      },
      "questionCode": "SAF_L_WRT_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd808c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c0aee8894e003dd48154"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d6e"
      },
      "questionCode": "SAF_L_WRT_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80ec"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c0aee8894e003dd48153"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f52"
      },
      "questionCode": "SAF_L_WRT_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8160"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f466b868d5548002c3e65e9"
  },
  "createdDate": {
    "$date": "2020-10-03T00:07:10.441Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f77c1a4349690003d75b46c"
  },
  "groupId": {
    "$oid": "5ec626c6329e33023ffd7ccc"
  },
  "companyId": {
    "$oid": "5ee2fe7ab2a6db0265fdab1b"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b476"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e76"
      },
      "questionCode": "SAF_L_LEA_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7fb4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b475"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e1e"
      },
      "questionCode": "SAF_L_LEA_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8150"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b474"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7db0"
      },
      "questionCode": "SAF_L_LEA_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8170"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b473"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d2c"
      },
      "questionCode": "SAF_L_LEA_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ffc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b472"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f10"
      },
      "questionCode": "SAF_L_LEA_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8130"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b471"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e08"
      },
      "questionCode": "SAF_L_LEA_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd814c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b470"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e8c"
      },
      "questionCode": "SAF_L_LEA_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8084"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b46f"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ece"
      },
      "questionCode": "SAF_L_LEA_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8140"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b46e"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d42"
      },
      "questionCode": "SAF_L_LEA_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80c4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c1a4349690003d75b46d"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f26"
      },
      "questionCode": "SAF_L_LEA_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8068"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f466b868d5548002c3e65e9"
  },
  "createdDate": {
    "$date": "2020-10-03T00:11:16.265Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f77c212e8894e003dd4815d"
  },
  "groupId": {
    "$oid": "5ec6269e329e33023ffd7cc8"
  },
  "companyId": {
    "$oid": "5ee2fe7ab2a6db0265fdab1b"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd48167"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cea"
      },
      "questionCode": "SAF_L_PCC_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8014"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd48166"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7df2"
      },
      "questionCode": "SAF_L_PCC_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80b0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd48165"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cd4"
      },
      "questionCode": "SAF_L_PCC_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ff4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd48164"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7dc6"
      },
      "questionCode": "SAF_L_PCC_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd801c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd48163"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d00"
      },
      "questionCode": "SAF_L_PCC_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8044"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd48162"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d84"
      },
      "questionCode": "SAF_L_PCC_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7fb0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd48161"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e60"
      },
      "questionCode": "SAF_L_PCC_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f6c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd48160"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d9a"
      },
      "questionCode": "SAF_L_PCC_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd804c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd4815f"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d16"
      },
      "questionCode": "SAF_L_PCC_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd803c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f77c212e8894e003dd4815e"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7ddc"
      },
      "questionCode": "SAF_L_PCC_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f78"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f466b868d5548002c3e65e9"
  },
  "createdDate": {
    "$date": "2020-10-03T00:13:06.587Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f82a0fd349690003d75b718"
  },
  "groupId": {
    "$oid": "5ecf5d54329e33023ffd9af6"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b726"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8562"
      },
      "questionCode": "LSI10.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b725"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8654"
      },
      "questionCode": "LSI10.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8676"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b724"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85d0"
      },
      "questionCode": "LSI10.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86be"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b723"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8612"
      },
      "questionCode": "LSI10.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86da"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b722"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8536"
      },
      "questionCode": "LSI9.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8686"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b721"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8628"
      },
      "questionCode": "LSI9.1.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd867e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b720"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85e6"
      },
      "questionCode": "LSI9.1.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86de"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b71f"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85a4"
      },
      "questionCode": "LSI9.1.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd869a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b71e"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8578"
      },
      "questionCode": "LSI9.1.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b71d"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd854c"
      },
      "questionCode": "LSI9.1.5",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd866a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b71c"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd863e"
      },
      "questionCode": "LSI9.1.6",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8696"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b71b"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85ba"
      },
      "questionCode": "LSI9.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd871e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b71a"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85fc"
      },
      "questionCode": "LSI9.3",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a0fd349690003d75b719"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd858e"
      },
      "questionCode": "LSI9.4",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873e"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e87e64cb5d7003d64d67c"
  },
  "createdDate": {
    "$date": "2020-10-11T06:06:53.683Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f82a179349690003d75b727"
  },
  "groupId": {
    "$oid": "5ec6269e329e33023ffd7cc8"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b731"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cea"
      },
      "questionCode": "SAF_L_PCC_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7fdc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b730"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7df2"
      },
      "questionCode": "SAF_L_PCC_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80b0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b72f"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cd4"
      },
      "questionCode": "SAF_L_PCC_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ff4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b72e"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7dc6"
      },
      "questionCode": "SAF_L_PCC_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd801c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b72d"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d00"
      },
      "questionCode": "SAF_L_PCC_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8044"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b72c"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d84"
      },
      "questionCode": "SAF_L_PCC_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f8c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b72b"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e60"
      },
      "questionCode": "SAF_L_PCC_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f6c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b72a"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d9a"
      },
      "questionCode": "SAF_L_PCC_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd804c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b729"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d16"
      },
      "questionCode": "SAF_L_PCC_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd803c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a179349690003d75b728"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7ddc"
      },
      "questionCode": "SAF_L_PCC_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f78"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e87e64cb5d7003d64d67c"
  },
  "createdDate": {
    "$date": "2020-10-11T06:08:57.316Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f82a24e349690003d75b732"
  },
  "groupId": {
    "$oid": "5ece18f2329e33023ffd8dd7"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b73e"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ddb"
      },
      "questionCode": "BWV_W_V_Wrk_1",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f5f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b73d"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e49"
      },
      "questionCode": "BWV_W_V_Wrk_10",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fc7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b73c"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ecd"
      },
      "questionCode": "BWV_W_V_Wrk_11",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fab"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b73b"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e8b"
      },
      "questionCode": "BWV_W_V_Wrk_12",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fd3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b73a"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e33"
      },
      "questionCode": "BWV_W_V_Wrk_2",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f03"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b739"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ea1"
      },
      "questionCode": "BWV_W_V_Wrk_3",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8eef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b738"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e07"
      },
      "questionCode": "BWV_W_V_Wrk_4",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8eeb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b737"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e5f"
      },
      "questionCode": "BWV_W_V_Wrk_5",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f83"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece1922329e33023ffd904b"
        }
      ],
      "_id": {
        "$oid": "5f82a24e349690003d75b736"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8df1"
      },
      "questionCode": "BWV_W_V_Wrk_6",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f63"
      },
      "subQuestionId": {
        "$oid": "5ece191b329e33023ffd8fe7"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b735"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e75"
      },
      "questionCode": "BWV_W_V_Wrk_7",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f13"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b734"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e1d"
      },
      "questionCode": "BWV_W_V_Wrk_8",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f8f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f82a24e349690003d75b733"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8eb7"
      },
      "questionCode": "BWV_W_V_Wrk_9",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f2b"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e87e64cb5d7003d64d67c"
  },
  "createdDate": {
    "$date": "2020-10-11T06:12:30.327Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83bc99e8894e003dd487ee"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487ff"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487fe"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487fd"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487fc"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487fb"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487fa"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f9"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f8"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f7"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f6"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f5"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f4"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f3"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f2"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f1"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487f0"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bc99e8894e003dd487ef"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e87e64cb5d7003d64d67c"
  },
  "createdDate": {
    "$date": "2020-10-12T02:16:57.098Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83bd16e8894e003dd48800"
  },
  "groupId": {
    "$oid": "5ecf5d54329e33023ffd9af6"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd4880e"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8562"
      },
      "questionCode": "LSI10.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd4880d"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8654"
      },
      "questionCode": "LSI10.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8676"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd4880c"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85d0"
      },
      "questionCode": "LSI10.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86be"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd4880b"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8612"
      },
      "questionCode": "LSI10.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86da"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd4880a"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8536"
      },
      "questionCode": "LSI9.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8686"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd48809"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8628"
      },
      "questionCode": "LSI9.1.1",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd867e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd48808"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85e6"
      },
      "questionCode": "LSI9.1.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd86de"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd48807"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85a4"
      },
      "questionCode": "LSI9.1.3",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd869a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd48806"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd8578"
      },
      "questionCode": "LSI9.1.4",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd870a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd48805"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd854c"
      },
      "questionCode": "LSI9.1.5",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd866a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd48804"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd863e"
      },
      "questionCode": "LSI9.1.6",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd8696"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd48803"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85ba"
      },
      "questionCode": "LSI9.2",
      "answerId": {
        "$oid": "5eccc1f4329e33023ffd871e"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd48802"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd85fc"
      },
      "questionCode": "LSI9.3",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bd16e8894e003dd48801"
      },
      "questionId": {
        "$oid": "5eccc1ed329e33023ffd858e"
      },
      "questionCode": "LSI9.4",
      "answerId": {
        "$oid": "5eccc1f5329e33023ffd873e"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e88345fea2c003d76915a"
  },
  "createdDate": {
    "$date": "2020-10-12T02:19:02.208Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83bda6349690003d75b73f"
  },
  "groupId": {
    "$oid": "5ec6269e329e33023ffd7cc8"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b749"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cea"
      },
      "questionCode": "SAF_L_PCC_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8048"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b748"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7df2"
      },
      "questionCode": "SAF_L_PCC_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80b0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b747"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cd4"
      },
      "questionCode": "SAF_L_PCC_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ff4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b746"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7dc6"
      },
      "questionCode": "SAF_L_PCC_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd801c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b745"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d00"
      },
      "questionCode": "SAF_L_PCC_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8044"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b744"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d84"
      },
      "questionCode": "SAF_L_PCC_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f8c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b743"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e60"
      },
      "questionCode": "SAF_L_PCC_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f6c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b742"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d9a"
      },
      "questionCode": "SAF_L_PCC_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd804c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b741"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d16"
      },
      "questionCode": "SAF_L_PCC_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd803c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bda6349690003d75b740"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7ddc"
      },
      "questionCode": "SAF_L_PCC_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f78"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e88345fea2c003d76915a"
  },
  "createdDate": {
    "$date": "2020-10-12T02:21:26.338Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83be39e8894e003dd4880f"
  },
  "groupId": {
    "$oid": "5ec6269e329e33023ffd7cc8"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48819"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cea"
      },
      "questionCode": "SAF_L_PCC_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7fdc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48818"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7df2"
      },
      "questionCode": "SAF_L_PCC_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80b0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48817"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cd4"
      },
      "questionCode": "SAF_L_PCC_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ff4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48816"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7dc6"
      },
      "questionCode": "SAF_L_PCC_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd801c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48815"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d00"
      },
      "questionCode": "SAF_L_PCC_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8044"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48814"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d84"
      },
      "questionCode": "SAF_L_PCC_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f8c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48813"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e60"
      },
      "questionCode": "SAF_L_PCC_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f6c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48812"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d9a"
      },
      "questionCode": "SAF_L_PCC_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd804c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48811"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d16"
      },
      "questionCode": "SAF_L_PCC_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd803c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be39e8894e003dd48810"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7ddc"
      },
      "questionCode": "SAF_L_PCC_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f78"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e88345fea2c003d76915a"
  },
  "createdDate": {
    "$date": "2020-10-12T02:23:53.221Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83be7de8894e003dd4881a"
  },
  "groupId": {
    "$oid": "5ec6269e329e33023ffd7cc8"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd48824"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cea"
      },
      "questionCode": "SAF_L_PCC_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7fdc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd48823"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7df2"
      },
      "questionCode": "SAF_L_PCC_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80b0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd48822"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7cd4"
      },
      "questionCode": "SAF_L_PCC_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ff4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd48821"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7dc6"
      },
      "questionCode": "SAF_L_PCC_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd801c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd48820"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d00"
      },
      "questionCode": "SAF_L_PCC_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8044"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd4881f"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d84"
      },
      "questionCode": "SAF_L_PCC_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f8c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd4881e"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e60"
      },
      "questionCode": "SAF_L_PCC_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f6c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd4881d"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d9a"
      },
      "questionCode": "SAF_L_PCC_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd804c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd4881c"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d16"
      },
      "questionCode": "SAF_L_PCC_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd803c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83be7de8894e003dd4881b"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7ddc"
      },
      "questionCode": "SAF_L_PCC_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7f78"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e88345fea2c003d76915a"
  },
  "createdDate": {
    "$date": "2020-10-12T02:25:01.297Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83bf0fe8894e003dd48825"
  },
  "groupId": {
    "$oid": "5ec626c6329e33023ffd7ccc"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd4882f"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e76"
      },
      "questionCode": "SAF_L_LEA_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80f0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd4882e"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e1e"
      },
      "questionCode": "SAF_L_LEA_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8150"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd4882d"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7db0"
      },
      "questionCode": "SAF_L_LEA_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8170"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd4882c"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d2c"
      },
      "questionCode": "SAF_L_LEA_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ffc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd4882b"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f10"
      },
      "questionCode": "SAF_L_LEA_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8130"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd4882a"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e08"
      },
      "questionCode": "SAF_L_LEA_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd814c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd48829"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e8c"
      },
      "questionCode": "SAF_L_LEA_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8084"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd48828"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ece"
      },
      "questionCode": "SAF_L_LEA_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8140"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd48827"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d42"
      },
      "questionCode": "SAF_L_LEA_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80c4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf0fe8894e003dd48826"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f26"
      },
      "questionCode": "SAF_L_LEA_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8068"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e87e64cb5d7003d64d67c"
  },
  "createdDate": {
    "$date": "2020-10-12T02:27:27.309Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83bf5be8894e003dd48830"
  },
  "groupId": {
    "$oid": "5ec626ee329e33023ffd7cd0"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf5be8894e003dd4883a"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ea2"
      },
      "questionCode": "SAF_L_WRT_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80a8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf5be8894e003dd48839"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e4a"
      },
      "questionCode": "SAF_L_WRT_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8064"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf5be8894e003dd48838"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ee4"
      },
      "questionCode": "SAF_L_WRT_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80dc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf5be8894e003dd48837"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d58"
      },
      "questionCode": "SAF_L_WRT_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8178"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf5be8894e003dd48836"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f3c"
      },
      "questionCode": "SAF_L_WRT_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8094"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ec627d1329e33023ffd83ab"
        }
      ],
      "_id": {
        "$oid": "5f83bf5be8894e003dd48835"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e34"
      },
      "questionCode": "SAF_L_WRT_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8108"
      },
      "subQuestionId": {
        "$oid": "5ec627c7329e33023ffd832f"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf5be8894e003dd48834"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7eb8"
      },
      "questionCode": "SAF_L_WRT_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd812c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf5be8894e003dd48833"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7efa"
      },
      "questionCode": "SAF_L_WRT_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd808c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf5be8894e003dd48832"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d6e"
      },
      "questionCode": "SAF_L_WRT_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80ec"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bf5be8894e003dd48831"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f52"
      },
      "questionCode": "SAF_L_WRT_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8160"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e87e64cb5d7003d64d67c"
  },
  "createdDate": {
    "$date": "2020-10-12T02:28:43.099Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83bfd6e8894e003dd4883b"
  },
  "groupId": {
    "$oid": "5ece18f2329e33023ffd8dd7"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd48847"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ddb"
      },
      "questionCode": "BWV_W_V_Wrk_1",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8eff"
      },
      "subQuestionId": {
        "$oid": "5ece191b329e33023ffd8fdb"
      }
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece1922329e33023ffd904f"
        }
      ],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd48846"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e49"
      },
      "questionCode": "BWV_W_V_Wrk_10",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f67"
      },
      "subQuestionId": {
        "$oid": "5ece191b329e33023ffd8feb"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd48845"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ecd"
      },
      "questionCode": "BWV_W_V_Wrk_11",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fab"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd48844"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e8b"
      },
      "questionCode": "BWV_W_V_Wrk_12",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8fd3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd48843"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e33"
      },
      "questionCode": "BWV_W_V_Wrk_2",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f03"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd48842"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8ea1"
      },
      "questionCode": "BWV_W_V_Wrk_3",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8eef"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd48841"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e07"
      },
      "questionCode": "BWV_W_V_Wrk_4",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8eeb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd48840"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e5f"
      },
      "questionCode": "BWV_W_V_Wrk_5",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f83"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece1922329e33023ffd8ff7"
        }
      ],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd4883f"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8df1"
      },
      "questionCode": "BWV_W_V_Wrk_6",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f63"
      },
      "subQuestionId": {
        "$oid": "5ece191b329e33023ffd8fe7"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd4883e"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e75"
      },
      "questionCode": "BWV_W_V_Wrk_7",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f13"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd4883d"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8e1d"
      },
      "questionCode": "BWV_W_V_Wrk_8",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f8f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83bfd6e8894e003dd4883c"
      },
      "questionId": {
        "$oid": "5ece1904329e33023ffd8eb7"
      },
      "questionCode": "BWV_W_V_Wrk_9",
      "answerId": {
        "$oid": "5ece1913329e33023ffd8f2b"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e88345fea2c003d76915a"
  },
  "createdDate": {
    "$date": "2020-10-12T02:30:46.463Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83c01f349690003d75b74a"
  },
  "groupId": {
    "$oid": "5ecf38e5329e33023ffd9690"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b757"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9694"
      },
      "questionCode": "BWV_W_V_Csr_1",
      "answerId": {
        "$oid": "5ecf3903329e33023ffd97b2"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b756"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd979c"
      },
      "questionCode": "BWV_W_V_Csr_10",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd988a"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b755"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9770"
      },
      "questionCode": "BWV_W_V_Csr_11",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9856"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b754"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9786"
      },
      "questionCode": "BWV_W_V_Csr_12",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9846"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b753"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd975a"
      },
      "questionCode": "BWV_W_V_Csr_13",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9876"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b752"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96c0"
      },
      "questionCode": "BWV_W_V_Csr_2",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97e6"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b751"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd972e"
      },
      "questionCode": "BWV_W_V_Csr_3",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97d2"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ecf396c329e33023ffd98be"
        },
        {
          "$oid": "5ecf396c329e33023ffd98ce"
        },
        {
          "$oid": "5ecf396c329e33023ffd98c2"
        }
      ],
      "_id": {
        "$oid": "5f83c01f349690003d75b750"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96ec"
      },
      "questionCode": "BWV_W_V_Csr_4",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd98a6"
      },
      "subQuestionId": {
        "$oid": "5ecf390b329e33023ffd98ae"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b74f"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9718"
      },
      "questionCode": "BWV_W_V_Csr_5",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97ea"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b74e"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96aa"
      },
      "questionCode": "BWV_W_V_Csr_6",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd97c2"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b74d"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd96d6"
      },
      "questionCode": "BWV_W_V_Csr_7",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9862"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b74c"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9744"
      },
      "questionCode": "BWV_W_V_Csr_8",
      "answerId": {
        "$oid": "5ecf3903329e33023ffd97ba"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83c01f349690003d75b74b"
      },
      "questionId": {
        "$oid": "5ecf38f3329e33023ffd9702"
      },
      "questionCode": "BWV_W_V_Csr_9",
      "answerId": {
        "$oid": "5ecf3904329e33023ffd9892"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e87e64cb5d7003d64d67c"
  },
  "createdDate": {
    "$date": "2020-10-12T02:31:59.764Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83fe31e8894e003dd48848"
  },
  "groupId": {
    "$oid": "5ecf3875329e33023ffd93ee"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [
        {
          "$oid": "5ecf38ad329e33023ffd95dc"
        },
        {
          "$oid": "5ecf38ad329e33023ffd9640"
        },
        {
          "$oid": "5ecf38ad329e33023ffd9618"
        }
      ],
      "_id": {
        "$oid": "5f83fe31e8894e003dd48853"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd94a2"
      },
      "questionCode": "BWV_W_V_Env_1",
      "answerId": {
        "$oid": "5ecf3899329e33023ffd94e4"
      },
      "subQuestionId": {
        "$oid": "5ecf38a5329e33023ffd95ac"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fe31e8894e003dd48852"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9460"
      },
      "questionCode": "BWV_W_V_Env_10",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9588"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fe31e8894e003dd48851"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd94ce"
      },
      "questionCode": "BWV_W_V_Env_11",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd95a4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fe31e8894e003dd48850"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd93f2"
      },
      "questionCode": "BWV_W_V_Env_2",
      "answerId": {
        "$oid": "5ecf3899329e33023ffd9508"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fe31e8894e003dd4884f"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd941e"
      },
      "questionCode": "BWV_W_V_Env_3",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9550"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fe31e8894e003dd4884e"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9476"
      },
      "questionCode": "BWV_W_V_Env_4",
      "answerId": {
        "$oid": "5ecf3899329e33023ffd94f8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fe31e8894e003dd4884d"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd944a"
      },
      "questionCode": "BWV_W_V_Env_5",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9564"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ecf38ad329e33023ffd967c"
        }
      ],
      "_id": {
        "$oid": "5f83fe31e8894e003dd4884c"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd94b8"
      },
      "questionCode": "BWV_W_V_Env_6",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9578"
      },
      "subQuestionId": {
        "$oid": "5ecf38a5329e33023ffd95b4"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fe31e8894e003dd4884b"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd948c"
      },
      "questionCode": "BWV_W_V_Env_7",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9554"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fe31e8894e003dd4884a"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9408"
      },
      "questionCode": "BWV_W_V_Env_8",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd958c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fe31e8894e003dd48849"
      },
      "questionId": {
        "$oid": "5ecf388b329e33023ffd9434"
      },
      "questionCode": "BWV_W_V_Env_9",
      "answerId": {
        "$oid": "5ecf389a329e33023ffd9560"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e87e64cb5d7003d64d67c"
  },
  "createdDate": {
    "$date": "2020-10-12T06:56:49.959Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f83fea2349690003d75b758"
  },
  "groupId": {
    "$oid": "5ece156f329e33023ffd89f6"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b763"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a1e"
      },
      "questionCode": "BWV_W_V_Ind_1",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8aed"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b762"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd89fd"
      },
      "questionCode": "BWV_W_V_Ind_10",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8bad"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b761"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a67"
      },
      "questionCode": "BWV_W_V_Ind_11",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b45"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b760"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a60"
      },
      "questionCode": "BWV_W_V_Ind_2",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b1d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b75f"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a4a"
      },
      "questionCode": "BWV_W_V_Ind_3",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b71"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b75e"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a34"
      },
      "questionCode": "BWV_W_V_Ind_4",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b51"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ece159e329e33023ffd8bcd"
        }
      ],
      "_id": {
        "$oid": "5f83fea2349690003d75b75d"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a76"
      },
      "questionCode": "BWV_W_V_Ind_5",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b15"
      },
      "subQuestionId": {
        "$oid": "5ece1596329e33023ffd8bbd"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b75c"
      },
      "questionId": {
        "$oid": "5ec25425329e33023ffd6a8c"
      },
      "questionCode": "BWV_W_V_Ind_6",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b41"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b75b"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a7d"
      },
      "questionCode": "BWV_W_V_Ind_7",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b8d"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b75a"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8aa9"
      },
      "questionCode": "BWV_W_V_Ind_8",
      "answerId": {
        "$oid": "5ece158d329e33023ffd8b75"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f83fea2349690003d75b759"
      },
      "questionId": {
        "$oid": "5ece1583329e33023ffd8a93"
      },
      "questionCode": "BWV_W_V_Ind_9",
      "answerId": {
        "$oid": "5ece158e329e33023ffd8b9d"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e87e64cb5d7003d64d67c"
  },
  "createdDate": {
    "$date": "2020-10-12T06:58:42.719Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f84f7dee8894e003dd48854"
  },
  "groupId": {
    "$oid": "5ec626ee329e33023ffd7cd0"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd4885e"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ea2"
      },
      "questionCode": "SAF_L_WRT_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80a8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd4885d"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e4a"
      },
      "questionCode": "SAF_L_WRT_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8064"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd4885c"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ee4"
      },
      "questionCode": "SAF_L_WRT_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80dc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd4885b"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d58"
      },
      "questionCode": "SAF_L_WRT_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8178"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd4885a"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f3c"
      },
      "questionCode": "SAF_L_WRT_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8094"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd48859"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e34"
      },
      "questionCode": "SAF_L_WRT_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80d4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd48858"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7eb8"
      },
      "questionCode": "SAF_L_WRT_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8188"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd48857"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7efa"
      },
      "questionCode": "SAF_L_WRT_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8058"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd48856"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d6e"
      },
      "questionCode": "SAF_L_WRT_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80ec"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f7dee8894e003dd48855"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f52"
      },
      "questionCode": "SAF_L_WRT_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8160"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e71a70a49406d00ac89d427"
  },
  "createdDate": {
    "$date": "2020-10-13T00:42:06.240Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f84f848349690003d75b764"
  },
  "groupId": {
    "$oid": "5ec626c6329e33023ffd7ccc"
  },
  "companyId": {
    "$oid": "5e71a56653d9d402ddccb030"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b76e"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e76"
      },
      "questionCode": "SAF_L_LEA_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80f0"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b76d"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e1e"
      },
      "questionCode": "SAF_L_LEA_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8128"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b76c"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7db0"
      },
      "questionCode": "SAF_L_LEA_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8170"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b76b"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d2c"
      },
      "questionCode": "SAF_L_LEA_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd7ffc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b76a"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f10"
      },
      "questionCode": "SAF_L_LEA_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8180"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b769"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e08"
      },
      "questionCode": "SAF_L_LEA_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80b4"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b768"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e8c"
      },
      "questionCode": "SAF_L_LEA_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8084"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b767"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ece"
      },
      "questionCode": "SAF_L_LEA_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8140"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b766"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d42"
      },
      "questionCode": "SAF_L_LEA_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8050"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f84f848349690003d75b765"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f26"
      },
      "questionCode": "SAF_L_LEA_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8068"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5e71a70a49406d00ac89d427"
  },
  "createdDate": {
    "$date": "2020-10-13T00:43:52.657Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f912895e8894e003dd4886b"
  },
  "groupId": {
    "$oid": "5ecf63e7329e33023ffd9fa4"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd4887c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8751"
      },
      "questionCode": "LSI7.1",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89a7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd4887b"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87d5"
      },
      "questionCode": "LSI7.10",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8987"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd4887a"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd877d"
      },
      "questionCode": "LSI7.11",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89bb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48879"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd889b"
      },
      "questionCode": "LSI7.12",
      "answerId": {
        "$oid": "5eccc833329e33023ffd890b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48878"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8817"
      },
      "questionCode": "LSI7.13",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8933"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48877"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8859"
      },
      "questionCode": "LSI7.14.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88eb"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48876"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87eb"
      },
      "questionCode": "LSI7.14.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89c7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48875"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87bf"
      },
      "questionCode": "LSI7.15",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89d3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48874"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd88b1"
      },
      "questionCode": "LSI7.16",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88e3"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48873"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd886f"
      },
      "questionCode": "LSI7.2",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88cf"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48872"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd87a9"
      },
      "questionCode": "LSI7.3",
      "answerId": {
        "$oid": "5eccc833329e33023ffd895f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48871"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8793"
      },
      "questionCode": "LSI7.4",
      "answerId": {
        "$oid": "5eccc833329e33023ffd894b"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd48870"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd882d"
      },
      "questionCode": "LSI7.5",
      "answerId": {
        "$oid": "5eccc833329e33023ffd89af"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd4886f"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8767"
      },
      "questionCode": "LSI7.6",
      "answerId": {
        "$oid": "5eccc833329e33023ffd897f"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd4886e"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8885"
      },
      "questionCode": "LSI7.7",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8943"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd4886d"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8801"
      },
      "questionCode": "LSI7.8",
      "answerId": {
        "$oid": "5eccc833329e33023ffd88d7"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912895e8894e003dd4886c"
      },
      "questionId": {
        "$oid": "5eccc826329e33023ffd8843"
      },
      "questionCode": "LSI7.9",
      "answerId": {
        "$oid": "5eccc833329e33023ffd8913"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e88345fea2c003d76915a"
  },
  "createdDate": {
    "$date": "2020-10-22T06:37:09.100Z"
  },
  "__v": 0
},{
  "_id": {
    "$oid": "5f912901e8894e003dd4887d"
  },
  "groupId": {
    "$oid": "5ec626ee329e33023ffd7cd0"
  },
  "companyId": {
    "$oid": "5ee85b5e83101402549a5b17"
  },
  "feedback": [
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912901e8894e003dd48887"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ea2"
      },
      "questionCode": "SAF_L_WRT_1",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80a8"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912901e8894e003dd48886"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e4a"
      },
      "questionCode": "SAF_L_WRT_10",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8064"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912901e8894e003dd48885"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7ee4"
      },
      "questionCode": "SAF_L_WRT_2",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80dc"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912901e8894e003dd48884"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d58"
      },
      "questionCode": "SAF_L_WRT_3",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8178"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912901e8894e003dd48883"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f3c"
      },
      "questionCode": "SAF_L_WRT_4",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8094"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [
        {
          "$oid": "5ec627d1329e33023ffd83ab"
        }
      ],
      "_id": {
        "$oid": "5f912901e8894e003dd48882"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7e34"
      },
      "questionCode": "SAF_L_WRT_5",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8108"
      },
      "subQuestionId": {
        "$oid": "5ec627c7329e33023ffd832f"
      }
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912901e8894e003dd48881"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7eb8"
      },
      "questionCode": "SAF_L_WRT_6",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd812c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912901e8894e003dd48880"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7efa"
      },
      "questionCode": "SAF_L_WRT_7",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd808c"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912901e8894e003dd4887f"
      },
      "questionId": {
        "$oid": "5ec62700329e33023ffd7d6e"
      },
      "questionCode": "SAF_L_WRT_8",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd80ec"
      },
      "subQuestionId": null
    },
    {
      "subAnswerId": [],
      "_id": {
        "$oid": "5f912901e8894e003dd4887e"
      },
      "questionId": {
        "$oid": "5ec62701329e33023ffd7f52"
      },
      "questionCode": "SAF_L_WRT_9",
      "answerId": {
        "$oid": "5ec6270d329e33023ffd8160"
      },
      "subQuestionId": null
    }
  ],
  "createdBy": {
    "$oid": "5f7e88345fea2c003d76915a"
  },
  "createdDate": {
    "$date": "2020-10-22T06:38:57.662Z"
  },
  "__v": 0
}]
export default DataAnswers
const reportTitle = [
    {id: '1', name:  'Số lượng tương tác', active: false, groupId: '', abb: ''},
    {id: '2', name:  'Bộ câu hỏi được làm nhiều nhất và đúng nhất', active: false, groupId: '', abb: ''},
    {id: '3', name:  'Mức độ hài lòng về chính sách của công ty', active: false, groupId: '', abb: ''},
    {id: '4', name:  'Hợp đồng lao động', active: false, groupId: '5ec335aa329e33023ffd6f77', abb: 'Hợp đồng lao động'},
    {id: '5', name:  'Mức và thời hạn trả lương', active: false, groupId: '', abb: ''},
    {id: '6', name:  'Lương làm thêm', active: false, groupId: '', abb: ''},
    {id: '7', name:  'Kỷ luật lao động', active: false, groupId: '5ec33833329e33023ffd6f85', abb: 'Xử lý kỷ luật'},
    {id: '8', name:  'Bảo hiểm xã hội, y tế, tai nạn lao động, bệnh nghề nghiệp', active: false, groupId: '5ecf5d54329e33023ffd9af6', abb: 'Bảo hiểm, trợ cấp'},
    {id: '9', name:  'Nghỉ, trợ cấp thai sản và bảo đảm công việc', active: false, groupId: '5ecf5d54329e33023ffd9af6', abb: 'Bảo hiểm, trợ cấp'},
    {id: '10', name:  'An toàn vệ sinh lao động', active: false, groupId: '5ecf63e7329e33023ffd9fa4', abb: 'An toàn vệ sinh lao động'},
    {id: '11', name:  'Phòng cháy chữa cháy', active: false, groupId: '5ecf5ca6329e33023ffd98e2', abb: 'Phòng cháy chữa cháy'},
    {id: '12', name:  'Thời giờ làm việc', active: false, groupId: '', abb: ''},
    {id: '13', name:  'Thời giờ nghỉ ngơi', active: false, groupId: '', abb: ''},
];
export default reportTitle;
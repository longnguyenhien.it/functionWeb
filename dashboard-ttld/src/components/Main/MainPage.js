import React, {useEffect, useState} from 'react';
import _ from 'lodash';
import reportTitle from './Datas/ReportTitle';
import AreaField from './filterField/AreaField';
import CompanyField from './filterField/CompanyField';
import IndustriesField from './filterField/IndustriesField';
import SizeField from './filterField/SizeField';
import StartTimeField from './filterField/StartTimeField';
import EndTimeField from './filterField/EndTimeField';
import './MainPage.css';
import Interaction from './chartField/Interaction';
import TypeOfBusinessField from './filterField/TypeOfBusinessField';
import DataAnswers from './Datas/BTWDataAnswer';
import Contract from './chartField/Contract';
import ATVSLD from './chartField/ATVSLD';
import FemaleLaborWelfare from './chartField/FemaleLaborWelfare';
import BHXH from './chartField/BHXH';
import LaborDiscipline from './chartField/LaborDiscipline';
import PCCC from './chartField/PCCC';
import HighSetOfQuestion from './chartField/HighSetOfQuestions';


function MainPage(props) {

const [reportTitleList, setReportTiltleList] = useState(reportTitle);
const [checkDate, setCheckDate] = useState(false);
const [areaValue, setAreaValue] = useState('');
const [indusValue, setIndusValue] = useState('');
const [sizeValue, setSizeValue] = useState('');
const [typeValue, setTypeValue] = useState('');
const [dataComFiltered, setDataComFiltered] = useState();
const [startDate, setStartDate] = useState('');
const [endDate, setEndDate] = useState('');
const [dataAllFiltered, setDataAllFiltered] = useState();


//ActiveHandler
const onClickTitle = (id) => {
    let reportTitleArray = [...reportTitleList];
    setReportTiltleList(
        reportTitleArray.map((title) =>
        title.id === id ? { ...title, active: !title.active } : title
      )
    );
};
// useEffect(()=>{
//     console.log(dataComFiltered);
// },[dataComFiltered])
//Make Data for Chart
const makeDataForChart = () => {
    let sd = new Date(startDate).getTime();
    let ed = new Date(endDate).getTime();
    if (sd > ed) {
        //return <alert>Ngày bắt đầu phải nhỏ hơn ngày kết thúc</alert>
        alert('Ngày bắt đầu phải nhỏ hơn ngày kết thúc');
        setCheckDate(false)
    }
    if (sd <= ed || startDate === '' || endDate === '') {
        console.log(dataComFiltered);
        //Fillter answer theo company
        let filAnsByCom = []
        DataAnswers.map(ans => {
            dataComFiltered.map(data => {
                if (data.companyId.$oid === ans.companyId.$oid) {
                    filAnsByCom.push(ans);
                }
            })
        })
        console.log(filAnsByCom);
        //Filter answer theo date
        let result = filAnsByCom.filter(d => {
            let time = new Date(d.createdDate.$date).getTime();
            if (startDate === '' && endDate === '') {
                return true
            }
            if (startDate !== '' && endDate !== '') {
                return (sd < time && time < ed);
            }
            if (startDate !== '' && endDate === '') {
                let endday = new Date().getTime();
                return (sd < time && time < endday);
            }
            if (startDate === '' && endDate !== '') {
                return (time < ed);
            }
            return false
        });
        setCheckDate(true)
        setDataAllFiltered([...result])
    }
}

/*chart control*/
const chartControl = (titleKey,titleid) => {
    switch (titleKey) {
        case 'Số lượng tương tác':
            if (startDate === '' && endDate === '') {
                alert('Chọn Ngày bắt đầu và Ngày kết thúc');
                onClickTitle(titleid);
            }
            if (startDate === '' && endDate !== '') {
                alert('Chọn Ngày bắt đầu');
                onClickTitle(titleid);
            }
            if (startDate !== '' && endDate === '') {
                alert('Chọn Ngày kết thúc');
                onClickTitle(titleid);
            }
            if ((new Date(endDate).getTime()) - (new Date(startDate).getTime()) > 604800000) {
                alert('Chỉ chọn khoảng cách 1 tuần đối với số lượng tương tác');
                onClickTitle(titleid);
            };
            if ((new Date(endDate).getTime()) - (new Date(startDate).getTime()) <= 604800000) {
                return <Interaction  dataAllFiltered={dataAllFiltered} startDate={startDate} endDate={endDate}/>
            }
            break;
        case 'Bộ câu hỏi được làm nhiều nhất và đúng nhất':
            return <HighSetOfQuestion dataAllFiltered={dataAllFiltered}/>
        case 'Mức độ hài lòng về chính sách của công ty':
            return null
        case 'Hợp đồng lao động':
            return <Contract  dataAllFiltered={dataAllFiltered} />
        case 'Mức và thời hạn trả lương':
            return null
        case 'Lương làm thêm':
            return null
        case 'Kỷ luật lao động':
            return <LaborDiscipline dataAllFiltered={dataAllFiltered}/>
        case 'Bảo hiểm xã hội, y tế, tai nạn lao động, bệnh nghề nghiệp':
            return <BHXH dataAllFiltered={dataAllFiltered}/>
        case 'Nghỉ, trợ cấp thai sản và bảo đảm công việc':
            return <FemaleLaborWelfare dataAllFiltered={dataAllFiltered}/>
        case 'An toàn vệ sinh lao động':
            return <ATVSLD dataAllFiltered={dataAllFiltered}/>
        case 'Phòng cháy chữa cháy':
            return <PCCC dataAllFiltered={dataAllFiltered}/>
        case 'Thời giờ làm việc':
            return null
        case 'Thời giờ nghỉ ngơi':
            return null
        default:
            return null
    }
}

    return (
        <>
            {/*fillter field */}
            <div className="row filterField1" >
                <AreaField setAreaValue={setAreaValue}/>
                <div style={{width:'38px'}}></div>
                <IndustriesField setIndusValue={setIndusValue}/>
                <div style={{width:'38px'}}></div>
                <SizeField setSizeValue={setSizeValue}/>
                <div style={{width:'38px'}}></div>
                <TypeOfBusinessField setTypeValue={setTypeValue}/>
            </div>
            <div className="row filterField2" >
                <CompanyField 
                    areaValue={areaValue}
                    indusValue={indusValue}
                    sizeValue={sizeValue}
                    typeValue={typeValue}
                    setDataComFiltered = {setDataComFiltered}
                />
                <div style={{width:'30px'}}></div>
                <StartTimeField timeName='Ngày bắt đầu' setStartDate={setStartDate}/>
                <div style={{width:'30px'}}></div>
                <EndTimeField timeName='Ngày kết thúc' setEndDate={setEndDate}/>
            </div>
            {/*report title field  */}
            <div className="titlefield">
                <div className="row" style={{marginBottom:'30px'}}>
                    {
                        reportTitleList.map(title => 
                            <div className={
                                title.active ? "reportTitleActive" : "reportTitle"
                                }
                                key={title.id}
                            >
                                <div className="reportTitleContent" 
                                      
                                     onClick={()=>{
                                         onClickTitle(title.id);
                                         if (title.active === false) {
                                            makeDataForChart()
                                         }
                                     }}>
                                    {title.name}
                                </div>
                            </div>
                        )
                    }
                </div> 
            </div>
            {/*chart field */}
            <div className='chartfield'>
                <div className='row'>
                    {
                        reportTitleList.map(title => 
                            title.active && checkDate
                            ? 
                            <div className='chartColumn' key={title.id}>
                                {chartControl(title.name, title.id)}
                            </div>
                            :
                            null
                        )
                    }
                </div>
            </div>
        </>
    );
}

export default MainPage;
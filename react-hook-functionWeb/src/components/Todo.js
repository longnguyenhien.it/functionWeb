import React, { useEffect, useState } from "react";
import Form from "./todoComponents/Form";
import List from "./todoComponents/List";
import ListComplete from "./todoComponents/ListComplete";
import Button from "./todoComponents/Button";
import ListActive from "./todoComponents/ListActive";

const Storage_Task_Key = "task-key";

function Todo(props) {
  const [taskList, setTaskList] = useState([]);
  const [allList, setAllList] = useState(true);
  const [completeList, setCompleteList] = useState(false);
  const [activeList, setActiveList] = useState(false);

  useEffect(() => {
    const storageTaskList = JSON.parse(localStorage.getItem(Storage_Task_Key));
    if (storageTaskList) {
      setTaskList(storageTaskList);
    }
  }, []);

  /*     useEffect(()=>{
        const storageTaskList = JSON.parse(localStorage.getItem(Storage_Task_Key));
        if(storageTaskList){ 
            setAllList(storageTaskList);
            setCompleteList(storageTaskList);
            setActiveList(storageTaskList);
        }
        let newCompleteList = [...completeList];
        let newActiveList = [...activeList];
        setCompleteList(newCompleteList.filter(x => x.complete === true));
        setActiveList(newActiveList.filter(x => x.complete === false));    
    },[]) */

  useEffect(() => {
    localStorage.setItem(Storage_Task_Key, JSON.stringify(taskList));
  }, [taskList]);

  const listAppear = () => {
    if (allList) {
      return <List taskList={taskList} setTaskList={setTaskList} />;
    }
    if (completeList) {
      return <ListComplete taskList={taskList} setTaskList={setTaskList} />;
    }
    if (activeList) {
      return <ListActive taskList={taskList} setTaskList={setTaskList} />;
    }
  };

  return (
    <div style={{ textAlign: "center" }}>
        <br />
      <h1 className="todo-title">Todo App</h1>
      <br />
      <br />
      <br />
      <div>
        <Form taskList={taskList} setTaskList={setTaskList} />
        <br />
        <br />
        <br />
        <div style={{ marginLeft: "40%" }}>{listAppear()}</div>
        <br />
        <br />
          <Button
            allList={allList}
            completeList={completeList}
            activeList={activeList}
            setAllList={setAllList}
            setCompleteList={setCompleteList}
            setActiveList={setActiveList}
          />
      </div>
    </div>
  );
}

export default Todo;

import React, { useState } from "react";
import Welcome from "./Welcome";
import Todo from "./Todo";
import Drag_Drop from "./Drag&Drop";
import Trello from "./Trello";
import ChartDnD from "./ChartDnD";
//import Chart from "./Chart";

const Home = (props) => {
    const {handleLogout} = props;
    const [keyApp, setKeyApp] = useState(''); 

    const navClick = (e) => {
        switch (keyApp) {
            case 'welcome':
                return <Welcome />
            case 'todo':
                return <Todo />
            case 'drag&drop':
                return <Drag_Drop />
            case 'chart':
                return <ChartDnD/>
            case 'trello':
                return <Trello/>
            default:
                return <Welcome />
        }
    }

    return(
        <div className="hero">
            <nav className="navbutton">
                <a href="#"><h2 onClick={() => {setKeyApp('welcome')}}>Welcome</h2></a>
                <a href="#"><h2 onClick={() => {setKeyApp('todo')}}>Todo-App</h2></a>
                <a href="#"><h2 onClick={() => {setKeyApp('drag&drop')}}>Drag & Drop</h2></a>
                <a href="#"><h2 onClick={() => {setKeyApp('chart')}}>Chart</h2></a>
                <a href="#"><h2 onClick={() => {setKeyApp('trello')}}>Trello</h2></a>
                <a href="#"><h2>...</h2></a>
                <button onClick={handleLogout}>Logout</button>
            </nav>
            <div>
                {navClick()}
            </div>
        </div>
    )
}

export default Home;
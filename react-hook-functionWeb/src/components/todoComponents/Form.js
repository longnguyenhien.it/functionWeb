import React, {useState} from 'react';
import { v4 as uuidv4 } from "uuid";

function Form(props) {
    const {taskList, setTaskList} = props;

    const [value, setValue] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        const formValues = {
            id: uuidv4(),
            name: value,
            complete: false,
        }
        let newTask = [...taskList];
        newTask.unshift(formValues);
        setTaskList(newTask);
        setValue('');
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input className="inputbox" type="text" value={value} onChange={(e) => setValue(e.target.value)} placeholder="Enter Task Here...." />
            </form>
            
        </div>
    );
}

export default Form;
import React from "react";
function Button(props) {
  const {allList, completeList, activeList, setAllList, setCompleteList, setActiveList} = props;

  const handleCompleteTask = () => {
    setAllList(false);
    setCompleteList(true);
    setActiveList(false);
  };
  const handleActiveTask = () => {
    setAllList(false);
    setCompleteList(false);
    setActiveList(true);
  };
  const handleAll = () => {
    setAllList(true);
    setCompleteList(false);
    setActiveList(false);
  };

  return (
    <div className="buttonfield">
      <div>
      <button onClick={handleAll} className={allList?"button-active":"button-default"}>
        All
      </button>
      <button onClick={handleCompleteTask} className={completeList?"button-active":"button-default"}>
        Complete
      </button>
      <button onClick={handleActiveTask} className={activeList?"button-active":"button-default"}>
        Active
      </button>
      </div>
    </div>
  );
}

export default Button;

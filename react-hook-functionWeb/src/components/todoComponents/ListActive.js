import React from "react";

function ListActive(props) {
  const { taskList, setTaskList } = props;

  const handleComplete = (id) => {
    console.log(id);
    let comItem = [...taskList];
    setTaskList(
      comItem.map((item) =>
        item.id === id ? { ...item, complete: !item.complete } : item
      )
    );
  };

  const handleDelete = (id) => {
    console.log(id);
    let delItem = [...taskList];
    setTaskList(delItem.filter((x) => x.id !== id));
  };

  const handleAllChange = () => {
    let activeList = [...taskList];
    setTaskList(
        activeList.map(x => x.complete === false ? {...x, complete: !x.complete} : x)
    );
  }

  return (
    <div>
      <div>
        <table>
          <tbody>
            {taskList
              .filter((x) => x.complete === false)
              .map((task) => (
                <tr key={task.id} className={task.complete ? "taskdone" : null}>
                  <td style={{ width: "10%" }}>
                    <input
                      defaultChecked={task.complete ? true : false}
                      onClick={() => handleComplete(task.id)}
                      type="checkbox"
                    />
                  </td>
                  <td style={{ width: "40%" }}>{task.name}</td>
                  <td style={{ width: "10%" }}>
                    <button
                      className="button-delete"
                      onClick={() => handleDelete(task.id)}
                    >
                      X
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
      <div>
        <button onClick={handleAllChange} className="button-Toggle">Toggle</button>
      </div>
    </div>
  );
}

export default ListActive;

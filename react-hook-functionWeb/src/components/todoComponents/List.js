import React from "react";

function List(props) {
  const { taskList, setTaskList} = props;

  const handleComplete = (id) => {
    console.log(id);
    let comItem = [...taskList];
    setTaskList(
      comItem.map((item) =>
        item.id === id ? { ...item, complete: !item.complete } : item
      )
    );
  };

  const handleDelete = (id) => {
    console.log(id);
    let delItem = [...taskList];
    setTaskList(delItem.filter((x) => x.id !== id));
  };

  return (
    <div>
      <table>
        <tbody>
          {taskList.map((task) => (
            <tr key={task.id} className={task.complete ? "taskdone" : null}>
              <td style={{ width: "10%" }}>
                <input
                  defaultChecked={task.complete ? true : false}
                  onClick={() => handleComplete(task.id)}
                  type="checkbox"
                />
              </td>
              <td style={{ width: "40%" }}>{task.name}</td>
              <td style={{ width: "10%" }}>
                <button
                  className="button-delete"
                  onClick={() => handleDelete(task.id)}
                >
                  X
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default List;

import React, {useState, useEffect} from 'react';
import DragNDrop from './DragNDropComponents/DragNDrop';

const defaultData = [
    {title: 'group 1', items: ['1', '2', '3']},
    {title: 'group 2', items: ['4', '5']}
  ]

function Drag_Drop(props) {
    const [data, setData] = useState();  
    useEffect(() => {
      if (localStorage.getItem('List')) {
        console.log(localStorage.getItem('List'))
        setData(JSON.parse(localStorage.getItem('List')))
      } else {
        setData(defaultData)
      }
    }, [setData])

    return (
    <div className="AppChart">
        <header className="AppChart-header">
            <DragNDrop data={data}/>
            {/* <div className="drag-n-drop">
                {defaultData.map((grp, grpI) => (
                    <div key={grp.title} className="dnd-group">
                        <div className="group-title">{grp.title}</div>
                        {grp.items.map((item, itemI) => (
                            <div draggable key={item} className="dnd-item">
                                {item}
                            </div>
                        ))}
                    </div>
                ))}
            </div> */}
        </header>
    </div>
    );
}

export default Drag_Drop;
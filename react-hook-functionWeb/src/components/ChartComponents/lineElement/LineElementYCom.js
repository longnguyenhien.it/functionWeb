import React, {useState} from 'react';

function LineElementYCom(props) {
    const [activeY, setActiveY] = useState([]);

    const activeYField = () => {
        if (activeY.length == 0) {
            return 'Drag Y Element Here...'
        }
        return (
            activeY.map((ele) => 
                <a key={ele.id}
                    className="element"
                    draggable
                >
                    {ele.name}&nbsp;
                </a>
            )
        )
    }
    return (
        <>
         <h4 style={{marginTop: "20px"}}>Row Y-Axis:</h4>
            <div className="midle-item">
                <div className="midle-row">
                    <div className="midle-column midle midle-item">
                        <p>
                            {
                                activeYField()
                            }
                        </p>
                    </div>
                </div>
            </div>   
        </>
    );
}

export default LineElementYCom;
import React, { useRef, useState } from 'react';
import lineElementX from '../fileChart/lineElementX';
import lineElementY from '../fileChart/lineElementY';

function LineElementCom(props) {
    const {dragging, setDragging, activeX, setActiveX, handleXEnter} = props;

    const [eleXActive, setEleXActive] = useState(false);

    const dragItemX = useRef();
    const dragNodeX = useRef();

    const handleListDragStart = (e, ele) => {
        console.log('drag...', ele);
        dragItemX.current = ele;
        dragNodeX.current = e.target;
        //dragNodeX.current.addEventListener('dragend', handleXDragEnd)
        console.log(handleXEnter);
            dragNodeX.current.addEventListener('dragend', handleXDragEnd)
        setTimeout(()=>{
            setDragging(true);
        },0) 
    }


    const handleXDragEnd = () => {
        const currentItem = dragItemX.current;
        let actX = [...activeX];
        console.log(currentItem);

        //add Chart to active field

        actX.unshift(currentItem);
        setActiveX(actX);
        
        //delete Chart in default field
        //let delChart = [...listChart];
        //setListChart(delChart.filter((x)=> x.id !== currentItemId));


        //set Dragend
        setDragging(false);
        dragNodeX.current.removeEventListener('dragend', handleXDragEnd);
        dragItemX.current = null;
        dragNodeX.current = null;
        console.log('Ending drag...');
    }

    const getStyle = (ele) => {
        const currentItem = dragItemX.current;
        if (currentItem == ele) {
            return 'current-ele'
        }
        return 'element'
    }

    return (
        <div>
            <h5 style={{marginTop: "20px"}}>Element</h5>
            <div className="left-item">
                <div className="left-row">
                    <div className="left-column">
                        <ul>
                            <li>
                                <strong>X Element:<br /></strong>
                                {
                                    lineElementX.map((ele) => 
                                        <a key={ele.id}
                                              className={dragging?getStyle(ele):"element"}
                                              draggable
                                              
                                              onDragStart={(e) => {handleListDragStart(e, ele)}}
                                        >
                                            {ele.name}&nbsp;
                                        </a>
                                    )
                                }
                            </li>
                            <li>
                                <strong>Y Element:<br /></strong>
                                {
                                    lineElementY.map((ele) => 
                                    <p key={ele.id}
                                          className="element"
                                          draggable
                                    >
                                        {ele.name}&nbsp;
                                    </p>
                                    )
                                }
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LineElementCom;
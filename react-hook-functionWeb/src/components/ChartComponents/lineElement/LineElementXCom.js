import React, { useState } from 'react';

function LineElementXCom(props) {
    const {activeX, sethandleXEnter, dragging} = props


    /* const handleDragEnter = (e) => {
        console.log('Entering drag...');
        dragNode.addEventListener('dragend', handleListDragEnd)
    }
    const handleListDragEnd = () => {
        const currentItem = dragNode;
        const currentItemId = dragItem;
        let actX = [...activeX];

        console.log(currentItemId);
        console.log('Ending drag...');
        //add Chart to active field
        actX.pop(currentItem)
        setActiveX(actX);

        //delete Chart in default field
        //let delChart = [...listChart];
        //setListChart(delChart.filter((x)=> x.id !== currentItemId));


        //set Dragend
        setDragging(false);
        dragNode.removeEventListener('dragend', handleListDragEnd);
        dragItem = null;
        dragNode = null;
    } */


    const activeXField = () => {
        if (activeX.length == 0) {
            return 'Drag X Element Here...'
        }
        console.log(activeX);
        return (
            activeX.map((ele) => 
                <a key={ele.id}
                    className="element"
                >
                    {ele.name}&nbsp;
                </a>
            )
        )
    }

    return (
        <>
         <h4 style={{marginTop: "20px"}}>Column/Label X-Axis:</h4>
            <div className="midle-item">
                <div className="midle-row">
                    <div className="midle-column midle midle-item"
                         
                    >
                        <p>
                            {
                                activeXField()
                            }
                        </p>
                    </div>
                </div>
            </div>   
        </>
    );
}

export default LineElementXCom;
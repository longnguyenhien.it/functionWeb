const pieElement = {
        XName: {
          Day: ["Mon", "Tue", "Wed", "Thu", "Fri"],
          Week: ["1", "2", "3", "4"],
          Month: ["Jan", "Mar", "May", "Jul", "Sep", "Nov", "Dec"],
        },
        YName: {
            HourlyTemp: {
            Day: [3, 2, 5, 6, 1],
            Week: [12, 23, 24, 13],
            Month: [35, 56, 67, 12, 34, 67, 43],
          },
          RainFall: {
            Day: [5, 2, 3, 1, 6],
            Week: [23, 27, 24, 13],
            Month: [31, 36, 27, 52, 14, 27, 33],
          },
          WindSpeed: {
            Day: [3, 2, 1, 6, 5],
            Week: [23, 27, 24, 13],
            Month: [31, 36, 27, 52, 14, 27, 33],
          },
          Money: {
            Day: [8, 1, 4, 3, 2],
            Week: [23, 27, 24, 13],
            Month: [31, 36, 27, 52, 14, 27, 33],
          },
          Rate: {
            Day: [8, 1, 4, 3, 2],
            Week: [23, 27, 24, 13],
            Month: [31, 36, 27, 52, 14, 27, 33],
          },
        },
    defaultElement: {
         X:[
          { id: "X1", name: "Day" },
          { id: "X2", name: "Week" },
          { id: "X3", name: "Month" },
         ],
         Y:[
              {id:'Y1', name: 'Hourly Temp'},
              {id:'Y2', name: 'RainFall'},
              {id:'Y3', name: 'WindSpeed'},
              {id:'Y4', name: 'Money'},
              {id:'Y5', name: 'Rate'},
         ],
    },
    activeElement: {
         X: [],
         Y: [],
   }
}
export default pieElement;
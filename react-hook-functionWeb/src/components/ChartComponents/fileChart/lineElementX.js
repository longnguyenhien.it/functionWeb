const lineElementX = [
    {id:1, name: 'Day'},
    {id:2, name: 'Week'},
    {id:3, name: 'Month'},
    {id:4, name: 'Year'},
];

export default lineElementX;
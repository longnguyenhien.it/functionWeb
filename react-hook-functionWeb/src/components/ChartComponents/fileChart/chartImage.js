const chartImages = [
    { id: '1', src: '/chartIcon/line-graph.png', alt: 'line', title: 'line-chart', name: 'Line'},
    { id: '2', src: '/chartIcon/pie-chart.png', alt: 'pie', title: 'pie-chart', name: 'Pie'},
    { id: '3', src: '/chartIcon/column.png', alt: 'column', title: 'bar-chart', name: 'Bar'},
    { id: '4', src: '/chartIcon/bar-chart.png', alt: 'bar', title: 'horizon-bar-chart', name: 'Horizone'},
    { id: '5', src: '/chartIcon/radar.png',alt: 'radar', title: 'radar-chart', name: 'Radar'},
    { id: '6', src: '/chartIcon/doughnut-chart.png',alt: 'doughnut', title: 'doughnut-chart', name: 'Doughnut'},
  ];
  
  export default chartImages;
import React from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";

function LeftChart(props) {
  const { chartList } = props;
  return (
    <Droppable droppableId="dndLeftChart">
      {(provided, snapshot) => {
        return (
          <div
            className="left-column"
            {...provided.droppableProps}
            ref={provided.innerRef}
            style={{
              background: snapshot.isDraggingOver ? "lightgrey" : "white",
              width: "100%",
              ...provided.droppableProps.style,
            }}
          >
            <div>
              {chartList.map((cList, index) => {
                return (
                  <Draggable
                    key={cList.id}
                    draggableId={cList.id}
                    index={index}
                  >
                    {(provided) => {
                      return (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={{
                            display: "inline-block",
                            ...provided.draggableProps.style,
                          }}
                          className="draggable-chart"
                        >
                          <img
                            src={cList.src}
                            alt={cList.alt}
                            title={cList.title}
                            style={{ padding: "10px 15px" }}
                          />
                          <p style={{ fontSize: "10px" }}>{cList.name}</p>
                        </div>
                      );
                    }}
                  </Draggable>
                );
              })}
            </div>
            {provided.placeholder}
          </div>
        );
      }}
    </Droppable>
  );
}

export default LeftChart;

import React from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";

function MiddleChart(props) {
  const { activeChart, hasChart } = props;

  const activeField = () => {
    if (activeChart.length == 0) {
      return <p style={{ opacity: 0.6, marginTop: "15px" }}>Drag Chart Here</p>;
    }
    return activeChart.map((acList, index) => {
      return (
        <Draggable key={acList.id} draggableId={acList.id} index={index}>
          {(provided) => {
            return (
              <strong
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                style={{
                  ...provided.draggableProps.style,
                }}
              >
                <img
                  src={acList.src}
                  alt={acList.alt}
                  title={acList.title}
                  style={{ padding: "3px 10px", width: "60px" }}
                />
                <p style={{ fontSize: "13px", marginLeft: "27%" }}>
                  {acList.name}
                </p>
              </strong>
            );
          }}
        </Draggable>
      );
    });
  };

  return (
    <Droppable droppableId="dndMiddleChart">
      {(provided, snapshot) => {
        return (
          <div
            className={
              hasChart
                ? "midle-row-true"
                : "midle-item midle-column midle-row-false"
            }
            {...provided.droppableProps}
            ref={provided.innerRef}
            style={{
              background: snapshot.isDraggingOver ? "lightgrey" : "white",
            }}
          >
            <p>{activeField()}</p>
            {provided.placeholder}
          </div>
        );
      }}
    </Droppable>
  );
}

export default MiddleChart;

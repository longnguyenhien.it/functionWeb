import React, { useEffect, useState } from "react";
import { Bar, Doughnut, HorizontalBar, Line, Pie, Radar } from "react-chartjs-2";
import barElements from "../ChartComponents/fileChart/barElement";
import lineElements from "../ChartComponents/fileChart/lineElements";
import pieElement from "../ChartComponents/fileChart/pieElement";

function RightChart(props) {
  const { keyChart, activeElement } = props;

  //Line
  const [labelLineState, setLabelLineState] = useState();
  const [dataLineState, setDataLineState] = useState();
  //Pie
  const [labelPieState, setLabelPieState] = useState();
  const [dataPieState, setDataPieState] = useState();
  //Bar
  const [labelBarState, setLabelBarState] = useState();
  const [dataBarState, setDataBarState] = useState();

  //set data
  const labelSet = () => {
    const currentElement = activeElement.X[0].name;
    switch (currentElement) {
      case "Day":
        setLabelLineState(lineElements.XName.Day);
        setLabelPieState(pieElement.XName.Day);
        setLabelBarState(barElements.XName.Day);
        break;
      case "Week":
        setLabelLineState(lineElements.XName.Week);
        setLabelPieState(pieElement.XName.Week);
        setLabelBarState(barElements.XName.Week);
        break;
      case "Month":
        setLabelLineState(lineElements.XName.Month);
        setLabelPieState(pieElement.XName.Month);
        setLabelBarState(barElements.XName.Week);
        break;
      case "Year":
        setLabelLineState(lineElements.XName.Year);
        setLabelBarState(barElements.XName.Year);
        break;
      default:
        break;
    }
  };
  const dataPerCase = (currentLabel, dataCase) => {
    switch (currentLabel) {
      case "Day":
        setDataLineState(lineElements.YName[dataCase].Day);
        setDataPieState(pieElement.YName[dataCase].Day);
        setDataBarState(barElements.YName[dataCase].Day);
        break;
      case "Week":
        setDataLineState(lineElements.YName[dataCase].Week);
        setDataPieState(pieElement.YName[dataCase].Week);
        setDataBarState(barElements.YName[dataCase].Week);
        break;
      case "Month":
        setDataLineState(lineElements.YName[dataCase].Month);
        setDataPieState(pieElement.YName[dataCase].Month);
        setDataBarState(barElements.YName[dataCase].Month);
        break;
      case "Year":
        setDataLineState(lineElements.YName[dataCase].Year);
        setDataBarState(barElements.YName[dataCase].Year);
        break;
      default:
        break;
    }
  };
  const dataSet = () => {
    const currentElement = activeElement.Y[0].name;
    const currentLabel = activeElement.X[0].name;
    switch (currentElement) {
      case "Hourly Temp":
        dataPerCase(currentLabel, "HourlyTemp");
        break;
      case "RainFall":
        dataPerCase(currentLabel, "RainFall");
        break;
      case "WindSpeed":
        dataPerCase(currentLabel, "WindSpeed");
        break;
      case "Money":
        dataPerCase(currentLabel, "Money");
        break;
      case "Rate":
        dataPerCase(currentLabel, "Rate");
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    labelSet();
    dataSet();
  }, []);
  //For LineChart
  const dataLine = {
    labels: labelLineState,
    datasets: [
      {
        label: activeElement.Y[0].name,
        fill: false,
        data: dataLineState,
        borderColor: ["rgba(255, 206, 86, 1)"],
        backgroundColor: ["rgba(255, 206, 86, 1)"],
        pointbackgroundColor: "rgba(255, 206, 86, 1)",
        pointborderColor: "rgba(255, 206, 86, 1)",
      },
    ],
  };
  const optionsLine = {
    title: {
      display: true,
      text: "Line Chart",
    },
  };
  //For PieChart
  const dataPie = {
    labels: labelPieState,
    datasets: [
      {
        label: activeElement.Y[0].name,
        data: dataPieState,
        backgroundColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(255, 205, 86, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 159, 64, 1)",
          "rgba(153, 102, 255, 1)",
          "rgb(94, 228, 127)",
          "rgb(184, 228, 28)",
        ],
      },
    ],
  };
  const optionsPie = {
    title: {
      display: true,
      text: "Pie Chart",
    },
  };
  //For BarChart
  const dataBar = {
    labels: labelBarState,
    datasets: [
      {
        label: activeElement.Y[0].name,
        data: dataBarState,
        borderColor: [
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
        ],
        backgroundColor: [
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
        ],
      },
    ],
  };
  const optionsBar = {
    title: {
      display: true,
      text: "Bar Chart",
    },
  };
  //For Horizone
  const dataHorizone = {
    labels: labelBarState,
    datasets: [
      {
        label: activeElement.Y[0].name,
        data: dataBarState,
        borderColor: [
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
        ],
        backgroundColor: [
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(153, 102, 255, 1)",
        ],
      },
    ],
  };
  const optionHorizone = {
    title: {
      display: true,
      text: "Horizone-Bar Chart",
    },
  }
  //For Radar
  const optionsRadar = {
    title: {
      display: true,
      text: "Radar Chart",
    },
  };
  //For Doughnut
  const optionsDoughnut = {
    title: {
      display: true,
      text: "Doughnut Chart",
    },
  };


  const showChart = () => {
    switch (keyChart) {
      case "1":
        return <Line data={dataLine} options={optionsLine} />;
      case "2":
        return <Pie data={dataPie} options={optionsPie} />;
      case "3":
        return <Bar data={dataBar} options={optionsBar} />;
      case "4":
        return <HorizontalBar data={dataHorizone} options={optionHorizone} />;
      case "5":
        return <Radar data={dataLine} options={optionsRadar} />;
      case "6":
        return <Doughnut data={dataPie} options={optionsDoughnut} />;
      default:
        break;
    }
  };

  return (
    <>
      <div className="chartjs">{showChart()}</div>
    </>
  );
}

export default RightChart;

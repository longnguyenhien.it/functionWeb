import React, { useState, useRef } from "react";
import chartImages from './ChartComponents/fileChart/chartImage';
import LineElementXCom from "./ChartComponents/lineElement/LineElementXCom";
import LineElementYCom from "./ChartComponents/lineElement/LineElementYCom";
import LineElementCom from "./ChartComponents/lineElement/LineElementCom";
import lineElementX from "./ChartComponents/fileChart/lineElementX";

function Chart(props) {

const [dragging, setDragging] = useState(false);
const [listChart, setListChart] = useState(chartImages);
const [activeChart, setactiveChart] = useState([]);
const [hasChart, setHasChart] = useState(false);
const [activeX, setActiveX] = useState([]);

const [handleXEnter, setHandleXEnter] = useState(false);

const dragItem = useRef();
const dragNode = useRef();


//Drag List
    const handleListDragStart = (e, chartId) => {
        console.log('drag...', chartId);
        dragItem.current = chartId;
        dragNode.current = e.target;
        setTimeout(()=>{
            setDragging(true);
        },0) 
    }

    const handleDragEnter = (e) => {
        console.log('Entering drag...');
        dragNode.current.addEventListener('dragend', handleListDragEnd)
    }
    
    const handleListDragEnd = () => {
        const currentItem = dragNode.current;
        const currentItemId = dragItem.current;
        let actChart = [...activeChart];

        console.log(currentItem);
        console.log('Ending drag...');
        //add Chart to active field
        actChart.splice(0,1,currentItem);
        setactiveChart(actChart);

        //delete Chart in default field
        //let delChart = [...listChart];
        //setListChart(delChart.filter((x)=> x.id !== currentItemId));

        //sethasChart
        if (activeChart !== []) {
            setHasChart(true)
        }

        //set Dragend
        setDragging(false);
        dragNode.current.removeEventListener('dragend', handleListDragEnd);
        dragItem.current = null;
        dragNode.current = null;
    }

// End Drag List


    const activeChartField = () => {
        if (activeChart.length == 0) {
            return 'Drag Chart Here...'
        }
        return (
            activeChart.map((chart) =>
            <strong 
                key={chart.id}
            >                                 
            <img  
                src={chart.src} 
                alt={chart.alt}
                title={chart.title} 
                style={{padding: "10px", width:"40%"}}
            />                                
            </strong>
            )
        )
    }

  return (
    <div>
        <div className="row">
            <aside className="column left">
                <div className="left-group">
                    <div className="left-item" style={{marginTop: "40px"}}>
                        <input type="text" placeholder="Seach..."></input>
                    </div>
                    <h5 style={{marginTop: "30px"}}>Chart</h5>
                    <div className="left-item">
                        <div className="left-row">
                            <div className="left-column left-item">
                                <p>
                                    {listChart.map((chart) => 
                                        <strong
                                            key={chart.id}
                                            draggable 
                                            onDragStart={(e) => {handleListDragStart(e, chart.id)}}
                                        >
                                                <img  
                                                src={chart.src} 
                                                alt={chart.alt}
                                                title={chart.title} 
                                                style={{padding: "10px"}}/>
                                                
                                        </strong>   
                                        )
                                    }
                                </p>
                            </div>
                        </div>
                    </div>  
                    {
                        hasChart ? 
                        (
                        <LineElementCom 
                            dragging={dragging}
                            setDragging={setDragging}
                            activeX={activeX}
                            setActiveX={setActiveX}
                            handleXEnter={handleXEnter}
                        />
                        )
                        :
                        null
                    }    
                </div>
            </aside>
            <section className="column midle">
                <div className="midle-group">
                    <h4 style={{marginTop: "40px"}}>Chart:</h4>
                    <div className="midle-item">
                        <div className="midle-row">
                            <div 
                                className="midle-column midle midle-item"
                                onDragEnter={dragging?(e) => {handleDragEnter(e)}:null}
                            >
                                <p>
                                    {   
                                        activeChartField()
                                    }
                                </p>
                            </div>
                        </div>
                    </div>
                    {
                        hasChart ?
                        (
                            <>
                                <LineElementXCom 
                                    dragging={dragging}
                                    activeX={activeX} 
                                    setHandleXEnter = {setHandleXEnter}   
                                />
                                <LineElementYCom />
                            </>
                        )
                        :
                        null
                    }
                       
                    <div style={{textAlign: "center", marginTop: "20px"}}><button className="button-Toggle">Confirm</button></div>
                </div>       
            </section>
            <section className="column right">
                <div className="right-group">
                    <div className="right-item">
                        <img style={{width: "590px"}} src="/logo192.png" />
                    </div>
                </div>
                
            </section>
        </div>
    </div>
  );
}

export default Chart;
